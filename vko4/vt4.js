"use strict";

const gradientteja = 10;

const skrolleriTeksti =
	"TIEA2120 Web-käyttöliittymien ohjelmointi -kurssin viikkotehtävä 4 taso 3 edellyttää skrollerin toteuttamista. Tämä skrolleri toimii tekstin määrästä riippumatta";

// Aloitetaan sitten kun on latautunut
document.addEventListener("DOMContentLoaded", () => {
	// Lisätään click kuuntelija nappulalle
	lisaaPolloKuuntelija();

	// alustetaan tolpat annetulla lukumäärällä
	luoGradientPalkit(gradientteja);

	// Eka pöllö jo HTML -koodissa, kopsataan sitä jatkossa uusia varten

	luoJanis();
	teeAlempiSkrolleri();
	const ylempi = new YlempiSkrolleri();
	ylempi.animoi();
	window.requestAnimationFrame(animoiLumiHiutaleet);
});

function lisaaPolloKuuntelija() {
	let nappula = document.getElementById("lisaaPollo");
	nappula.addEventListener("click", () => {
		luoPollo();
	});
}

// PALKIT

/**
 * Luo vaakatasossa rullaavat palkit
 * @param {*} maara annettu palkkien määrä
 */
function luoGradientPalkit(maara) {
	let kontti = document.getElementById("palkkiContainer");

	// kahden sekunnin välein uusi palkki
	const intervalli = 2000 / maara;

	for (let i = 0; i < maara; i++) {
		let objekti = document.createElement("object");
		objekti.data = "gradientBar.svg";
		objekti.type = "image/svg+xml";
		objekti.className = "gradienttiPalkki";
		objekti.style.animationDelay = `${intervalli * i}ms`;

		kontti.appendChild(objekti);
	}
}

// JÄNIS

/**
 * Luo jäniksen
 */
function luoJanis() {
	let vasen = document.getElementById("janisVasenPiilotettu");
	let vasenKonteksti = vasen.getContext("2d");
	let oikea = document.getElementById("janisOikeaPiilotettu");
	let oikeaKonteksti = oikea.getContext("2d");

	let janisKuva = document.getElementById("janisKuva");

	// Callbacki kuvan latautumista varten
	janisKuva.onload = () => {
		vasen.width = janisKuva.width / 2;
		vasen.height = janisKuva.height;
		const vasenParams = [
			janisKuva,
			0,
			0,
			vasen.width,
			vasen.height,
			0,
			0,
			vasen.width,
			vasen.height
		];
		vasenKonteksti.drawImage(...vasenParams);

		oikea.width = janisKuva.width / 2;
		oikea.height = janisKuva.height;
		const oikeaParams = [
			janisKuva,
			oikea.width,
			0,
			oikea.width,
			oikea.height,
			0,
			0,
			oikea.width,
			oikea.height
		];
		oikeaKonteksti.drawImage(...oikeaParams);

		// Laitetaan tässä vaiheessa ruudulla näkyvien piirtokohteiden koot kuntoon
		let vasuri = document.getElementById("janisVasenPuoli");
		vasuri.width = vasen.width;
		vasuri.height = vasen.height * 2; // varataan tilaa venyttämiselle

		let oikeaP = document.getElementById("janisOikeaPuoli");
		oikeaP.width = oikea.width;
		oikeaP.height = oikea.height * 2;

		window.requestAnimationFrame(luoVenyvaJanis);
	};
}

/**
 * Luo aaltoilevan jäniksen; puolien kokoava funktio samalla timestampilla.
 * @param {*} aika aika
 */
function luoVenyvaJanis(aika) {
	const vasenPiilo = document.getElementById("janisVasenPiilotettu");
	const vasen = document.getElementById("janisVasenPuoli");
	const oikeaPiilo = document.getElementById("janisOikeaPiilotettu");
	const oikea = document.getElementById("janisOikeaPuoli");

	venyttele(vasenPiilo, vasen, aika);
	venyttele(oikeaPiilo, oikea, aika);

	window.requestAnimationFrame(luoVenyvaJanis);
}

/**
 * Venyttelee annettua canvas-elementtiä
 * @param {*} lahde lähde-elementti
 * @param {*} kohde kohde-elementti jolle piirretään
 * @param {*} aika aika
 */
function venyttele(lahde, kohde, aika) {
	let lahdeKonteksti = lahde.getContext("2d");
	let kohdeKonteksti = kohde.getContext("2d");

	let lahdeData = lahdeKonteksti.getImageData(
		0,
		0,
		lahde.width,
		lahde.height
	);
	let kohdeData = kohdeKonteksti.createImageData(kohde.width, kohde.height);

	let lahdeRivi = 0,
		maaliRivi = 0;

	while (lahdeRivi < lahdeData.height && maaliRivi < kohdeData.height) {
		// Kokeillaan 4 per pikseli
		const a = 4;
		for (let l = 0; l < laskeRivit(lahdeRivi, aika); l++) {
			let lahdeRiviAlku = lahdeRivi * lahdeData.width * a;
			let kohdeRiviAlku = maaliRivi * kohdeData.width * a;
			// Piirretään rivi
			for (let i = 0; i < a * lahdeData.width; i++) {
				const uusi = lahdeData.data[lahdeRiviAlku + i];
				kohdeData.data[kohdeRiviAlku + i] = uusi;
			}
			maaliRivi++;
		}
		lahdeRivi++;
	}

	kohdeKonteksti.putImageData(kohdeData, 0, 0);
}

// Katsoo aikaa vasten montako kertaa rivi pitää kopioida
const laskeRivit = (rivit, aika) =>
	Math.round(2 + Math.sin((rivit - aika / 30) / 20));

// PÖLLÖ

/**
 * Lisää pöllön, joka menee vuorotellen kumpaankin suuntaan
 */
function luoPollo() {
	let kontti = document.getElementById("polloKontti");
	let ekaPollo = kontti.children[0];

	// Kopioidaan valmiiksi dokumentissa olevasta pohjasta
	let pollo = ekaPollo.cloneNode(true);
	const luku = haeRandomLuku(1, 7);
	pollo.classList.add(`anim${luku}`);

	// jos toteutuu niin toiseen suuntaan
	if (kontti.children.length % 2 == 1)
		pollo.classList.add("kaanteinenSuunta");

	kontti.appendChild(pollo);
}

/**
 * Apufunktio, joka luo random luvun min ja max väliltä.
 * Käytetty pöllöjen luokan laittamisessa
 * @param {*} min minimi
 * @param {*} max maksimi
 * @returns luvun
 */
function haeRandomLuku(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * Tekee gradienttitekstiskrollerin
 */
function teeAlempiSkrolleri() {
	let kanvas = document.getElementById("alempiSkrolleri");
	let konteksti = kanvas.getContext("2d");

	const alkuKorkeus = 200;

	kanvas.height = alkuKorkeus;
	let gradientti = konteksti.createLinearGradient(0, -20, 0, kanvas.height);
	gradientti.addColorStop(0, "black");
	gradientti.addColorStop(0.5, "yellow");
	gradientti.addColorStop(1, "black");

	const fontti = `${kanvas.height}px calibri`;
	konteksti.font = fontti;
	konteksti.fillStyle = gradientti;
	const { width } = konteksti.measureText(skrolleriTeksti); // haetaan tekstin leveys dynaamisesti
	kanvas.width = width;

	// Ilmeisesti pakko laittaa uusiksi koska nollaantuvat
	konteksti.font = fontti;
	konteksti.fillStyle = gradientti;
	// Alapuolella roikkuu
	konteksti.textBaseline = "hanging";

	konteksti.fillText(skrolleriTeksti, 0, 0);
}

/**
 * Ylempiskrolleri luokka joka tekee ja animoi ylemmän skrollerin
 * @class YlempiSkrolleri
 */
class YlempiSkrolleri {
	constructor() {
		// sidotaan this
		this.animoiYlempiSkrolleri = this.animoiYlempiSkrolleri.bind(this);
		this.onScrollerLoop = this.onScrollerLoop.bind(this);
		this.animoi = this.animoi.bind(this);

		// tila
		this.parametrit = {
			sivuttaisLiike: 0, // sivuttaisliikkeen suuruus
			sivuttaisNopeus: 1, // sivuttaisliikkeen nopeus
			pystyNopeus: 1, // pystyliikkeen nopeus
			sivuAallonPituus: 1, // sivuttaisliikkeen aallonpituus
			pystyAallonPituus: 1 // pystyliikkeen aallonpituus
		};

		this.canvas = document.getElementById("ylempiSkrolleri");
		this.konteksti = this.canvas.getContext("2d");
		this.canvas.height = 150;

		this.konteksti.font = "35px serif";
		this.canvas.width =
			this.konteksti.measureText(skrolleriTeksti).width + 1000; // lisää tilaa

		// satunnainen uusi animaatio
		this.canvas.parentElement.addEventListener(
			"animationiteration",
			this.onScrollerLoop
		);
	}

	// julkinen aloitusmetodi
	animoi() {
		window.requestAnimationFrame(this.animoiYlempiSkrolleri);
	}

	// Animoi ylemmän skrollerin
	animoiYlempiSkrolleri(aika) {
		let canvas = document.getElementById("ylempiSkrolleri");
		let konteksti = canvas.getContext("2d");

		konteksti.clearRect(0, 0, canvas.width, canvas.height);

		konteksti.fillStyle = "white";
		konteksti.font = "40px Sans-serif";
		konteksti.textBaseline = "middle";

		// mennään kirjain kerrallaan
		let x = 400;
		for (let i = 0; i < skrolleriTeksti.length; i++) {
			let kirjain = skrolleriTeksti.charAt(i);
			konteksti.fillText(
				kirjain,
				x + this.vaakaSaato(x, aika),
				this.pystySaato(x, aika)
			);
			x += konteksti.measureText(kirjain).width;
		}

		window.requestAnimationFrame(this.animoiYlempiSkrolleri);
	}

	vaakaSaato(xPos, aika) {
		return -Math.round(
			Math.cos(
				xPos / (50 * this.parametrit.sivuAallonPituus) +
					(aika * this.parametrit.sivuttaisNopeus) / 400
			) *
				this.parametrit.sivuttaisLiike *
				50
		);
	}

	pystySaato(xPos, aika) {
		let keskusta = this.canvas.height / 2;
		return Math.round(
			keskusta +
				Math.sin(
					xPos / (50 * this.parametrit.pystyAallonPituus) +
						(aika * this.parametrit.pystyNopeus) / 400
				) *
					(keskusta - 20)
		);
	}

	// Kun looppi alkaa alusta pistetään uusi animaatio
	onScrollerLoop() {
		this.parametrit.sivuttaisLiike = Math.random() * 5;
		// välillä siniaaltoa
		if (this.parametrit.sivuttaisLiike < 0.75) {
			this.parametrit.sivuttaisLiike = 0;
		}
		this.parametrit.sivuttaisNopeus = Math.random() + 1;
		this.parametrit.pystyNopeus = Math.random() + 1;

		this.parametrit.sivuAallonPituus =
			this.parametrit.sivuttaisLiike - 1 + Math.random() * 2;
		this.parametrit.pystyAallonPituus = 1 + Math.random() * 3;
	}
}

// HIUTALEET

var edellisenTimestamp = 0;
const intervalli = 200;

// animoi hiutaleet
function animoiLumiHiutaleet(aika) {
	if (aika - edellisenTimestamp > intervalli) {
		luoLumiHiutale();
		// pidetään yllä edellisen aikaa tilamuuttujalla
		edellisenTimestamp = aika;
	}
	tarkastaJaPysayta();
	window.requestAnimationFrame(animoiLumiHiutaleet);
}

var hiutaleet = [];

const hiutaleLeveys = 23,
	hiutaleKorkeus = 23;

/**
 * Tarkastaa lumihiutaleiden sijainnin pystysuunnassa ja pysäyttää jos tarpeeksi matalla
 *
 */
function tarkastaJaPysayta() {
	let hiutaleet = document.getElementsByClassName("lumihiutale");

	for (let hiutale of hiutaleet) {
		let pohja = getComputedStyle(hiutale).bottom;
		let h = parseFloat(pohja.slice(0, pohja.length - 2));
		// kun päästään pohjalle
		if (h <= hiutale.tavoiteKorkeus) {
			hiutale.style.bottom = `${hiutale.tavoiteKorkeus}px`;
			hiutale.style.animation = "none";
		}
	}
}

/**
 * Luo uuden lumihiutaleen ruudulle
 */
function luoLumiHiutale() {
	let kontti = document.getElementById("hiutaleKontti");

	// dokumentissa olevasta pohjasta kopio
	let pohja = document.getElementById("hiutaleMalliPohja");
	let hiutale = pohja.cloneNode(true);
	// id attribuutti muutetaan pois templatesta
	hiutale.id = "";

	// satunnainen x
	const vaakaKoordinaatti =
		Math.random() * (window.innerWidth - hiutaleLeveys);
	hiutale.style.left = `${vaakaKoordinaatti}px`;
	hiutale.x = vaakaKoordinaatti;

	// etsitään tavoite-y
	hiutale.tavoiteKorkeus = 0;
	let indeksi = Math.floor(vaakaKoordinaatti / hiutaleLeveys);
	for (let i = indeksi - 1; i <= indeksi + 1; i++) {
		if (i > 0 && hiutaleet[i]) {
			for (let j in hiutaleet[i]) {
				if (
					hiutaleet[i][j].x + hiutaleLeveys > hiutale.x &&
					hiutaleet[i][j].x < hiutale.x + hiutaleLeveys
				) {
					// alla hiutale
					const korkeus =
						hiutaleet[i][j].tavoiteKorkeus + hiutaleKorkeus;
					if (korkeus > hiutale.tavoiteKorkeus) {
						hiutale.tavoiteKorkeus = korkeus;
					}
				}
			}
		}
	}

	// tehdään taulukon sisälle aluksi indeksiin tyhjä taulukko, jonne puskea hiutaleita
	if (!hiutaleet[indeksi]) {
		hiutaleet[indeksi] = [];
	}
	hiutaleet[indeksi].push(hiutale);

	kontti.appendChild(hiutale);
}
