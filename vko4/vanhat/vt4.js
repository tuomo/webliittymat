"use strict";

function aloita() {
	let canvas = document.getElementById("canvas");
	canvas.width = document.clientWidth;
	canvas.height = document.body.clientHeight;
	let img = document.querySelector("img");
	let ctx = canvas.getContext("2d");
	ctx.drawImage(img, 0, 0);
}

window.onload = function() {
	aloita();
};
