# Järjestyslogiikat:

## default:
listaa tulokset sarjoittain aakkosjärjestyksessä ja pisteiden mukaan laskevassa pistejärjestyksessä eli eniten pisteitä saanut joukkue aina sarjan ensimmäisenä.

## Joukkue:
Jos klikkaa joukkue-sarakeotsikkoa niin listaus järjestetään joukkueen nimen mukaan nousevaan aakkosjärjestykseen. 

# Pisteet:
Jos klikkaa Pisteet-sarakeotsikkoa niin tulokset järjestetään pisteiden mukaan laskevaan järjestykseen. 

# Sarjat:
Jos klikkaa sarjat-sarakeotsikkoa niin tulokset järjestetään sarjan, pisteiden ja nimen mukaan järjestykseen. 

# Matka:
matkajärjestys

# Aika:
aikajärjestys