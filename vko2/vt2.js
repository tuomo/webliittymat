// data-muuttuja on sama kuin viikkotehtävässä 1.
//

"use strict";

console.log(data);

// Kopioidaan uuteen muuttujaan kaikki data, jota aletaan manipuloida
var kopio = Object.assign({}, data);
var joukkueet;

window.onload = function() {
	renderoiTaulukko();
	alustaKaikki();
};

function alustaKaikki() {
	joukkueet = haeKaikki();
	renderoiJoukkueet(joukkueet);

	renderoiLisaaRasti();

	lisaaJoukkueLomake(null);
}

// järjestysvakiot
const JOUKKUE = "JOUKKUE";
const PISTEET = "PISTEET";
const SARJAT = "SARJAT";
const MATKA = "MATKA";
const AIKA = "AIKA";
const OLETUS = "OLETUS";

// Järjestysmuuttuja
let jarjestys = OLETUS;

/* 
* 
* DOM-MANIPULOINTIFUNKTIOT
* 
*/

/**
 * Renderöi joukkuetaulukon kaikki muuttuvat elementit eli itse joukkueet
 * @param {Object} joukkueet
 */
function renderoiJoukkueet(joukkueet) {
	// Aluksi poistetaan mahdollinen edellinen listaus:
	const parent = document.getElementById("paaTaulukko");
	while (parent.lastChild.className === "joukkue") {
		parent.removeChild(parent.lastChild);
	}

	let vertaaFunktio;
	switch (jarjestys) {
		case JOUKKUE:
			vertaaFunktio = jarjestaJoukkue;
			break;

		case PISTEET:
			vertaaFunktio = jarjestaPisteet;
			break;

		case SARJAT:
			vertaaFunktio = jarjestaOletus;
			break;

		case MATKA:
			vertaaFunktio = jarjestaMatka;
			break;

		case AIKA:
			vertaaFunktio = jarjestaAika;
			break;

		default:
			vertaaFunktio = jarjestaOletus;
			break;
	}

	const joukkueetJarjestetty = joukkueet.sort(vertaaFunktio);

	const taulukko = document.getElementById("paaTaulukko");

	const joukkueLista = joukkueetJarjestetty.map(joukkue => {
		// Luodaan kaikki rivin DOM-elementit
		let rivi = document.createElement("tr");
		rivi.className = "joukkue";

		// Sarjatiedot
		let sarjaTiedot = document.createElement("td");
		let sarjaString = document.createTextNode(joukkue.sarja);
		sarjaTiedot.appendChild(sarjaString);

		// Joukkuetiedot
		let joukkueTiedot = document.createElement("td");
		let joukkueLinkki = document.createElement("a");
		joukkueLinkki.href = "#joukkue";
		joukkueLinkki.addEventListener("click", e => {
			e.preventDefault();
			lisaaJoukkueLomake(joukkue);
		});
		let joukkueNimi = document.createTextNode(joukkue.nimi);
		let rivinVaihto = document.createElement("br");
		let jasenString = joukkue.jasenet.join(", ");
		let jasenet = document.createTextNode(jasenString);
		joukkueLinkki.appendChild(joukkueNimi);
		joukkueTiedot.appendChild(joukkueLinkki);
		joukkueTiedot.appendChild(rivinVaihto);
		joukkueTiedot.appendChild(jasenet);

		// Pistetiedot
		const pisteTiedot = document.createElement("td");
		const pisteMaara = document.createTextNode(joukkue.pisteet);
		pisteTiedot.appendChild(pisteMaara);

		// Aika
		const aikaTiedot = document.createElement("td");
		const parsattuAika = millisekunnitAjaksi(joukkue.aika);
		const aikaMaara = document.createTextNode(parsattuAika);
		aikaTiedot.appendChild(aikaMaara);

		// Matka
		const matkaTiedot = document.createElement("td");
		const parsattuMatka = `${joukkue.matka} km`;
		const matkaMaara = document.createTextNode(parsattuMatka);
		matkaTiedot.appendChild(matkaMaara);

		// Lisätään kaikki luodulle riville
		rivi.appendChild(sarjaTiedot);
		rivi.appendChild(joukkueTiedot);
		rivi.appendChild(pisteTiedot);
		rivi.appendChild(aikaTiedot);
		rivi.appendChild(matkaTiedot);
		return rivi;
	});

	joukkueLista.forEach(rivi => taulukko.appendChild(rivi));
}

/**
 * Renderöi tyhjän taulukon
 *
 */
function renderoiTaulukko() {
	// Eka renderöidään itse taulukko
	let tupa = document.getElementById("tupa");
	let taulukko = document.createElement("TABLE");
	taulukko.setAttribute("id", "paaTaulukko");
	// Lisää taulukolle otsikko
	let caption = document.createElement("CAPTION");
	let captionTeksti = document.createTextNode("Tulokset");
	caption.appendChild(captionTeksti);
	taulukko.appendChild(caption);

	const ekaRivi = document.createElement("tr");
	// Sarja
	const sarjaElementti = document.createElement("th");
	const sarjaTeksti = document.createTextNode("Sarja");
	sarjaElementti.appendChild(sarjaTeksti);
	sarjaElementti.addEventListener("click", () => {
		jarjestys = SARJAT;
		renderoiJoukkueet(joukkueet);
	});

	// Joukkue
	const joukkueElementti = document.createElement("th");
	const joukkueTeksti = document.createTextNode("Joukkue");
	joukkueElementti.appendChild(joukkueTeksti);
	joukkueElementti.addEventListener("click", () => {
		jarjestys = JOUKKUE;
		renderoiJoukkueet(joukkueet);
	});
	// Pisteet
	const pisteElementti = document.createElement("th");
	const pisteTeksti = document.createTextNode("Pisteet");
	pisteElementti.appendChild(pisteTeksti);
	pisteElementti.addEventListener("click", () => {
		jarjestys = PISTEET;
		renderoiJoukkueet(joukkueet);
	});
	// Aika
	const aikaElementti = document.createElement("th");
	const aikaTeksti = document.createTextNode("Aika");
	aikaElementti.appendChild(aikaTeksti);
	aikaElementti.addEventListener("click", () => {
		jarjestys = AIKA;
		renderoiJoukkueet(joukkueet);
	});
	// Matka
	const matkaElementti = document.createElement("th");
	const matkaTeksti = document.createTextNode("Matka");
	matkaElementti.appendChild(matkaTeksti);
	matkaElementti.addEventListener("click", () => {
		jarjestys = MATKA;
		renderoiJoukkueet(joukkueet);
	});

	ekaRivi.appendChild(sarjaElementti);
	ekaRivi.appendChild(joukkueElementti);
	ekaRivi.appendChild(pisteElementti);
	ekaRivi.appendChild(aikaElementti);
	ekaRivi.appendChild(matkaElementti);

	taulukko.appendChild(ekaRivi);
	tupa.appendChild(taulukko);
}

/**
 * Lisää joukkueen lisäämiseksi tai muokkaamiseksi tarkoitetun DOM-elementtipuun
 * @param {Object} [joukkue=null] esitäytetty joukkue
 */
function lisaaJoukkueLomake(joukkue = null) {
	let lomake = document.getElementsByTagName("form")[1];

	// Aluksi poistetaan mahdollinen edellinen lomake
	lomake.removeChild(lomake.lastChild);

	let fieldset = document.createElement("FIELDSET");
	fieldset.setAttribute("id", "lisaaJoukkueFieldset");

	// <legend>
	let legend = document.createElement("LEGEND");
	let legendTeksti = document.createTextNode("Uusi joukkue");
	legend.appendChild(legendTeksti);

	let paragraph = document.createElement("P");
	let label = document.createElement("LABEL");
	let labelNimi = document.createTextNode("Nimi");
	let nimiInput = document.createElement("INPUT");
	// Jos annettu joukkue käytä sen nimeä
	const joukkueenNimi = joukkue == null ? "" : joukkue.nimi;
	nimiInput.type = "text";
	nimiInput.value = joukkueenNimi;
	nimiInput.id = "joukkue_nimi";
	nimiInput.addEventListener("input", e => {
		e.preventDefault();
		validoiJoukkueLisays();
	});
	label.appendChild(labelNimi);
	label.appendChild(nimiInput);
	paragraph.appendChild(label);

	// Jäsenet
	let fieldsetJasenet = document.createElement("FIELDSET");
	let jasenetLegend = document.createElement("LEGEND");
	let jasenetTeksti = document.createTextNode("Jäsenet");
	// Luo jäsen-input kentät
	if (joukkue !== null) {
		joukkue.jasenet.forEach(jasen => {
			let uusJasen = jasenLomakeGenerator(jasen);
			fieldsetJasenet.appendChild(uusJasen);
		});
	} else {
		let jasen1 = jasenLomakeGenerator();
		let jasen2 = jasenLomakeGenerator();
		fieldsetJasenet.appendChild(jasen1);
		fieldsetJasenet.appendChild(jasen2);
	}

	// Lisää jäsen nappula
	let lisaaJasenButton = document.createElement("BUTTON");
	lisaaJasenButton.name = "jäsen";
	let uusiJasen = document.createTextNode("Lisää jäsen");
	lisaaJasenButton.addEventListener("click", e => {
		e.preventDefault();
		let taasUusiJasen = jasenLomakeGenerator();
		fieldsetJasenet.insertBefore(taasUusiJasen, lisaaJasenButton);
	});
	lisaaJasenButton.appendChild(uusiJasen);

	// Poista jäsen nappula
	let poistaJasenButton = document.createElement("BUTTON");
	poistaJasenButton.name = "poista_jäsen";
	let poistaJasen = document.createTextNode("Poista jäsen");
	poistaJasenButton.addEventListener("click", e => {
		e.preventDefault();
		const jasenMaara = document.getElementsByClassName("jasenContainer");
		if (jasenMaara.length > 2) {
			fieldsetJasenet.removeChild(jasenMaara[jasenMaara.length - 1]);
			validoiJoukkueLisays();
		}
	});
	poistaJasenButton.appendChild(poistaJasen);

	jasenetLegend.appendChild(jasenetTeksti);
	fieldsetJasenet.appendChild(jasenetLegend);
	fieldsetJasenet.appendChild(lisaaJasenButton);
	fieldsetJasenet.appendChild(poistaJasenButton);

	let paragraphButton = document.createElement("P");
	let lisaaButton = document.createElement("BUTTON");
	lisaaButton.name = "joukkue";
	lisaaButton.setAttribute("id", "lisaaJoukkue");
	// lisaaButton.addEventListener("click", lahetaJoukkue);
	let buttonTeksti = document.createTextNode("Lisää/Tallenna Joukkue");
	lisaaButton.disabled = true;
	lisaaButton.addEventListener("click", e => {
		e.preventDefault();
		const id = joukkue !== null ? joukkue.id : null;
		const sarja = joukkue !== null ? joukkue.sarja : null;
		lisaaTaiMuokkaaJoukkuetta(sarja, id);
	});
	lisaaButton.appendChild(buttonTeksti);
	paragraphButton.appendChild(lisaaButton);

	// Lopuksi lisätään kaikki elementit elementtipuuksi
	fieldset.appendChild(legend);
	fieldset.appendChild(paragraph);
	fieldset.appendChild(fieldsetJasenet);
	fieldset.appendChild(paragraphButton);

	// Lisätään myös joukkueen rastit
	const joukkueenRastit = renderoiLisaaRastiLeimaus(joukkue);
	fieldset.appendChild(joukkueenRastit);

	lomake.appendChild(fieldset);
}

var rastiListaus = [];

/**
 * Tekee rastileimauslomakkeen joukkueelle
 * @param {Object} [joukkue=null] annettu joukkue
 */
function renderoiLisaaRastiLeimaus(joukkue = null) {
	rastiListaus = [];

	let fieldset = document.createElement("FIELDSET");
	fieldset.setAttribute("id", "joukkueenRastitLomake");
	let legend = document.createElement("LEGEND");
	let otsikko = document.createTextNode("Joukkueen rastit");

	legend.appendChild(otsikko);
	fieldset.appendChild(legend);

	let dropdown = document.createElement("SELECT");
	dropdown.setAttribute("id", "rastiDropdown");
	let tyhja = document.createElement("OPTION");
	tyhja.value = "";
	let tyhjaTeksti = document.createTextNode("");
	tyhja.appendChild(tyhjaTeksti);
	dropdown.appendChild(tyhja);
	if (joukkue !== null) {
		joukkue.rastit.forEach(rasti => {
			let vaihtoehto = document.createElement("OPTION");
			vaihtoehto.value = rasti.aika;
			vaihtoehto.dataset.rasti = rasti;
			let aikaTeksti = document.createTextNode(rasti.aika);
			vaihtoehto.appendChild(aikaTeksti);
			dropdown.appendChild(vaihtoehto);
			// Lisätään tilamuuttujaan
			rastiListaus.push(rasti);
		});
	}

	dropdown.addEventListener("change", e => {
		e.preventDefault();
		saturoiRastiEditointiLomake();
	});

	const rastit = joukkue == null ? [] : joukkue.rastit;
	const taulukko = renderoiJoukkueenRastit(rastit);

	fieldset.appendChild(dropdown);

	const rastinTiedotLomake = luoRastiLeimausEditointiLomake(joukkue);

	fieldset.appendChild(rastinTiedotLomake);
	fieldset.appendChild(taulukko);

	return fieldset;
}

/**
 * Esitäyttää valitun rastin tiedot lomakkeelle
 */
function saturoiRastiEditointiLomake() {
	let rastiArvo = document.getElementById("rastiDropdown").value;
	const valittuRastiIndex = rastiListaus.findIndex(
		rasti => rasti.aika == rastiArvo
	);
	if (valittuRastiIndex > -1) {
		let koodiElem = document.getElementById("editoiRastinKoodi");
		let aikaElem = document.getElementById("editoiRastinAika");
		const { koodi, lat, lon, aika } = rastiListaus[valittuRastiIndex];
		koodiElem.value = koodi;
		aikaElem.value = aika;
	}
}

/**
 * Luo lomakkeen, jolla rastin tietoja voi editoida
 * @param {*} rasti
 */
function luoRastiLeimausEditointiLomake(joukkue = null) {
	const rasti = joukkue !== null ? joukkue.rasti : null;

	let fieldset = document.createElement("FIELDSET");
	fieldset.setAttribute("id", "rastinLeimausEditointi");

	let legend = document.createElement("LEGEND");
	let legendTeksti = document.createTextNode(
		"Lisää tai muuta joukkueen rastileimauksen tietoja"
	);
	legend.appendChild(legendTeksti);

	let koodiParagraph = document.createElement("P");
	let koodiLabel = document.createElement("LABEL");
	let koodiNimi = document.createTextNode("Koodi");
	let koodiInput = document.createElement("INPUT");
	koodiInput.setAttribute("id", "editoiRastinKoodi");

	const rastiNimi = rasti == null ? "" : rasti.koodi;
	koodiInput.type = "text";
	koodiInput.value = rastiNimi;

	koodiLabel.appendChild(koodiNimi);
	koodiLabel.appendChild(koodiInput);
	koodiParagraph.appendChild(koodiLabel);

	let aikaParagraph = document.createElement("P");
	let aikaLabel = document.createElement("LABEL");
	let aikaNimi = document.createTextNode("aika");
	let aikaInput = document.createElement("INPUT");
	aikaInput.setAttribute("id", "editoiRastinAika");

	const rastiaika = rasti == null ? "" : rasti.aika;
	aikaInput.type = "text";
	aikaInput.value = rastiaika;

	aikaLabel.appendChild(aikaNimi);
	aikaLabel.appendChild(aikaInput);
	aikaParagraph.appendChild(aikaLabel);

	let paragraphButton = document.createElement("P");
	let lisaaButton = document.createElement("BUTTON");
	lisaaButton.name = "lisaaRasti";
	lisaaButton.setAttribute("id", "lisaaRasti");
	let buttonTeksti = document.createTextNode(
		"Lisää tai muuta rastileimausta"
	);
	lisaaButton.addEventListener("click", e => {
		e.preventDefault();
		lisaaRastiLeimausJoukkueelle(joukkue);
	});
	lisaaButton.appendChild(buttonTeksti);
	paragraphButton.appendChild(lisaaButton);

	let paragraPoistaphButton = document.createElement("P");
	let poistaButton = document.createElement("BUTTON");
	poistaButton.name = "poistaRasti";
	poistaButton.setAttribute("id", "poistaRasti");
	let poistaButtonTeksti = document.createTextNode("Poista rastileimaus");
	poistaButton.addEventListener("click", e => {
		e.preventDefault();
		poistaRastiLeimausJoukkueelta(joukkue);
	});
	poistaButton.appendChild(poistaButtonTeksti);
	paragraPoistaphButton.appendChild(poistaButton);

	fieldset.appendChild(legend);
	fieldset.appendChild(koodiParagraph);
	fieldset.appendChild(aikaParagraph);
	fieldset.appendChild(paragraphButton);
	fieldset.appendChild(paragraPoistaphButton);

	return fieldset;
}

/**
 * renderöi joukkueen rastit
 * @param {*} rastit joukkueen rastit
 */
function renderoiJoukkueenRastit(rastit = []) {
	let taulukko = document.createElement("TABLE");
	taulukko.setAttribute("id", "rastiTaulukko");

	const ekaRivi = document.createElement("tr");
	const koodiElementti = document.createElement("th");
	const koodiTeksti = document.createTextNode("Koodi");
	koodiElementti.appendChild(koodiTeksti);
	const latElementti = document.createElement("th");
	const latTeksti = document.createTextNode("Lat");
	latElementti.appendChild(latTeksti);
	const lonElementti = document.createElement("th");
	const lonTeksti = document.createTextNode("Lon");
	lonElementti.appendChild(lonTeksti);
	const aikaElementti = document.createElement("th");
	const aikaTeksti = document.createTextNode("aika");
	aikaElementti.appendChild(aikaTeksti);

	ekaRivi.appendChild(koodiElementti);
	ekaRivi.appendChild(latElementti);
	ekaRivi.appendChild(lonElementti);
	ekaRivi.appendChild(aikaElementti);

	taulukko.appendChild(ekaRivi);

	if (rastit.length > 0) {
		rastit.forEach(rasti => {
			let rivi = document.createElement("tr");
			rivi.className = "rastiLeimaus";

			let koodiTiedot = document.createElement("td");
			let koodiString = document.createTextNode(rasti.koodi);
			koodiTiedot.appendChild(koodiString);

			let latTiedot = document.createElement("td");
			let latString = document.createTextNode(rasti.lat);
			latTiedot.appendChild(latString);

			let lonTiedot = document.createElement("td");
			let lonString = document.createTextNode(rasti.lon);
			lonTiedot.appendChild(lonString);

			let aikaTiedot = document.createElement("td");
			let aikaString = document.createTextNode(rasti.aika);
			aikaTiedot.appendChild(aikaString);

			rivi.appendChild(koodiTiedot);
			rivi.appendChild(latTiedot);
			rivi.appendChild(lonTiedot);
			rivi.appendChild(aikaTiedot);

			taulukko.appendChild(rivi);
		});
	}

	return taulukko;
}

/**
 * Renderöi "Lisää rasti" -lomakkeen
 */
function renderoiLisaaRasti() {
	let rastiLomake = document.getElementsByTagName("form")[0];
	rastiLomake.removeChild(rastiLomake.lastChild);

	let fieldset = document.createElement("FIELDSET");

	// <legend>
	let legend = document.createElement("LEGEND");
	let legendTeksti = document.createTextNode("Rastin tiedot");
	legend.appendChild(legendTeksti);

	let lat = teeRastiInput("lat");
	let lon = teeRastiInput("lon");
	let koodi = teeRastiInput("koodi");

	let paragraphButton = document.createElement("P");
	let lisaaButton = document.createElement("BUTTON");
	lisaaButton.name = "lisaaRasti";
	lisaaButton.setAttribute("id", "lisaaRasti");
	// lisaaButton.addEventListener("click", lahetaJoukkue);
	let buttonTeksti = document.createTextNode("Lisää rasti");
	lisaaButton.addEventListener("click", e => {
		e.preventDefault();
		lisaaRasti();
	});
	lisaaButton.appendChild(buttonTeksti);
	paragraphButton.appendChild(lisaaButton);

	fieldset.appendChild(lat);
	fieldset.appendChild(lon);
	fieldset.appendChild(koodi);
	fieldset.appendChild(paragraphButton);

	rastiLomake.appendChild(fieldset);
}

/**
 * Apufunktio, joka tuottaa rasti-inputin
 * @param {string} nimi annettu nimi
 * @returns <p> DOM-elementin, joka sisältää rasti-inputin
 */
function teeRastiInput(nimi) {
	let paragraph = document.createElement("P");
	let label = document.createElement("LABEL");
	let labelNimi = document.createTextNode(nimi);
	let latInput = document.createElement("INPUT");

	latInput.type = "text";
	latInput.value = "";
	latInput.id = nimi;
	latInput.addEventListener("input", e => {
		e.preventDefault();
	});
	label.appendChild(labelNimi);
	label.appendChild(latInput);
	paragraph.appendChild(label);

	return paragraph;
}

/* 
* 
* DATAN KERÄYS- JA MANIPULOINTIFUNKTIOT
* 
*/

// VERTAILUFUNKTIOT

const jarjestaOletus = (a, b) => {
	if (a.sarja > b.sarja) return 1;
	else if (a.sarja < b.sarja) return -1;
	// Sama sarja, joten katsotaan pisteet
	else {
		if (a.pisteet > b.pisteet) return -1;
		else if (a.pisteet < b.pisteet) return 1;
		// Myös samat pisteet - katsotaan nimi
		else {
			return jarjestaJoukkue(a, b);
		}
	}
};

const jarjestaJoukkue = (a, b) => {
	if (a.nimi > b.nimi) return 1;
	else if (a.nimi < b.nimi) return -1;
	else return 0;
};

const jarjestaPisteet = (a, b) => {
	if (a.pisteet > b.pisteet) return -1;
	else if (a.pisteet < b.pisteet) return 1;
	else return 0;
};

const jarjestaMatka = (a, b) => {
	if (a.matka > b.matka) return -1;
	else if (a.matka < b.matka) return 1;
	else return 0;
};

const jarjestaAika = (a, b) => {
	if (a.aika > b.aika) return 1;
	else if (a.aika < b.aika) return -1;
	else return 0;
};

/**
 * Lisää rastileimauksen joukkueelle.
 * Jos leimaus on jo olemassa, muokataan sitä.
 * Jos leimauksen annettua koodia vastaavaa rastia ei löydy, leimausta ei lisätä.
 * @param {Object} [joukkue=null] annettu joukkue
 */
function lisaaRastiLeimausJoukkueelle(joukkue = null) {
	let koodi = document.getElementById("editoiRastinKoodi").value;
	let aika = document.getElementById("editoiRastinAika").value;
	const dropdownAika = document.getElementById("rastiDropdown").value;

	const aikaParsattu = Date.parse(aika);

	// Renderöidään kaikki uusiksi
	function renderoiUusiksi() {
		joukkueet = haeKaikki();
		const uusiJoukkue = joukkueet.find(j => j.id === joukkue.id);
		renderoiJoukkueet(joukkueet);
		lisaaJoukkueLomake(uusiJoukkue);
	}

	// kentät valideja ja joukkue olemassa
	if (koodi.length > 0 && aikaParsattu > 0 && joukkue !== null) {
		// rasti valittu
		if (dropdownAika.length > 1) {
			// etsitään leimauksen indeksi
			const leimausIndeksi = kopio.tupa.findIndex(
				leimaus =>
					leimaus.aika == dropdownAika &&
					leimaus.joukkue == joukkue.id
			);
			// Muutetaan leimauksen tiedot
			if (leimausIndeksi > -1) {
				const loytynytRasti = kopio.rastit.find(
					rasti => rasti.koodi == koodi
				);
				// Koodia vastaava rasti löytyi, muokataan leimausta
				if (loytynytRasti !== undefined) {
					kopio.tupa[leimausIndeksi] = {
						...kopio.tupa[leimausIndeksi],
						aika,
						rasti: loytynytRasti.id
					};
					renderoiUusiksi();
				}
			}
		} else {
			// Rastia ei valittu, lisätään uusi
			const loytynytRasti = kopio.rastit.find(
				rasti => rasti.koodi == koodi
			);
			if (loytynytRasti !== undefined) {
				// Koodia vastaava rasti löytyi, lisätään leimaus
				const uusiRasti = {
					aika,
					joukkue: joukkue.id,
					rasti: loytynytRasti.id
				};
				kopio.tupa.push(uusiRasti);
				renderoiUusiksi();
			}
		}
	}
}

/**
 * Poistaa rastileimauksen joukkueelta ja renderöi tiedot uusiksi
 * @param {*} joukkue annettu joukkue
 */
function poistaRastiLeimausJoukkueelta(joukkue) {
	if (joukkue !== null) {
		let koodi = document.getElementById("editoiRastinKoodi").value;

		// Etsi ja poista koodin rastileimaus, jos löytyy
		if (koodi.length > 0) {
			const { id } = joukkue;

			const indeksi = kopio.tupa.findIndex(rasti => {
				const joukkueenrasti = rasti.joukkue == id;
				if (joukkueenrasti) {
					const tiedot = haeRastinTiedot(parseInt(rasti.rasti));
					if (tiedot !== undefined && tiedot.koodi == koodi)
						return true;
				}
				return false;
			});

			if (indeksi > -1) {
				// Poistetaan globaalista tietorakenteesta
				kopio.tupa.splice(indeksi, 1);
				joukkueet = haeKaikki();
				const uusiJoukkue = joukkueet.find(j => j.id === joukkue.id);
				renderoiJoukkueet(joukkueet);
				lisaaJoukkueLomake(uusiJoukkue);
			}
		}
	}
}

/**
 * Validoi ja lisää rastin
 */
function lisaaRasti() {
	let lat = document.getElementById("lat").value;
	let lon = document.getElementById("lon").value;
	let koodi = document.getElementById("koodi").value;
	const onNumero = parseFloat(lat) && parseFloat(lon);

	if (lat.length > 1 && koodi.length > 1 && lon.length > 1 && onNumero) {
		const uusiRasti = {
			lat,
			lon,
			koodi,
			id: generoiRandom()
		};
		kopio.rastit.push(uusiRasti);
		kopio.rastit.forEach(rasti => {
			const { lat, lon, koodi } = rasti;
			console.log(`rasti koodi ${koodi} lat ${lat} lon ${lon}`);
		});
		alustaKaikki();
	}
}

/**
 * Luo uuden yksittäisen jäsenkentän, esitäytetyillä tiedoilla tai ilman
 * @param {Object} jasen dataobjekti
 * @return jäsen DOM elementin
 */
function jasenLomakeGenerator(jasen = null) {
	let jasenContainer = document.createElement("p");
	jasenContainer.setAttribute("class", "jasenContainer");
	let jasenLabel = document.createElement("label");
	let jasenTeksti = document.createTextNode("Jäsenen nimi");
	jasenLabel.appendChild(jasenTeksti);
	jasenContainer.appendChild(jasenLabel);

	let jasenInput = document.createElement("input");
	jasenInput.type = "text";
	jasenInput.className = "jasenInput";
	jasenInput.addEventListener("input", e => {
		e.preventDefault();
		validoiJoukkueLisays();
	});

	if (jasen !== null && jasen.length > 0) {
		jasenInput.defaultValue = jasen;
	} else jasenInput.defaultValue = "";

	jasenContainer.appendChild(jasenInput);
	return jasenContainer;
}

/**
 * Validoi onko lisättävällä joukkueella tarpeeksi dataa.
 * Pitää myös Lisäysnappulan disabled tilan ajan tasalla.
 * @return {boolean} true jos tarpeeksi dataa, muuten false
 */
function validoiJoukkueLisays() {
	const nimi = document.getElementById("joukkue_nimi");
	const jasenet = document.getElementsByClassName("jasenInput");
	let jasenNimet = [];
	for (const jasen of jasenet) jasenNimet.push(jasen.value);

	const tarpeeksiPitkanimi = nimi.value.length > 2;
	const tarpeeksiJasenia = jasenNimet.every(value => value.length > 2);
	const onOk = tarpeeksiJasenia && tarpeeksiPitkanimi;

	let nappula = document.getElementById("lisaaJoukkue");
	if (onOk) {
		nappula.disabled = false;
		return true;
	} else {
		nappula.disabled = true;
	}
	return false;
}

/**
 * Lisää uuden joukkueen sivulle tai muokkaa olemassaolevaa annettua.
 * @param {Integer} id muokattavan joukkueen id.
 */
function lisaaTaiMuokkaaJoukkuetta(sarja = null, id = null) {
	const nimi = document.getElementById("joukkue_nimi").value;
	const jasenet = document.getElementsByClassName("jasenInput");
	let jasenNimet = [];
	for (const jasen of jasenet) jasenNimet.push(jasen.value);

	if (id !== null && sarja !== null) {
		let sarjaIndeksi = 1;
		if (sarja == "4h") sarjaIndeksi = 0;
		if (sarja == "8h") sarjaIndeksi = 2;

		const indeksi = kopio.sarjat[sarjaIndeksi].joukkueet.findIndex(
			joukkue => joukkue.id == id
		);
		const joukkue = {
			...kopio.sarjat[sarjaIndeksi].joukkueet[indeksi],
			nimi,
			jasenet: jasenNimet
		};
		kopio.sarjat[sarjaIndeksi].joukkueet[indeksi] = joukkue;
	} else {
		const joukkue = {
			nimi,
			last: "",
			jasenet: jasenNimet,
			id: generoiRandom()
		};
		kopio.sarjat[1].joukkueet.push(joukkue);
	}
	alustaKaikki();
}

const generoiRandom = () =>
	Math.floor(Math.random() * 8999999999999999 + 1000000000000000);

/**
 *
 * Hakee joukkueet
 * @returns joukkueobjektin
 */
function haeJoukkueet() {
	let joukkueet = [];
	kopio.sarjat.forEach(sarja => {
		sarja.joukkueet.forEach(joukkue => {
			joukkue.sarja = sarja.nimi;
			joukkueet.push(joukkue);
		});
	});
	return joukkueet;
}

// Hakee kaikkien joukkueiden tiedot, palauttaa taulukon joukkue-objekteista
function haeKaikki() {
	let joukkueet = haeJoukkueet();
	joukkueet.forEach(joukkue => {
		const joukkueenLeimaukset = haeJoukkueenLeimaukset(joukkue.id);
		joukkue.pisteet = joukkueenPisteet(joukkue.nimi, joukkueenLeimaukset);
		// lisätään lat ja lon rastin tietoihin
		const rastit = joukkueenLeimaukset
			.map(rasti => {
				const rastinTiedot = haeRastinTiedot(parseInt(rasti.rasti));

				if (rastinTiedot !== undefined) {
					rasti.lat = rastinTiedot.lat;
					rasti.lon = rastinTiedot.lon;
					rasti.koodi = rastinTiedot.koodi;
				}
				return rasti;
			})
			.filter(yksiRasti => yksiRasti.hasOwnProperty("lat"));
		joukkue.rastit = rastit;
		const aika = laskeAika(rastit);
		joukkue.matka = laskeMatka(rastit);
		joukkue.aika = aika;
	});
	return joukkueet;
}

// Laskee käytetyn kokonaismatkan, pyöristäen alaspäin
function laskeMatka(rastit) {
	let kokoMatka = 0;
	if (rastit.length > 1) {
		for (let i = 1; i < rastit.length; i++) {
			const lat1 = parseFloat(rastit[i - 1].lat);
			const lon1 = parseFloat(rastit[i - 1].lon);
			const lat2 = parseFloat(rastit[i].lat);
			const lon2 = parseFloat(rastit[i].lon);
			const etaisyys = getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2);
			kokoMatka = kokoMatka + etaisyys;
		}
	}
	return Math.floor(kokoMatka);
}

/**
 * Laskee rastien perusteella käytetyn kokonaisajan millisekunneissa.
 * Palauttaa 0 jos aikaa ei löydy tai voida laskea
 * @param {Array<Object>} rastit kaikki joukkueen rastit
 * @returns {Integer} ajan millisekunneissa
 */
function laskeAika(rastit) {
	const lahto = rastit.find(rasti => rasti.koodi == "LAHTO");
	const maali = rastit.find(rasti => rasti.koodi == "MAALI");

	let erotus = 0;
	if (lahto !== undefined && maali !== undefined) {
		const lahtoAika = Date.parse(lahto.aika);
		const maaliAika = Date.parse(maali.aika);
		// Palautetaan 0 jos MAALI aika ennen LAHTO aikaa.
		// ks. esim. joukkue "Sopupeli"
		const tempErotus = maaliAika - lahtoAika;
		if (tempErotus > 0) erotus = tempErotus;
	}
	return erotus;
}

/**
 * Hakee rastin tiedot
 * @param {*} rastiId rastin id
 * @returns tiedot
 */
function haeRastinTiedot(rastiId) {
	return kopio.rastit.find(
		rasti => parseInt(rasti.id) == rastiId && rasti.lat && rasti.lon
	);
}

/**
 * Hakee joukkueen kaikki rastit joille löytyy vastine
 * @param {int} joukkueId joukkueen id
 * @return leimauksen jossa kentät aika, rasti, joukkue
 */
function haeJoukkueenLeimaukset(joukkueId) {
	const rastit = kopio.tupa.filter(rasti => rasti.joukkue === joukkueId);
	const lahto = rastit.find(rasti => rasti.koodi == "LAHTO");
	const maali = rastit.find(rasti => rasti.koodi == "MAALI");
	// Varmistetaan, että kaikki rastit LÄHTÖ ja MAALI välissä ajallisesti
	let rajattu = rastit;
	if (lahto !== undefined && maali !== undefined) {
		rajattu = rastit.filter(rasti => {
			const lahtoParsattu = Date.parse(lahto.aika);
			const maaliParsattu = Date.parse(maali.aika);

			// Rastin aika
			const { aika } = rasti;
			const aikaParsattu = Date.parse(aika);

			// Jos aika LÄHTÖ ja MAALI välissä päästä läpi
			if (aikaParsattu >= lahtoParsattu && aikaParsattu <= maaliParsattu)
				return true;
			return false;
		});
	}

	return rajattu;
}

/**
 * Hakee joukkueen kaikki pisteet. Samasta rastista ei saa pisteitä kahdesti.
 * Leimausten pitää olla LAHTO ja MAALI välissä.
 * Jos ei LAHTO ja MAALI leimausta, ei pisteitä.
 * @param {Object} joukkue joukkue
 * @param {Array<Object>} leimaukset joukkueen leimaukset
 * @returns {Integer} pistemäärä
 */
function joukkueenPisteet(joukkue, leimaukset) {
	let pisteet = 0;
	let leimatut = [];
	const lahto = leimaukset.find(leimaus => {
		const { rasti } = leimaus;
		const tiedot = haeRastinTiedot(rasti);
		if (tiedot !== undefined) return tiedot.koodi == "LAHTO";
		return false;
	});
	const maali = leimaukset.find(leimaus => {
		const { rasti } = leimaus;
		const tiedot = haeRastinTiedot(rasti);
		if (tiedot !== undefined) return tiedot.koodi == "MAALI";
		return false;
	});
	for (const leimaus of leimaukset) {
		const rastiTunnus = leimaus.rasti;
		const rastinPisteet = etsiPisteet(rastiTunnus);
		if (rastinPisteet && !leimatut.includes(rastiTunnus)) {
			const leimausAika = Date.parse(leimaus.aika);
			if (lahto !== undefined && maali !== undefined) {
				const lahtoAika = Date.parse(lahto.aika);
				const maaliAika = Date.parse(maali.aika);
				if (leimausAika > lahtoAika && leimausAika < maaliAika) {
					pisteet = pisteet + rastinPisteet;
					leimatut.push(rastiTunnus);
				}
			}
		}
	}
	return pisteet;
}

// Apufunktiot
function vertaa5(a, b) {
	const erotus = b.pisteet - a.pisteet;
	if (erotus !== 0) return erotus;
	else return a.aika - b.aika;
}

function millisekunnitAjaksi(kesto) {
	var seconds = parseInt((kesto / 1000) % 60),
		minutes = parseInt((kesto / (1000 * 60)) % 60),
		hours = parseInt((kesto / (1000 * 60 * 60)) % 24);

	hours = hours < 10 ? "0" + hours : hours;
	minutes = minutes < 10 ? "0" + minutes : minutes;
	seconds = seconds < 10 ? "0" + seconds : seconds;

	const pad = (num, koko) => ("000" + num).slice(koko * -1);
	return hours + ":" + pad(minutes, 2) + ":" + pad(seconds, 2);
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
	var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2 - lat1); // deg2rad below
	var dLon = deg2rad(lon2 - lon1);
	var a =
		Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.cos(deg2rad(lat1)) *
			Math.cos(deg2rad(lat2)) *
			Math.sin(dLon / 2) *
			Math.sin(dLon / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c; // Distance in km
	return d;
}

function deg2rad(deg) {
	return deg * (Math.PI / 180);
}

/**
 * Listaa kaikki joukkueet taulukkona
 * laskevassa pistejärjestyksessä
 * @returns {Array<Object>} joukkueet
 */
function joukkueidenPisteet() {
	let joukkueet = [];
	for (let i = 0; i < kopio.sarjat.length; i++) {
		for (let j = 0; j < kopio.sarjat[i].joukkueet.length; j++) {
			let nimi = kopio.sarjat[i].joukkueet[j].nimi;
			let pisteet = joukkueenPisteet(nimi);
			let joukkue = {
				nimi: nimi,
				pisteet: pisteet
			};
			joukkueet.push(joukkue);
		}
	}
	return joukkueet.sort(vertaa);
}

// Apufunktio, jota array.sort käyttää vertailussa määreenä
function vertaa(a, b) {
	return b.pisteet - a.pisteet;
}

// Etsii joukkueen ID -tunnuksen
// Palauttaa null, jos tunnusta ei löydy
function etsiTunnus(joukkue) {
	let tunnus = null;
	for (let i = 0; i < kopio.sarjat.length; i++) {
		for (let j = 0; j < kopio.sarjat[i].joukkueet.length; j++) {
			if (kopio.sarjat[i].joukkueet[j].nimi === joukkue) {
				tunnus = kopio.sarjat[i].joukkueet[j].id;
			}
		}
	}
	return tunnus;
}

// Etsii rastin pisteet.
// Palauttaa null, jos rastia ei löydy
function etsiPisteet(rastiTunnus) {
	let pisteet = null;
	let tunnus = parseInt(rastiTunnus);
	// Jos annettu tunnus on validi numero
	if (tunnus) {
		for (let i = 0; i < kopio.rastit.length; i++) {
			// Jos löytyy tunnusta vastaava rasti
			if (kopio.rastit[i].id === tunnus) {
				let ekaMerkki = kopio.rastit[i].koodi.charAt(0);
				let ekaNumero = parseInt(ekaMerkki);
				// Jos ensimmäinen merkki on numero:
				if (ekaNumero) pisteet = ekaNumero;
			}
		}
	}
	return pisteet;
}
