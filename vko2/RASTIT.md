# Rastilogiikka
* tupa-tietorakenteessa on listattu kaikki joukkueiden leimaamat rastit.
* Rakenteesta löytyy aika jolloin rastilla on käyty ja rastin sekä joukkueen tunniste, jotka vastaavat aina jonkin rastin tai joukkueen id-attribuutin arvoa.

## Rastit:
* Huom. joillakin tupa-rakenteessa oleville tunnisteille ei löydy vastinetta rasteista tai joukkueista. Nämä leimaukset jätetään huomiotta. 
* Jos leimauksia on tehty ennen alkuaikaa tai ennen LAHTO-rastin leimausta tai MAALI-rastin leimaamisen jälkeen niin myös nämä jätetään huomioimatta.

## Pisteet:
* Joukkue saa kustakin rastista pisteitä rastin koodin ensimmäisen merkin verran. Esim. jos rastin koodi on 9A niin joukkue saa yhdeksän (9) pistettä. Jos rastin koodin ensimmäinen merkki ei ole kokonaisluku niin kyseisestä rastista saa nolla (0) pistettä. Esim. rasteista LÄHTÖ ja F saa 0 pistettä.
* Samasta rastista voi saada pisteitä vain yhden kerran. Jos leimauksista löytyy useampaan kertaan esim. rasti 77 niin joukkue saa kuitenkin vain yhden kerran seitsemän (7) pistettä.


## Plan:
kaikki joukkueen rastit
    |> filter välille ALKU-LOPPU
    |> filter vain kerran per rasti

aika:
    |> redusoi aika

matka:
    |> redusoi matka

pisteet:
    |> filter ensimmäinen merkki kokonaisluku
    |> redusoi pisteet