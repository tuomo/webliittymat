// voit tutkia tarkemmin käsiteltäviä tietorakenteita konsolin kautta
// tai json-editorin kautta osoitteessa http://jsoneditoronline.org/
// Jos käytät json-editoria niin avaa data osoitteesta:
// http://appro.mit.jyu.fi/tiea2120/vt/vt1/2018/data.json

// Seuraavilla voit tutkia käytössäsi olevaa tietorakennetta. Voit ohjelmallisesti
// vapaasti muuttaa selaimen muistissa olevan rakenteen sisältöä ja muotoa.

// console.log(data);

// console.dir(data);

// Kirjoita tästä eteenpäin oma ohjelmakoodis

"use strict";

/*
* TASO 1
*/

console.log(`--------------------TASO 1---------------------`);

// Tulostaa kaikkien joukkueiden nimet globaalista dataobjektista.
// Tulostaa myös "Vara" -joukkueet.
function tulostaNimet() {
	// Käydään läpi kummatkin taulukot, sarjat ja joukkueet
	for (let i = 0; i < data.sarjat.length; i++) {
		for (let j = 0; j < data.sarjat[i].joukkueet.length; j++) {
			console.log(data.sarjat[i].joukkueet[j].nimi);
		}
	}
}

// Määritellään uusi lisättävä joukkue
const uusiJoukkue = {
	nimi: "Mallijoukkue",
	last: "2017-09-01 10:00:00",
	jasenet: ["Tommi Lahtonen", "Matti Meikäläinen"],
	id: 99999
};

// Lisää parametrina annetun joukkueen parametrina annettuun sarjaan,
// jos sellainen olemassa.
function lisaaJoukkue(joukkue, sarja) {
	for (let i = 0; i < data.sarjat.length; i++) {
		// Jos parametrina annetun sarjan nimi täsmää data -objektissa olevan sarjan nimen kanssa
		if (data.sarjat[i].nimi === sarja) {
			// Lisätään uusi joukkue taulukkoon
			data.sarjat[i].joukkueet.push(joukkue);
		}
	}
}

// Tulostaa rastien koodit, joiden ensimmäinen merkki on kokonaisluku.
function tulostaRastit() {
	// Käy rastit -taulukon läpi
	let koodit = [];
	for (let i = 0; i < data.rastit.length; i++) {
		let koodi = data.rastit[i].koodi;
		let ekaMerkki = koodi.charAt(0);
		// Kun koodin ensimmäinen merkki on kokonaisluku,
		// lisää taulukkoon koodit.
		if (parseInt(ekaMerkki)) koodit.push(koodi);
	}
	console.log(koodit.join(";"));
}

// Lisätään joukkue
lisaaJoukkue(uusiJoukkue, "4h");

// Tulostaa joukkueet, sitten rastit
tulostaNimet();
tulostaRastit();

console.log(`--------------------TASO 3---------------------`);

/*
* TASO 3
*/

// Poistaa joukkueen nimen perusteella
function poistaJoukkue(joukkue) {
	for (let i = 0; i < data.sarjat.length; i++) {
		for (let j = 0; j < data.sarjat[i].joukkueet.length; j++) {
			if (data.sarjat[i].joukkueet[j].nimi === joukkue) {
				// Poistetaan joukkue taulukosta joukkueet, jos nimi täsmää
				data.sarjat[i].joukkueet.splice(j, 1);
			}
		}
	}
}

// Listataan poistettavat joukkueet
const poistettavatJoukkueet = ["Vara 1", "Vara 2", "Vapaat"];

// Parametrina callback, joka hoitaa poistamisen
poistettavatJoukkueet.forEach(function(joukkue) {
	poistaJoukkue(joukkue);
});

// Tulostaa kaikki joukkueet pisteineen pistejärjestyksessä
function tulostaKaikki() {
	const kaikki = joukkueidenPisteet();
	kaikki.forEach(function(joukkue) {
		console.log(`${joukkue.nimi} (${joukkue.pisteet} p)`);
	});
}

// Listaa kaikki joukkueet taulukkona
// laskevassa pistejärjestyksessä
function joukkueidenPisteet() {
	let joukkueet = [];
	for (let i = 0; i < data.sarjat.length; i++) {
		for (let j = 0; j < data.sarjat[i].joukkueet.length; j++) {
			let nimi = data.sarjat[i].joukkueet[j].nimi;
			let pisteet = joukkueenPisteet(nimi);
			let joukkue = {
				nimi: nimi,
				pisteet: pisteet
			};
			joukkueet.push(joukkue);
		}
	}
	return joukkueet.sort(vertaa);
}

// Apufunktio, jota array.sort käyttää vertailussa määreenä
function vertaa(a, b) {
	return b.pisteet - a.pisteet;
}

// Etsii joukkueen kokonaispistemäärän
// jos samalla rastilla käyty useamman kerran,
// antaa aina uudet lisäpisteet rastilta
function joukkueenPisteet(joukkue) {
	let pisteet = 0;
	// Etsitään joukkueen tunnus
	const tunnus = etsiTunnus(joukkue);
	// pidetään yllä listaa jo leimatuista
	let leimatut = [];
	for (let i = 0; i < data.tupa.length; i++) {
		// Etsitään iteroitavan rastin pisteet
		// Muutetaan rastitunnus ensiksi numeroiksi
		let rastiTunnus = data.tupa[i].rasti;
		let rastinPisteet = etsiPisteet(rastiTunnus);
		// Jos iteroitava rasti vastaa tunnusta,
		// ja rastille löytyy pisteet
		// ja rastilta ei vielä ole merkitty pisteitä
		if (
			data.tupa[i].joukkue === tunnus &&
			rastinPisteet &&
			!leimatut.includes(rastiTunnus)
		) {
			pisteet = pisteet + rastinPisteet;
			leimatut.push(rastiTunnus);
		}
	}
	return pisteet;
}

// Etsii joukkueen ID -tunnuksen
// Palauttaa null, jos tunnusta ei löydy
function etsiTunnus(joukkue) {
	let tunnus = null;
	for (let i = 0; i < data.sarjat.length; i++) {
		for (let j = 0; j < data.sarjat[i].joukkueet.length; j++) {
			if (data.sarjat[i].joukkueet[j].nimi === joukkue) {
				tunnus = data.sarjat[i].joukkueet[j].id;
			}
		}
	}
	return tunnus;
}

// Etsii rastin pisteet.
// Palauttaa null, jos rastia ei löydy
function etsiPisteet(rastiTunnus) {
	let pisteet = null;
	let tunnus = parseInt(rastiTunnus);
	// Jos annettu tunnus on validi numero
	if (tunnus) {
		for (let i = 0; i < data.rastit.length; i++) {
			// Jos löytyy tunnusta vastaava rasti
			if (data.rastit[i].id === tunnus) {
				let ekaMerkki = data.rastit[i].koodi.charAt(0);
				let ekaNumero = parseInt(ekaMerkki);
				// Jos ensimmäinen merkki on numero:
				if (ekaNumero) pisteet = ekaNumero;
			}
		}
	}
	return pisteet;
}

// Suoritetaan kaikki joukkueet pistejärjestyksessä tulostava funktio
tulostaKaikki();

/*
* TASO 5
*/

console.log(`--------------------TASO 5---------------------`);

/**
 *
 * Hakee joukkueet
 * @returns joukkueobjektin
 */
function haeJoukkueet() {
	let joukkueet = [];
	data.sarjat.forEach(sarja => {
		sarja.joukkueet.forEach(joukkue => {
			joukkueet.push(joukkue);
		});
	});
	return joukkueet;
}

// Hakee kaikkien joukkueiden tiedot, palauttaa taulukon joukkue-objekteista
function haeKaikki() {
	let joukkueet = haeJoukkueet();
	joukkueet.forEach(joukkue => {
		joukkue.pisteet = joukkueenPisteet(joukkue.nimi);
		const joukkueenRastit = haeJoukkueenRastit(joukkue.id);
		// lisätään lat ja lon rastin tietoihin
		const rastit = joukkueenRastit
			.map(rasti => {
				const rastinTiedot = haeRastinTiedot(parseInt(rasti.rasti));

				if (rastinTiedot !== undefined) {
					rasti.lat = rastinTiedot.lat;
					rasti.lon = rastinTiedot.lon;
					rasti.koodi = rastinTiedot.koodi;
				}
				return rasti;
			})
			.filter(yksiRasti => yksiRasti.hasOwnProperty("lat"));
		joukkue.rastit = rastit;
		const aika = laskeAika(rastit);
		joukkue.matka = laskeMatka(rastit);
		joukkue.aika = aika;
	});
	return joukkueet;
}

// Laskee käytetyn kokonaismatkan, pyöristäen alaspäin
function laskeMatka(rastit) {
	let kokoMatka = 0;
	if (rastit.length > 1) {
		for (let i = 1; i < rastit.length; i++) {
			const lat1 = parseFloat(rastit[i - 1].lat);
			const lon1 = parseFloat(rastit[i - 1].lon);
			const lat2 = parseFloat(rastit[i].lat);
			const lon2 = parseFloat(rastit[i].lon);
			// console.log(`lat1 lon1 lat2 lon2`, lat1, lon1, lat2, lon2);
			const etaisyys = getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2);
			// console.log(`etaisyys`, etaisyys);
			kokoMatka = kokoMatka + etaisyys;
		}
	}
	return Math.floor(kokoMatka);
}

// Laskee rastien perusteella käytetyn kokonaisajan millisekunneissa.
// Palauttaa 0 jos aikaa ei löydy tai voida laskea
function laskeAika(rastit) {
	const lahto = rastit.find(rasti => rasti.koodi == "LAHTO");
	const maali = rastit.find(rasti => rasti.koodi == "MAALI");

	let erotus = 0;
	if (lahto !== undefined && maali !== undefined) {
		const lahtoAika = Date.parse(lahto.aika);
		const maaliAika = Date.parse(maali.aika);
		// Palautetaan 0 jos MAALI aika ennen LAHTO aikaa.
		// ks. esim. joukkue "Sopupeli"
		const tempErotus = maaliAika - lahtoAika;
		if (tempErotus > 0) erotus = tempErotus;
	}
	return erotus;
}

function haeRastinTiedot(rastiId) {
	return data.rastit.find(
		rasti => parseInt(rasti.id) == rastiId && rasti.lat && rasti.lon
	);
}

function haeJoukkueenRastit(joukkueId) {
	let rastit = [];
	data.tupa.forEach(rasti => {
		if (rasti.joukkue === joukkueId) rastit.push(rasti);
	});
	return rastit;
}

function tulostaKaikkiJarjestyksessa() {
	const joukkueetTietoineen = haeKaikki();
	const lajiteltu = joukkueetTietoineen.sort(vertaa5);
	lajiteltu.forEach(joukkue => {
		const { nimi, pisteet, matka, aika } = joukkue;
		const kesto = millisekunnitAjaksi(aika);
		// Yksittäisen joukkueen tietojen tulostus
		console.log(`${nimi}, ${pisteet} p, ${matka} km, ${kesto}`);
	});
}

tulostaKaikkiJarjestyksessa();

// Apufunktiot

// Vertailulajittelufunktio 5-tasolle
function vertaa5(a, b) {
	const erotus = b.pisteet - a.pisteet;
	if (erotus !== 0) return erotus;
	else return a.aika - b.aika;
}

function millisekunnitAjaksi(kesto) {
	var seconds = parseInt((kesto / 1000) % 60),
		minutes = parseInt((kesto / (1000 * 60)) % 60),
		hours = parseInt((kesto / (1000 * 60 * 60)) % 24);

	hours = hours < 10 ? "0" + hours : hours;
	minutes = minutes < 10 ? "0" + minutes : minutes;
	seconds = seconds < 10 ? "0" + seconds : seconds;

	return hours + ":" + minutes + ":" + seconds;
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
	var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2 - lat1); // deg2rad below
	var dLon = deg2rad(lon2 - lon1);
	var a =
		Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.cos(deg2rad(lat1)) *
			Math.cos(deg2rad(lat2)) *
			Math.sin(dLon / 2) *
			Math.sin(dLon / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c; // Distance in km
	return d;
}

function deg2rad(deg) {
	return deg * (Math.PI / 180);
}
