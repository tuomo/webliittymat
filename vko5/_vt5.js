"use strict";

$(document).ready(function() {
	initMap();
	createLists();
});

//#region Kartta

var map;
var circles;
var lines = {}; // Rastien väliset viivat, avaimina joukkue-id:t
var moveMarker;

// Alustaa kartalle tarvittavat tiedot.
function initMap() {
	map = new L.map("map", {
		crs: L.TileLayer.MML.get3067Proj()
	}).setView([0, 0], 8);
	L.tileLayer.mml_wmts({ layer: "maastokartta" }).addTo(map);

	initMoveMarker();

	drawCheckpoints();
	// Keskitetään kartta
	map.fitBounds(circles.getBounds());
}

// Piirtää tietorakenteesta löytyvät rastit ympyröinä kartalle.
function drawCheckpoints() {
	let cps = [];
	for (let cp of data.rastit) {
		let c = L.circle([cp.lat, cp.lon], {
			color: "red",
			opacity: 0.75,
			fillOpacity: 0,
			radius: 150
		});

		// Tallennetaan ympyrän yhteyteen osoitin rastiin, niin sitä on helppo muokata myöhemmin
		c.data = cp;

		// Klikattaessa siirretään markkeri kohdalle
		c.addEventListener("click", function(evt) {
			moveMarker.addTo(map);
			moveMarker.setLatLng(c.getLatLng());
			c.setStyle({ fillOpacity: 0.75 });
			if (moveMarker.circle && moveMarker.circle !== c) {
				moveMarker.circle.setStyle({ fillOpacity: 0 }); // Täyttö pois edellisestä valitusta ympyrästä
			}

			// Tallennetaan markkeriin osoitin tähän ympyrään
			moveMarker.circle = c;
		});

		cps.push(c);
	}
	circles = new L.featureGroup(cps);

	circles.addTo(map);
}

// Piirtää kartalle viivan joukkueelle tai nostaa päällimmäiseksi, jos se on jo olemassa
function drawLine(team, color) {
	let line = lines[team.id];
	if (line) {
		// Jos viiva on jo olemassa, niin lisätään se vain uudelleen
		// Tehdään näin eikä bringToFront(), koska viiva ei välttämättä ole kartalla
		line.remove();
		line.addTo(map);
	} else {
		// Viivaa ei ole tehty vielä, piirretään se

		let line = L.polyline(getCheckpointCoords(team), {
			color: color
		}).addTo(map);
		// Tallennetaan viiva, niin ei tarvitse enää generoida sitä uudestaan
		lines[team.id] = line;
	}
}

// Poistaa viivan kartalta
function eraseLine(team) {
	let line = lines[team.id];
	if (line) {
		line.remove();
	}
}

// Alustaa rastinsiirtomarkkerin
function initMoveMarker() {
	moveMarker = L.marker([0, 0], {
		title: "raahaa siirtääksesi rastia",
		draggable: true
	});

	moveMarker.on("dragend", function(evt) {
		// Siirretään ympyrä
		let pos = moveMarker.getLatLng();
		moveMarker.circle.setLatLng(pos);

		// Päivitetään rasti tietorakenteeseen
		moveMarker.circle.data.lat = pos.lat;
		moveMarker.circle.data.lon = pos.lng;

		// Päivitetään kartalla olevat viivat
		redrawLines();
		updateDistances();

		// Piilotetaan siirtomarkkeri
		hideMarker();
	});

	map.on("click", function(evt) {
		// Piilotetaan markkeri kun klikataan kartan pohjaa
		if (evt.originalEvent.target === evt.originalEvent.currentTarget) {
			hideMarker();
		}
	});
}

// Piilottaa markkerin.
function hideMarker() {
	moveMarker.remove();
	if (moveMarker.circle) {
		// Väri pois valitusta ympyrästä
		moveMarker.circle.setStyle({ fillOpacity: 0 }); // ympyrä takaisin pimeäksi
		moveMarker.circle = null;
	}
}

// Piirtää kaikki viivat kokonaan uusiksi. Melko tehoton bruteforce-keino, mutta helppo ja nopea toteuttaa
function redrawLines() {
	for (let id in lines) {
		redrawLine(id);
	}
}

// Piirtää yksittäisen, annettua joukkueId:tä vastaavan viivan uudelleen
function redrawLine(teamId) {
	if (lines[teamId]) {
		// Jos joukkueelle ei ole vielä tehty viivaa, niin ei tarvitse tehdä mitään
		let team = data.joukkueet.find(function(el) {
			return el.id == teamId;
		});
		lines[teamId].setLatLngs(getCheckpointCoords(team));
	}
}

//#endregion Kartta

//#region Listat

function haeRastit(joukkue) {}

/**
 * Tekee joukkuelistauksen.
 * Lisää rastit joukkueille, mutta vain jos lähtö- ja maalirastin välisenä aikana leimattu.
 * Jos lähtörastia ei ole, ei näytetä mitään koska joukkue hylätty.
 * Jos maalirasti leimattu ennen lähtörastia, ei näytä lainkaan leimauksia koska joukkue hylätty.
 * Ks. esim. "Sopupeli" -joukkue esimerkkinä edellisestä.
 *
 */
function createLists() {
	let list = $("#joukkueet");
	for (let i in data.joukkueet) {
		// `<li id="team${i}" class="liTeam"></li>`;
		let li = $(`<li id="team${i}" class="liTeam"></li>`);
		let details = $("<details></details>");
		details.appendTo(li);
		// Tehdään vain otsikosta raahattava, mutta sitä siirtämällä siirtyy koko lista-alkio
		const { nimi } = data.joukkueet[i];
		let summary = $(
			`<summary class="team" draggable>${nimi} ( <span></span> km )</summary>`
			// '<summary class="team" draggable="true">' +
			// 	data.joukkueet[i].nimi +
			// 	" ( <span></span> km )</summary>"
		);
		summary.appendTo(details);

		const color = rainbow(data.joukkueet.length, i);
		li.css("background-color", color);
		li.data("team", data.joukkueet[i]); // Tallennetaan joukkue elementtiin niin sen käsittely on helppoa myöhemmin
		li.appendTo(list);

		// Rastiluettelo joukkueen allje
		let cpList = $('<ul class="cpList"></ul>');
		let stamped = []; // Pidetään kirjaa, ettei päästetä duplikaattileimauksia mukaan
		let rastitJaAjat = []; // listaus jossa myös ajat
		const lahtoId = data.rastit.find(r => r.koodi == "LAHTO").id;
		const maaliId = data.rastit.find(r => r.koodi == "LAHTO").id;
		const lahtoRasti = data.joukkueet[i].rastit.find(
			rs => parseInt(rs.id) == lahtoId
		);
		const maaliRasti = data.joukkueet[i].rastit.find(
			rs => rs.id == maaliId
		);
		let lahtoAika, maaliAika;
		if (lahtoRasti !== undefined) lahtoAika = new Date(lahtoRasti.aika);
		if (maaliRasti !== undefined) maaliAika = new Date(maaliRasti.aika);
		console.log(`lahtoAika`, lahtoRasti, i, data.joukkueet[i].nimi);
		// Tähän väliin laita if lähtöaikaa ei ole älä lisää mitään
		// else lisää
		for (let leimaus in data.joukkueet[i].rastit) {
			const { id, aika } = data.joukkueet[i].rastit[leimaus];
			if (!stamped.includes(id)) {
				const realCp = data.rastit.find(el => el.id == id);
				const rastinAika = new Date(aika);
				const lahdonJaMaalinValissa =
					lahtoAika &&
					maaliAika &&
					lahtoAika <= maaliAika &&
					rastinAika >= lahtoAika &&
					rastinAika <= maaliAika;
				if (realCp && lahdonJaMaalinValissa) {
					const sisalto = `<li id="cp${i}-${leimaus}" class="checkpoint" draggable="true">${
						realCp.koodi
					}</li>`;
					let cpLi = $(sisalto);
					cpLi.data("cp", data.joukkueet[i].rastit[leimaus]); // Tallennetaan osoitin tietorakenteeseen uudelleenjärjestämistä varten
					rastitJaAjat.push({ aika: rastinAika, elem: cpLi });
					// cpLi.appendTo(cpList);
				}

				stamped.push(id);
			}
		}
		const sorted = rastitJaAjat.sort((r1, r2) =>
			r1.rastinAika > r2.rastinAika ? -1 : 1
		);
		sorted
			.map(rasti => rasti.elem)
			.forEach(elem => {
				elem.appendTo(cpList);
			});
		cpList.appendTo(details);
	}

	updateDistances();

	initDragNDrop();
}

// Lisää drag and dropiin tarvittavat kuuntelijat (ilman JQueryä)
function initDragNDrop() {
	// Joukkue-elementit
	for (let team of document.getElementsByClassName("team")) {
		team.addEventListener("dragstart", function(evt) {
			evt.dataTransfer.setData(
				"text/plain",
				team.parentElement.parentElement.id
			);
			evt.dataTransfer.effectAllowed = "move";
		});
	}

	// Joukkueiden pudotusalueet
	let onmap = document.getElementById("onmap");

	onmap.addEventListener("dragover", doNothing);

	onmap.addEventListener("drop", function(evt) {
		evt.preventDefault();
		let id = evt.dataTransfer.getData("text");
		let li = document.getElementById(id);
		if (!li) return; // On vedetty jotain mitä ei saisi

		if (li.className === "liTeam") {
			// Jos joukkue, niin laitetaan se listaan ja piirretään viiva
			onmap.insertBefore(li, onmap.firstChild);
			drawLine($(li).data("team"), $(li).css("background-color"));
		} else if (li.className === "checkpoint") {
			// Jos rasti, niin siirretään sitä omassa listassaan
			handleCheckpointDrop(li, evt.target);
		}
	});

	let teams = document.getElementById("joukkueet");

	teams.addEventListener("dragover", doNothing);

	teams.addEventListener("drop", function(evt) {
		evt.preventDefault();
		let id = evt.dataTransfer.getData("text");
		let li = document.getElementById(id);
		if (!li) return; // On vedetty jotain mitä ei saisi
		// Ei anneta vedellä tavaraa joukkuelistan sisällä
		if (li.className === "liTeam" && li.parentElement.id != "joukkueet") {
			teams.appendChild(li);
			eraseLine($(li).data("team"));
		} else if (li.className === "checkpoint") {
			// Jos rasti, niin lisätään oman listansa viimeiseksi
			handleCheckpointDrop(li, evt.target);
		}
	});

	// Ei tehdä mitään dragOver-eventissä
	function doNothing(evt) {
		evt.preventDefault();
	}

	// Rastit
	for (let cp of document.getElementsByClassName("checkpoint")) {
		cp.addEventListener("dragstart", function(evt) {
			evt.dataTransfer.setData("text/plain", cp.id);
			evt.dataTransfer.effectAllowed = "move";
		});
	}

	// Siirtää rastia listansa sisällä, jos kohteena on toinen lista-alkio niin sen päälle ja muuten viimeiseksi.
	function handleCheckpointDrop(li, target) {
		let list = li.parentElement;
		// Jos ollaan samassa listassa niin laitetaan kohteen päälle
		if (
			target.className === "checkpoint" &&
			target.parentElement.id === list.id
		) {
			list.insertBefore(li, target);
		} else {
			// Muuten viimeiseksi
			list.appendChild(li);
		}

		reorderCheckpoints(list.parentElement.parentElement, list);
	}
}

// Järjestää rastit uudelleen tietorakenteessa listan perusteella
function reorderCheckpoints(liTeam, list) {
	let team = $(liTeam).data("team");
	team.rastit = []; // Täytetään taulukko kokonaan uudestaan
	for (let cp of list.children) {
		team.rastit.push($(cp).data("cp"));
	}

	// Viiva uusiksi jos se on kartalla
	redrawLine(team.id);

	updateDistance(liTeam);
}

// Hakee kaikkien joukkueen käymien rastien koordinaatit taulukkoon
function getCheckpointCoords(team) {
	let coords = [];
	for (let cp of team.rastit) {
		let realCp = data.rastit.find(function(el) {
			return el.id == cp.id;
		});
		if (realCp) {
			coords.push([realCp.lat, realCp.lon]);
		}
	}
	return coords;
}

// Laskee matkat kaikille joukkueille ja päivittää ne listaan
function updateDistances() {
	$("li.liTeam").each(function() {
		updateDistance($(this));
	});
}

// Laskee matkan yksittäiselle joukkueelle ja päivittää sen listaan
function updateDistance(li) {
	const sisalto = $(li).data("team");
	console.log(`sisalto`, sisalto);
	let dist = distance(sisalto);
	$(li)
		.find("span")
		.text(dist.toFixed(1));
}

// Laskee joukkueen kulkeman matkan
function distance(team) {
	let dist = 0;
	let cps = getCheckpointCoords(team);
	for (let i = 1; i < cps.length; i++) {
		dist += getDistanceFromLatLonInKm(
			cps[i - 1][0],
			cps[i - 1][1],
			cps[i][0],
			cps[i][1]
		);
	}

	return dist;
}

// Ekasta viikkotehtävästä
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
	var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2 - lat1); // deg2rad below
	var dLon = deg2rad(lon2 - lon1);
	var a =
		Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.cos(deg2rad(lat1)) *
			Math.cos(deg2rad(lat2)) *
			Math.sin(dLon / 2) *
			Math.sin(dLon / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c; // Distance in km
	return d;
}

function deg2rad(deg) {
	return deg * (Math.PI / 180);
}

// Värit
function rainbow(numOfSteps, step) {
	// This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
	// Adam Cole, 2011-Sept-14
	// HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
	let r, g, b;
	let h = step / numOfSteps;
	let i = ~~(h * 6);
	let f = h * 6 - i;
	let q = 1 - f;
	switch (i % 6) {
		case 0:
			r = 1;
			g = f;
			b = 0;
			break;
		case 1:
			r = q;
			g = 1;
			b = 0;
			break;
		case 2:
			r = 0;
			g = 1;
			b = f;
			break;
		case 3:
			r = 0;
			g = q;
			b = 1;
			break;
		case 4:
			r = f;
			g = 0;
			b = 1;
			break;
		case 5:
			r = 1;
			g = 0;
			b = q;
			break;
	}
	let c =
		"#" +
		("00" + (~~(r * 255)).toString(16)).slice(-2) +
		("00" + (~~(g * 255)).toString(16)).slice(-2) +
		("00" + (~~(b * 255)).toString(16)).slice(-2);
	return c;
}
