"use strict";

var map;
var tooltips;
var circles;
var moveMarker;
var viivat = {};

$(document).ready(() => {
	alustaKartta();
	luoJoukkueListaus();
});

/**
 * Alustaa kartan
 */
function alustaKartta() {
	map = new L.map("map", {
		crs: L.TileLayer.MML.get3067Proj()
	}).setView([0, 0], 9);
	L.tileLayer.mml_wmts({ layer: "maastokartta" }).addTo(map);

	alustaRastinSiirtaja();

	piirraRastitKartalle();

	map.fitBounds(circles.getBounds());
}

/**
 * Piirretään rastit kartalle
 */
function piirraRastitKartalle() {
	let kaikki = [];
	for (let rasti of data.rastit) {
		const { lat, lon, koodi } = rasti;
		let circle = L.circle([lat, lon], {
			color: "red",
			opacity: 0.75,
			fillOpacity: 0,
			radius: 150
		});

		// Haetaan tooltipin (rastin) koordinaatit; 200 m etelään ympyrän keskipisteestä
		const latlng = L.latLng(lat, lon);
		const bounds = latlng.toBounds(400);
		const south = bounds.getSouth();
		const newLatlng = L.latLng(south, lon);

		// data talteen
		circle.data = rasti;
		circle.addEventListener("click", evt => {
			moveMarker.addTo(map);
			moveMarker.setLatLng(circle.getLatLng());
			circle.setStyle({ fillOpacity: 0.75 });
			if (moveMarker.circle && moveMarker.circle !== circle) {
				moveMarker.circle.setStyle({ fillOpacity: 0 });
			}
			moveMarker.circle = circle;
		});

		kaikki.push(circle);
		// Lisätään rastin koodi näkymään
		circle.bindTooltip(koodi, {
			opacity: 0.8,
			direction: "bottom",
			permanent: true
		});
		circle.addTo(map);
		circle.openTooltip(newLatlng);
	}
	circles = new L.featureGroup(kaikki); // jotta voidaan keskittää tämän perusteella suoraan
}

/**
 * Piirretään joukkueelle viiva.
 * Jos olemassa, nostetaan päällimmäiseksi
 * @param {*} joukkue joukkue
 * @param {*} color väri
 */
function piirraViiva(joukkue, color) {
	let viiva = viivat[joukkue.id];
	if (viiva) {
		viiva.remove();
		viiva.addTo(map);
	} else {
		const koords = haeJoukkueenRastienKoordinaatit(joukkue);
		let viiva = L.polyline(koords, {
			color
		}).addTo(map);
		viivat[joukkue.id] = viiva;
	}
}

/**
 * Poistaa viivan joukkueelta kartalta
 * @param {*} joukkue annettu joukkue
 */
function poistaViiva(joukkue) {
	let viiva = viivat[joukkue.id];
	if (viiva) {
		viiva.remove();
	}
}

/**
 * Alustaa markerin
 */
function alustaRastinSiirtaja() {
	moveMarker = L.marker([0, 0], {
		title: "raahaa rastia",
		draggable: true
	});

	// jos klikkaus kartalla tyhjässä kohti poistetaan markkeri
	map.on("click", evt => {
		if (evt.originalEvent.target === evt.originalEvent.currentTarget) {
			piilotaMarkkeri();
		}
	});

	// Kun raahauksesta päästetään irti
	moveMarker.on("dragend", evt => {
		let sijainti = moveMarker.getLatLng();
		moveMarker.circle.setLatLng(sijainti);
		moveMarker.circle.data.lat = sijainti.lat;
		moveMarker.circle.data.lon = sijainti.lng;

		uudelleenPiirraKaikki();
		paivitaEtaisyydet();
		piilotaMarkkeri();
	});
}

/**
 * Piiloon
 */
function piilotaMarkkeri() {
	moveMarker.remove();
	if (moveMarker.circle) {
		moveMarker.circle.setStyle({ fillOpacity: 0 });
		moveMarker.circle = null;
	}
}

/**
 * Piirtää kaikki viivat uusiksi
 */
function uudelleenPiirraKaikki() {
	for (let id in viivat) {
		uudelleenPiirra(id);
	}
}

/**
 * Piirtää yksittäisen viivan uusiksi annetulle joukkueelle
 * @param {*} joukkueId joukkueen id
 */
function uudelleenPiirra(joukkueId) {
	if (viivat[joukkueId]) {
		const joukkue = data.joukkueet.find(elem => elem.id == joukkueId);
		const koords = haeJoukkueenRastienKoordinaatit(joukkue);
		viivat[joukkueId].setLatLngs(koords);
	}
}

/**
 * Luo joukkuelistauksen, rastit aikajärjestyksessä
 */
function luoJoukkueListaus() {
	let list = $("#joukkueet");
	const { joukkueet } = data;
	for (let i in joukkueet) {
		let li = $(`<li id="joukkue${i}" class="listaJoukkue"></li>`);
		// Tehdään pohja 5-tasoa varten
		let details = $("<details></details>");
		details.appendTo(li);
		const { nimi } = joukkueet[i];
		let summary = $(
			`<summary class="joukkue" draggable="true">${nimi} ( <span></span> km ) </summary>`
		);
		summary.appendTo(details);

		let color = rainbow(data.joukkueet.length, i);
		li.css("background-color", color);
		li.data("joukkue", data.joukkueet[i]);
		li.appendTo(list);

		let rastiLista = $('<ul class="rastiLista"></ul>');
		let leimattu = [];
		let rastit = [];
		for (let rasti in data.joukkueet[i].rastit) {
			if (!leimattu.includes(data.joukkueet[i].rastit[rasti].id)) {
				const loytynytRasti = data.rastit.find(
					elem => elem.id == data.joukkueet[i].rastit[rasti].id
				);
				if (loytynytRasti) {
					let rastiListaus = $(
						`<li id="rasti${i}-${rasti}" class="rastiPaikka" draggable="true">${
							loytynytRasti.koodi
						}</li>`
					);
					rastiListaus.data("rasti", data.joukkueet[i].rastit[rasti]);
					const aika = data.joukkueet[i].rastit[rasti].aika;
					rastiListaus.data("aika", aika);
					rastit.push(rastiListaus);
				}

				leimattu.push(data.joukkueet[i].rastit[rasti].id);
			}
		}
		// aikajärjestykseen
		let jarjestetty = jarjestaAikaJarjestykseen(rastit);
		jarjestetty.forEach(r => {
			r.appendTo(rastiLista);
		});
		rastiLista.appendTo(details);
	}

	paivitaEtaisyydet();
	alustaRaahaus();
}

// Järjestää aikajärjestykseen
const jarjestaAikaJarjestykseen = rastiLista => {
	let kopio = [...rastiLista];
	kopio.sort((r1, r2) => {
		const aika1 = new Date($(r1).data("aika"));
		const aika2 = new Date($(r2).data("aika"));
		const jarjestys = aika1 > aika2 ? 1 : -1;
		return jarjestys;
	});
	return kopio;
};

/**
 * Alustetaan drag-n-drop
 */
function alustaRaahaus() {
	for (let joukkue of document.getElementsByClassName("joukkue")) {
		joukkue.addEventListener("dragstart", evt => {
			const { id } = joukkue.parentElement.parentElement;
			evt.dataTransfer.setData("text/plain", id);
			evt.dataTransfer.effectAllowed = "move";
		});
	}

	// Oikea puoli
	let joukkueet = document.getElementById("joukkueet");
	joukkueet.addEventListener("dragover", evt => {
		evt.preventDefault();
	});
	joukkueet.addEventListener("drop", evt => {
		evt.preventDefault();
		let id = evt.dataTransfer.getData("text");
		let li = document.getElementById(id);
		if (li) {
			if (
				li.className === "listaJoukkue" &&
				li.parentElement.id !== "joukkueet"
			) {
				joukkueet.appendChild(li);
				poistaViiva($(li).data("joukkue"));
			} else if (li.className === "rastiPaikka") {
				// vikaksi
				asetaPudotettuRasti(li, evt.target);
			}
		}
	});

	// Vasen puoli johon joukkueet pudotetaan
	let kartalla = document.getElementById("kartalla");
	kartalla.addEventListener("dragover", evt => {
		evt.preventDefault();
	});
	kartalla.addEventListener("drop", evt => {
		evt.preventDefault();
		let id = evt.dataTransfer.getData("text");
		let elem = document.getElementById(id);
		if (elem) {
			if (elem.className === "listaJoukkue") {
				kartalla.insertBefore(elem, kartalla.firstChild);
				const data = $(elem).data("joukkue");
				const tyyli = $(elem).css("background-color");
				piirraViiva(data, tyyli);
			} else if (elem.className === "rastiPaikka") {
				asetaPudotettuRasti(elem, evt.target);
			}
		}
	});

	// Laitetaan drag kuuntelija rasteille
	for (let r of document.getElementsByClassName("rastiPaikka")) {
		r.addEventListener("dragstart", evt => {
			evt.dataTransfer.setData("text/plain", r.id);
			evt.dataTransfer.effectAllowed = "move";
		});
	}
}

/**
 * Kun rasti pudotetaan listalla, jos toisen rastin päällä niin sen päälle.
 * Muussa tapauksessa viimeiseksi.
 * @param {*} rasti rasti
 * @param {*} kohde kohde
 */
function asetaPudotettuRasti(rasti, kohde) {
	let lista = rasti.parentElement;
	if (
		kohde.className === "rastiPaikka" &&
		kohde.parentElement.id === lista.id
	) {
		lista.insertBefore(rasti, kohde);
	} else {
		lista.appendChild(rasti);
	}

	uudelleenJarjestaRastit(lista.parentElement.parentElement, lista);
}

/**
 * Laskee joukkueen kulkeman matkan
 * @param {*} joukkue annettu joukkue
 * @returns matkan
 */
function laskeJoukkueenMatka(joukkue) {
	let matka = 0;
	const rastit = haeJoukkueenRastienKoordinaatit(joukkue);
	for (let i = 1; i < rastit.length; i++) {
		matka += getDistanceFromLatLonInKm(
			rastit[i - 1][0],
			rastit[i - 1][1],
			rastit[i][0],
			rastit[i][1]
		);
	}
	return matka;
}

/**
 * Uudelleenjärjestetään rastit saadun listan perusteella
 * @param {*} listaJoukkue
 * @param {*} lista
 */
function uudelleenJarjestaRastit(listaJoukkue, lista) {
	let joukkue = $(listaJoukkue).data("joukkue");
	joukkue.rastit = [];
	for (let rasti of lista.children) {
		const data = $(rasti).data("rasti");
		joukkue.rastit.push(data);
	}

	uudelleenPiirra(joukkue.id);
	paivitaEtaisyys(listaJoukkue);
}

/**
 * Joukkueen rastien koordinaatit
 * @param {*} joukkue annettu joukkue
 * @returns koordinaattitaulukon
 */
function haeJoukkueenRastienKoordinaatit(joukkue) {
	let koords = [];
	for (let r of joukkue.rastit) {
		const jRasti = data.rastit.find(elem => elem.id == r.id);
		if (jRasti) {
			koords.push([jRasti.lat, jRasti.lon]);
		}
	}
	return koords;
}

/**
 * Päivittää kaikkien joukkueiden matkat
 */
function paivitaEtaisyydet() {
	$("li.listaJoukkue").each(function() {
		paivitaEtaisyys($(this));
	});
}

/**
 * Päivittää joukkueen kokonaismatkan
 * @param {*} li annettu joukkueen li elementti
 */
function paivitaEtaisyys(li) {
	let j = $(li).data("joukkue");
	let matka = laskeJoukkueenMatka(j).toFixed(1);
	$(li)
		.find("span")
		.text(matka);
}

function rainbow(numOfSteps, step) {
	// This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
	// Adam Cole, 2011-Sept-14
	// HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
	let r, g, b;
	let h = step / numOfSteps;
	let i = ~~(h * 6);
	let f = h * 6 - i;
	let q = 1 - f;
	switch (i % 6) {
		case 0:
			r = 1;
			g = f;
			b = 0;
			break;
		case 1:
			r = q;
			g = 1;
			b = 0;
			break;
		case 2:
			r = 0;
			g = 1;
			b = f;
			break;
		case 3:
			r = 0;
			g = q;
			b = 1;
			break;
		case 4:
			r = f;
			g = 0;
			b = 1;
			break;
		case 5:
			r = 1;
			g = 0;
			b = q;
			break;
	}
	let c =
		"#" +
		("00" + (~~(r * 255)).toString(16)).slice(-2) +
		("00" + (~~(g * 255)).toString(16)).slice(-2) +
		("00" + (~~(b * 255)).toString(16)).slice(-2);
	return c;
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
	var R = 6371;
	var dLat = deg2rad(lat2 - lat1);
	var dLon = deg2rad(lon2 - lon1);
	var a =
		Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.cos(deg2rad(lat1)) *
			Math.cos(deg2rad(lat2)) *
			Math.sin(dLon / 2) *
			Math.sin(dLon / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c;
	return d;
}

function deg2rad(deg) {
	return deg * (Math.PI / 180);
}
