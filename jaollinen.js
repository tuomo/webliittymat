console.log("works")

var apina = "aaapina";

function jaollinen(jono) {
    let alkumerkki = jono[0];
    let maara = 1;
    for (let i = 1; i < jono.length; i++) {
        if (jono[i] == alkumerkki) maara++;
        else break;
    }
    console.log(maara)
}

jaollinen("aaapina");

// 2a)
var lukujono = [1, 51, 1, 9, -5, 98, 1111, 4]

function erotus(lukujono) {
    let suurin = lukujono[0];
    let pienin = lukujono[0];
    for (let i = 0; i < lukujono.length; i++) {
        if (lukujono[i] > suurin) suurin = lukujono[i]
        if (lukujono[i] < pienin) pienin = lukujono[i]
    }
    console.log(`suurin: ${suurin}, pienin: ${pienin}`)
    return suurin - pienin;
}

var x = erotus(lukujono);
console.log(x)

// 2 b)
var jono = [5, 1, 3, 5, 1, 9, 12, 88, 5, 3];
function nollaus(jono) {
    let nollatut = 0;
    for (let i = 0; i < jono.length; i++) {
        for (let j = i + 1; j < jono.length; j++) {
            if (jono[j] === jono[i]) {
                jono[j] = 0;
                nollatut++;
            }
        }
    }
    return jono;
}

var y = nollaus(jono);
console.log(y);