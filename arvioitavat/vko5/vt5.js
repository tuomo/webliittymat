// Rastien liikuttaminen ei ole ihan täysin sulavaa, kun haluaa siirtää rastin tiettyyn paikkaan täytyy kohtaa klikata
// kaksi kertaa, koska en jostain syystä saanut rastia jäämään paikoilleen ensimmäisestä klikkauksesta. Myös jos koittaa
// siirtää samaa rastia kaksi kertaa peräkkäin, jälkimmäisellä kerralla rasti ei värjäydy ja rasti seuraa hiirtä heti
// klikkaamisen jälkeen.
"use strict";

window.onload = function () {
    luo_kartta();
    listaa_joukkueet();
    piirra_rastit();
    lisaa_liikutukset();

};

// Globaalit muuttujat
var kartta;
var viivat = {};
var marker;
var nykyinenRasti;
var edellinenRasti;

/* Funktio, joka luo kartan sivulle.
*/
function luo_kartta() {
    kartta = new L.map('map', {
        crs: L.TileLayer.MML.get3067Proj()
    }).setView([62.1176, 25.6080], 8);
    L.tileLayer.mml_wmts({ layer: "maastokartta" }).addTo(kartta);
}

/* Funktio, joka piirtää rastiympyrät kartalle.
*/
function piirra_rastit() {
    data.rastit.forEach(rasti => {
        let circle = L.circle(
            [rasti.lat, rasti.lon], {
                color: 'red',
                opacity: 0.75,
                fillOpacity: 0,
                radius: 150,
            }
        ).addTo(kartta);

        // Lisätään popup-ikkunalla rastikoodi rastin yhteyteen
        circle.addEventListener("mouseover", function () {
            let popup = L.popup()
                .setLatLng([L.latLng([rasti.lat, rasti.lon]).toBounds(400).getNorth(), rasti.lon])
                .setContent(rasti.koodi)
                .openOn(kartta);
        });
        circle.data = rasti;
        //circle.addEventListener("click", muokkaa_rastia);
        circle.addEventListener("click", muokkaa_rastia2);
    });

    // Keskitetään kartta oikeaan kohtaan
    kartta.fitBounds([
        [etsiKoodi("lat", 0), etsiKoodi("lon", 0)],
        [etsiKoodi("lat", 1), etsiKoodi("lon", 1)]
    ]);
}

/* Funktio, joka listaa joukkueet sivustolle. Joukkuetta voi raahata tarttumalla nimeen, mutta
   mikäli tartutaan muuhun osaan, jossa on vain taustaväria tai matka, joukkue ei raahaudu..
*/
function listaa_joukkueet() {
    let ul = document.getElementById("joukkuelista");
    let joukkueet = data.joukkueet;
    for (let i = 0; i < joukkueet.length; i++) {
        let li = document.createElement("li");
        let vari = rainbow(joukkueet.length, i);

        // Luodaan 2 span elementtiä siksi, että voidaan raahata joukkuetta vain nimeen tarttumalla ja matkaa täytyy
        // muokata muualla.
        let span = document.createElement("span");
        let span2 = document.createElement("span");
        let teksti = document.createTextNode(joukkueet[i].nimi);
        let teksti2 = document.createTextNode(" (" + laske_matka(i) + " km)");
        span.setAttribute("id", "S" + i + "C" + vari);
        span2.setAttribute("id", "km" + i);
        span.appendChild(teksti);
        span2.appendChild(teksti2);
        li.appendChild(span);
        li.appendChild(span2);
        li.style.background = vari;
        li.setAttribute("id", "jouk" + i + "C" + vari);
        li.setAttribute("class", "joukkueet");
        span.setAttribute("draggable", "true");
        span.addEventListener("dragstart", function (e) {
            e.dataTransfer.setData("text/plain", li.getAttribute("id"));
            e.dataTransfer.effectAllowed = 'move';
        });
        ul.appendChild(li);

        // Järjestetään joukkueen rastit aikajärjestykseen
        jarjesta(joukkueet[i].rastit);
    }

}

/* Funktio, joka etsii tietorakenteesta lat- & lon-koodit. Arvolla 0 etsitään pienin arvo ja arvolla 1 suurin arvo.
*/
function etsiKoodi(koodi, arvo) {
    let lat = data.rastit[0].lat;
    let lon = data.rastit[0].lon;

    // Etsitään lat-koodi, 0 on pienin koodi ja 1 on suurin koodi
    if (koodi === "lat") {
        if (arvo === 0) {
            data.rastit.forEach(rasti => {
                if (rasti.lat < lat) lat = rasti.lat;
            });
        }
        else {
            data.rastit.forEach(rasti => {
                if (rasti.lat > lat) lat = rasti.lat;
            });
        }
        return lat;
    }
    else {
        if (arvo === 0) {
            data.rastit.forEach(rasti => {
                if (rasti.lon < lon) lon = rasti.lon;
            });
        }
        else {
            data.rastit.forEach(rasti => {
                if (rasti.lon > lon) lon = rasti.lon;
            });
        }
        return lon;
    }
}

/* Funktio, joka lisää liikutteluominaisuudet listauksille.
*/
function lisaa_liikutukset() {
    let listaK = document.getElementById("joukkueetKartalla");
    let listaJ = document.getElementById("joukkuelista");

    // Estetään normaalitapahtumat kun joukkue raahataan tietyn osan yli.
    listaK.addEventListener("dragover", function (e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = "move";
    });
    listaJ.addEventListener("dragover", function (e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = "move";
    });

    // Lisätään tapahtuman käsittelijä siihen, kun joukkue raahataan ja pudotetaan Kartalla kohtaan.
    listaK.addEventListener("drop", function (e) {
        e.preventDefault();

        let data = e.dataTransfer.getData("text");
        let kohde = document.getElementById(data);

        // Katsotaan onko liikuteltava elementti koko joukkue vai joukkueen rasti
        if (kohde.className === "joukkueet") {
            listaK.insertAdjacentElement("afterbegin", kohde);
            let id = kohde.id.substring(4, kohde.id.indexOf("C"));
            let details = document.getElementById("D" + id);
            // Katsotaan onko jo olemassa details elementti, jos on niin sitte, sitä ei enää luoda.
            if (details === null) {
                muokkaa_joukkue(kohde);
            }
            piirra_reitti(kohde.id);
        }
        else {
            let joukkueNro = kohde.className.substring(6);
            // Joukkueen rasti voidaan pudottaa vain kyseisen joukkueen listaan, jos se pudotetaan mihin tahansa muualle
            // kohdassa Kartalla (esim. toisen joukkueen rastien joukkoon), kyseinen rasti siirretään joukkueen rastilistauksen
            // viimeiseksi, muutoin se sijoitetaan aina ennen kohdalla olevaa rastia.
            if (kohde.className === e.target.className) {
                e.target.insertAdjacentElement("beforebegin", kohde);
            }
            else {
                let pudotusPaikka = document.getElementById("rl" + joukkueNro);
                pudotusPaikka.insertAdjacentElement("beforeend", kohde);
            }

            // Aliohjelma, joka muokkaa rastien järjestyksen
            muokkaa_rastijarjestysta(joukkueNro);
        }

    });

    // Lisätään tapahtuman käsittelijä siihen, kun joukkue raahataan ja pudotetaan Joukkueet kohtan.
    listaJ.addEventListener("drop", function (e) {
        e.preventDefault();

        let data = e.dataTransfer.getData("text");
        let kohde = document.getElementById(data);
        listaJ.insertAdjacentElement("beforeend", kohde);
        palauta_joukkue(kohde);
        poista_reitti(kohde.id);
    });

}

/* Funktio, joka muokkaa joukkueen rastien järjestystä sekä päivittää kartalle muuttuneen reitin ja laskee joukkueelle
   uuden kuljetun matkan.
*/
function muokkaa_rastijarjestysta(joukkueNro) {
    let rastitListassa = document.getElementsByClassName("rastit" + joukkueNro);
    let rastitDatassa = data.joukkueet[joukkueNro].rastit;

    // Haetaan for-silmukan sisällä rastien alkuperäinen kohta ja 
    for (let i = 0; i < rastitListassa.length; i++) {
        let id = rastitListassa[i].id.substring(1, rastitListassa[i].id.indexOf("I"));
        rastitDatassa[id].uusi = i;
    }

    // Järjestetään datassa olevat rastit siihen järjestykseen kuin käyttäjä on ne laittanut
    rastitDatassa.sort(function (a, b) {
        return parseInt(a.uusi) - parseInt(b.uusi);
    });

    // Päivitetään joukkueen kulkema matka ja reitti (aluksi poistetaan vanha reitti ja luodaan tilalle uusi)
    let vari = rainbow(data.joukkueet.length, joukkueNro);
    let id = "jouk" + joukkueNro + "C" + vari;
    poista_reitti(id);
    piirra_reitti(id);
    paivita_matkat();
}

/* Funktio, joka muokkaa joukkue-elementtiä listassa lisäten siihen tiedot joukkueen rasteista. Samalla mahdollistetaan
   se, että joukkueen nimen viereen tulee pieni nuoli, jot klikkaamalla saadaan esille tiedot joukkueen rasteista.
*/
function muokkaa_joukkue(joukkue) {

    // Haetaan kaikki tiedot li-elementistä ja luodaan uudet tarpeelliset elementit.
    let elementti = document.getElementById(joukkue.id);
    let spanId = "S" + joukkue.id.substring(4, joukkue.id.length);
    let id = joukkue.id.substring(4, joukkue.id.indexOf("C"));
    let span = document.getElementById(spanId);
    let span2 = document.getElementById("km" + id);
    let rastit = etsi_rastit(id, "rastit");
    let koodit = etsi_rastit(id, "koodit");
    let details = document.createElement("details");
    details.setAttribute("id", "D" + id);
    let summary = document.createElement("summary");
    summary.setAttribute("id", "T" + id);
    let ul = document.createElement("ul");
    ul.setAttribute("id", "rl" + id);

    // Laitetaan rastitiedot ja "aukiklikattavat" tiedot li-elementtiin
    summary.appendChild(span);
    summary.appendChild(span2);
    details.appendChild(summary);
    for (let i = 0; i < rastit.length; i++) {
        let li = document.createElement("li");
        li.setAttribute("id", "R" + i + "ID" + data.joukkueet[id].rastit[i].id);
        let teksti = document.createTextNode(koodit[i]);
        li.appendChild(teksti);
        li.setAttribute("class", "rastit" + id);
        li.setAttribute("draggable", "true");
        li.addEventListener("dragstart", function (e) {
            e.dataTransfer.setData("text/plain", li.getAttribute("id"));
            e.dataTransfer.effectAllowed = 'move';

        });
        ul.appendChild(li);
    }
    details.appendChild(ul);
    elementti.appendChild(details);
}

/* Funktio, joka palauttaa joukkue-elementin siihen muotoon kuin se kuuluu olla joukkuelistauksessa.
*/
function palauta_joukkue(joukkue) {
    // Haetaan kaikki tiedot li-elementistä ja luodaan uudet tarpeelliset elementit.
    let elementti = document.getElementById(joukkue.id);
    let spanId = "S" + joukkue.id.substring(4, joukkue.id.length);
    let id = joukkue.id.substring(4, joukkue.id.indexOf("C"));
    let span = document.getElementById(spanId);
    let span2 = document.getElementById("km" + id);
    jarjesta(data.joukkueet[id].rastit);

    // Haetaan poistettavat osat ja poistetaan ne, samalla lisätään span-elementti joukkue-elementtiin
    let details = document.getElementById("D" + id);
    if (details !== null) {
        details.remove();
        elementti.appendChild(span);
        elementti.appendChild(span2);
    }
}


/* Funktio, jonka avulla käsitellään rastin muokkaaminen ja rastiympyrän siirtäminen sekä siitä aiheutuvat muutokset
   karttaan ja joukkueen kulkemaan matkaan. Rastia voi siirtää klikkaamalla rastia hiirellä ja pitämällä painike
   pohjassa, rasti siirretään siihen kohtaan missä hiiri on painikkeen noustessa ylös. Tällöin punainen ympyrä seuraa
   hiirtä
*/
function muokkaa_rastia2(e) {
    let rasti = e.target;
    let tarkistus = (nykyinenRasti === rasti);
    if (nykyinenRasti) nykyinenRasti.setStyle({ fillOpacity: 0 });
    if (edellinenRasti !== rasti) edellinenRasti = null;
    nykyinenRasti = rasti;
    rasti.setStyle({ fillOpacity: 1 });

    rasti.on("mousedown", function (e) {
        kartta.dragging.disable();
        let { lat: alkuLatCirle, lng: alkuLngCirle } = rasti._latlng;
        let { lat: alkuLatHiiri, lng: alkuLngHiiri } = e.latlng;

        kartta.on('mousemove', e => {
            let { lat: uusiLatHiiri, lng: uusiLngHiiri } = e.latlng;
            let lat = alkuLatHiiri - uusiLatHiiri;
            let lng = alkuLngHiiri - uusiLngHiiri;

            let circleKohta = [alkuLatCirle - lat, alkuLngCirle - lng];
            rasti.setLatLng(circleKohta);
        });
    });
    if (tarkistus) {
        kartta.on("mouseup", function (e) {
            // Haetaan rastin uusi sijainti siitä kohtaa, johon hiirellä on klikattu ja laitetaan ne rastin koordinaateiksi
            let koordinaatit = e.latlng;
            nykyinenRasti.setLatLng(koordinaatit);

            // Laitetaan muokattu rasti taas alkutilaan eli ympyrän keskusta ei ole väritetty.
            nykyinenRasti.setStyle({ fillOpacity: 0 });
            nykyinenRasti = null;

            // Tehdään tarvittavat muutokset sekä tietokantaan, kartalle että joukkuelistaukseen
            rasti.data.lat = koordinaatit.lat;
            rasti.data.lon = koordinaatit.lng;
            paivita_matkat();
            paivita_reitit();


            // Lisätään karttaan taas raahausmahdollisuus
            kartta.dragging.enable();
            kartta.removeEventListener("mousemove");
            kartta.removeEventListener("mouseup");

        });
         
        edellinenRasti = rasti;
    }
    if (edellinenRasti === rasti) {
        rasti.setStyle({ fillOpacity: 0 });
    }
    
}

/* Funktio, joka piirtää joukkueen kulkeman reitin kartalle joukkueen id:n perusteella.
*/
function piirra_reitti(joukkue) {
    // Kirjaimella C on merkitty id:hen kohta, josta alkaa värikoodi ja numero 4 tulee siitä, että id:n alku on jouk + nro.
    let vari = joukkue.substring(joukkue.indexOf("C") + 1, joukkue.length);
    let kohta = joukkue.substring(4, joukkue.indexOf("C"));
    let rastit = etsi_rastit(kohta, "rastit");

    // Jos on jo olemassa poistetaan
    if (viivat[joukkue]) viivat[joukkue].remove();
    viivat[kohta] = L.polyline(rastit, { color: vari }).addTo(kartta);
}

/* Funktio, joka poistaa piirretyn reitin kartalta joukkueen id:n perusteella.
*/
function poista_reitti(joukkue) {
    let kohta = joukkue.substring(4, joukkue.indexOf("C"));
    if (viivat[kohta]) {
        viivat[kohta].remove();
    }
}

/* Funktio, joka etsii joukkueen rastit ja palauttaa niistä kaksiulotteisen listan sisältäen lat- ja lon-koodit.
   Funktio myös etsii rastien koodit ja palauttaa niistä taulukon. Se kumpi toimenpide tehdään riippuu toisesta 
   parametristä, joka on joko "rastit" tai "koodit".
*/
function etsi_rastit(id, kumpi) {
    let joukkueenRastit = data.joukkueet[id].rastit;
    let rastit = [];

    // Etsitään tietorakenteesta ne rastit, jotka vastaavat joukkueen leimaamia rasteja
    for (let kohde of joukkueenRastit) {
        let leimattu = data.rastit.find(function (rasti) {
            return kohde.id == rasti.id;
        });
        if (leimattu) {
            if (kumpi === "rastit") {
                rastit.push([leimattu.lat, leimattu.lon]);
            }
            else {
                rastit.push(leimattu.koodi);
            }
        }
    }
    return rastit;
}

/* Funktio, joka järjestää joukkueen rastit aikajärjestykseen.
*/
function jarjesta(rastit) {
    rastit.sort(function (a, b) {
        let aAika = new Date(a.aika).getTime();
        let bAika = new Date(b.aika).getTime();
        if (aAika < bAika) return -1;
        if (aAika > bAika) return 1;
        return 0;
    });
}

/* Funktio, joka päivittää joukkueiden kulkemat matkat.
*/
function paivita_matkat() {
    // Haetaan kaikki li-elementit, joiden luokka on joukkueet.
    let joukkueet = $(".joukkueet");
    for (let i = 0; i < joukkueet.length; i++) {
        // Haetaan li-elementistä id.
        let id = $(joukkueet[i]).attr("id").substring(4, $(joukkueet[i]).attr("id").indexOf("C"));
        let matka = laske_matka(id);
        $("#km" + id).text(" (" + matka + " km)");
    }
}

/* Funktio, joka piirtää uudelleen viivat kartalle rastin siirtämisen jälkeen.
*/
function paivita_reitit() {
    for (let i in viivat) {
        let rastit = etsi_rastit(i, "rastit");
        viivat[i].setLatLngs(rastit);
    }
}

/* Funktio, joka laskee joukkueen kulkeman matkan.
*/
function laske_matka(id) {
    let rastit = etsi_rastit(id, "rastit");
    let km = 0;
    for (let i = 0; i < rastit.length - 1; i++) {
        let lat1 = rastit[i][0];
        let lon1 = rastit[i][1];
        let lat2 = rastit[i + 1][0];
        let lon2 = rastit[i + 1][1];
        km += getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2);
    }
    return km.toFixed(0);
}

/* Tehtävän annossa valmiiksi annettu funktio, jota täytyy käyttää värien luomiseen.
*/
function rainbow(numOfSteps, step) {
    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    let r, g, b;
    let h = step / numOfSteps;
    let i = ~~(h * 6);
    let f = h * 6 - i;
    let q = 1 - f;
    switch (i % 6) {
        case 0: r = 1; g = f; b = 0; break;
        case 1: r = q; g = 1; b = 0; break;
        case 2: r = 0; g = 1; b = f; break;
        case 3: r = 0; g = q; b = 1; break;
        case 4: r = f; g = 0; b = 1; break;
        case 5: r = 1; g = 0; b = q; break;
    }
    let c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
    return (c);
}

/* Viikkotehtävässä 2 annetut funktiot matkan laskemiseksi.
*/
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

/* Funktio, joka muokkaa kartalle piirrettyä rastia. Tason 3 vaatimusten mukainen, 
   tasolla 5 nämä asiat tehdään eri funktion avulla.
*/
function muokkaa_rastia(e) {
    let rasti = e.target;
    let koordinaatit = rasti.getLatLng();
    if (marker) {
        kasittele_marker(rasti);
        marker.remove();
    }
    nykyinenRasti = rasti;
    rasti.setStyle({ fillOpacity: 1 });
    marker = L.marker(koordinaatit, {
        draggable: true
    }).addTo(kartta);

    kasittele_marker(rasti);
}

/* Funktio, joka käsittelee markerin liikkumisen ja mahdolliset muutokset rastiin. Tason 3 vaatimusten mukainen, 
   tasolla 5 nämä asiat tehdään eri funktion avulla.
*/
function kasittele_marker(rasti) {
    marker.addEventListener("dragend", function (e) {

        // Koordinaattien tarkistaminen, että jos ne eroavat
        let koordinaatit = marker.getLatLng();
        rasti.setLatLng(koordinaatit);

        // Laitetaan rastille muutetut koordinaatit datarakenteeseen
        rasti.data.lat = koordinaatit.lat;
        rasti.data.lon = koordinaatit.lng;
        marker.remove();

        // Laitetaan rastit taas alkutilaan
        rasti.setStyle({ fillOpacity: 0 });

        paivita_matkat();
        paivita_reitit();
    });

    let rasti2 = nykyinenRasti;
    if (rasti2 !== rasti) {
        rasti2.setStyle({ fillOpacity: 0 });
    }
}