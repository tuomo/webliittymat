Hyvää settiä, osaat selkeästi kirjoittaa JS:ää. 
Lambdoja, funktionaalisuutta ja operaattoreita käytetty fiksusti. Rivin 375 reduceri on erityisen näppärä!
Funktiot vieläpä pääosin palauttaa paluuarvona tuotoksensa, nyt on hyvin jäsenneltyä koodi!

Kaikki näyttää toimivan mallikkaasti.

Muuten, selainten viimeiset versiot tukee template literalseja. Eli nuo 'joukkue ' + nimiInput.value + ... saattaa olla helpompi vaan kirjoittaa `joukkue ${nimiInput.value} ...`
