// data-muuttuja sisältää kaiken tarvittavan ja on rakenteeltaan lähes samankaltainen kuin viikkotehtävässä 2
// Rastileimaukset on siirretty tupa-rakenteesta suoraan jokaisen joukkueen yhteyteen
// Rakenteeseen on myös lisätty uusia tietoja
// voit tutkia tarkemmin käsiteltävää tietorakennetta konsolin kautta 
// tai json-editorin kautta osoitteessa http://jsoneditoronline.org/
// Jos käytät json-editoria niin avaa data osoitteesta:
// http://appro.mit.jyu.fi/tiea2120/vt/vt3/data.json

"use strict";

console.log(data);

window.onload = () => {
    setupJoukkueForm();
    setupSarjaForm();
    listaaJoukkueet();
}

// Sarjan lisäys form
function setupSarjaForm() {
    const nimiInput = document.getElementById('SarjanNimiInput');
    const kestoInput = document.getElementById('SarjanKestoInput');
    const alkuaikaInput = document.getElementById('SarjanAlkuaikaInput');
    const loppuaikaInput = document.getElementById('SarjanLoppuaikaInput');

    // tarkista ettei saman nimistä sarjaa ole jo
    nimiInput.addEventListener('change', evt => {
        const kaikkiNimet = data.kisat[0].sarjat.map(sarja => sarja.nimi.trim());
        const uusiNimi = nimiInput.value.trim();

        if (kaikkiNimet.indexOf(uusiNimi) > -1) {
            nimiInput.setCustomValidity('Saman niminen sarja on jo olemassa.');
        } else {
            nimiInput.setCustomValidity('');
        }
    })

    const kisanAlkuaika = new Date(data.kisat[0].alkuaika.replace(' ','T'));
    const kisanLoppuaika = new Date(data.kisat[0].loppuaika.replace(' ','T'));
    
    // aseta sarjan alkuaika min-rajoitus
    const minAlkuH = kisanAlkuaika.getHours();
    const minAlkuM = kisanAlkuaika.getMinutes();
    alkuaikaInput.setAttribute('min', minAlkuH.toString().padStart(2,'0')+':'+minAlkuM.toString().padStart(2,'0'));
    // voidaan jättää myös tyhjäksi
    alkuaikaInput.addEventListener('change', evt => {
        if (evt.target.value == '') {
            evt.target.deleteAttribute('required');
        } else {
            evt.target.setAttribute('required', 'true');
        }
    });

    // aseta sarjan loppuajan max-rajoitus
    const maxLoppuH = kisanLoppuaika.getHours();
    const maxLoppuM = kisanLoppuaika.getMinutes();
    loppuaikaInput.setAttribute('max', maxLoppuH.toString().padStart(2,'0')+':'+maxLoppuM.toString().padStart(2,'0'));
    // voidaan jättää myös tyhjäksi
    loppuaikaInput.addEventListener('change', evt => {
        if (evt.target.value == '') {
            evt.target.deleteAttribute('required');
        } else {
            evt.target.setAttribute('required', 'true');
        }
    });

    // sarjan loppuaika min-rajoitus voidaan määrittää vasta kun sarjan pituus ja alkuaika on annettu
    const asetaLoppuMinRajoitus = function(evt) {
        if (!kestoInput.validity.valid || !alkuaikaInput.validity.valid || alkuaikaInput.value == '') {
            return;
        }
        const sarjanKesto = parseInt(kestoInput.value);
        const sarjanAlkuaika = alkuaikaInput.value.split(':');
    
        // aseta sarjan loppuaika-rajoitukset
        const minLoppuH = parseInt(sarjanAlkuaika[0]) + sarjanKesto;
        const minLoppuM = parseInt(sarjanAlkuaika[1]);

        loppuaikaInput.setAttribute('min', minLoppuH.toString().padStart(2,'0')+':'+minLoppuM.toString().padStart(2,'0'));
    };

    alkuaikaInput.addEventListener('change', asetaLoppuMinRajoitus);
    kestoInput.addEventListener('change', asetaLoppuMinRajoitus);

    // Lomakkeen tarkistus ja tallennus
    const form = document.getElementById('LisaaSarjaForm');
    form.addEventListener('submit', evt => {
        evt.preventDefault();

        const alkuaika = (alkuaikaInput.value != '') ? alkuaikaInput.value : null;
        const loppuaika = (loppuaikaInput.value != '') ? loppuaikaInput.value : null;

        const kaikkiSarjaIDt = data.kisat[0].sarjat.map(sarja => sarja.id);
        const uusiSarjaID = Math.max(...kaikkiSarjaIDt) + 1;

        const sarja = {
            nimi: nimiInput.value,
            kesto: parseInt(kestoInput.value),
            alkuaika: alkuaika,
            loppuaika: loppuaika,
            id: uusiSarjaID
        };

        data.kisat[0].sarjat.push(sarja);

        document.getElementById('SarjaLisattyTeksti').textContent = 'Sarja ' + nimiInput.value + ' on lisätty.'

        nimiInput.value = '';
        kestoInput.value = '';
        alkuaikaInput.value = '';
        loppuaikaInput.value = '';

        const form = document.getElementById('LisaaJoukkueForm');
        form.reset();
    });
}


// Joukkueen lisäys form
function setupJoukkueForm() {
    const form = document.getElementById('LisaaJoukkueForm');
    const ilmoitusTeksti = document.getElementById('JoukkueLisattyTeksti');
    const nimiInput = document.getElementById('JoukkueenNimiInput');
    const luontiaikaInput = document.getElementById('LuontiaikaInput');
    let leimaustapaInputs = luoLeimaustapaValinnat();;
    let sarjaInputs = luoSarjaValinnat();;
    let jasenet = luoJasenKentat();;
    
    // Joukkueen nimen tarkistus
    nimiInput.addEventListener('change', evt => {
        const kaikkiNimet = data.joukkueet.map(joukkue => joukkue.nimi.trim());
        const uusiNimi = nimiInput.value.trim();

        if (uusiNimi.length < 2) {
            nimiInput.setCustomValidity('Joukkueen nimen tulee olla vähintään 2 merkkiä.');
        }
        else if (kaikkiNimet.indexOf(uusiNimi) > -1) {
            nimiInput.setCustomValidity('Saman niminen joukkue on jo olemassa.');
        }
        else {
            nimiInput.setCustomValidity('');
        }
    });

    // Resetoi kaavake
    form.reset = function(joukkue) {
        form.joukkue = joukkue;
        ilmoitusTeksti.textContent = ' ';  
        leimaustapaInputs = luoLeimaustapaValinnat();
        sarjaInputs = luoSarjaValinnat();
        jasenet = luoJasenKentat();
        
        if (joukkue) {
            leimaustapaInputs = luoLeimaustapaValinnat();
            nimiInput.value = joukkue.nimi;
            luontiaikaInput.setAttribute('step', '0.001');
            luontiaikaInput.value = joukkue.luontiaika.replace(' ', 'T');
            // aseta joukkueen leimaustavat
            joukkue.leimaustapa.forEach(leimausTapa => {
                leimaustapaInputs.forEach(valinta => {
                    if (valinta.value == leimausTapa) valinta.checked = true;
                });
            });
            // etsi joukkuuen sarjan id:tä vastaava sarja datasta
            const sarja = data.kisat[0].sarjat.find(sarja => sarja.id == joukkue.sarja);
            // valitse joukkueen sarja
            sarjaInputs.find(valinta => (valinta.value == sarja.nimi)).checked = true;
            // listaa jäsenet
            const iter = jasenet.values();
            while (jasenet.size < joukkue.jasenet.length) jasenet.luoKentta();
            joukkue.jasenet.forEach(jasen => {
                const jasenInput = iter.next().value;
                jasenInput.value = jasen;
            });
    
            leimaustapaInputs.tarkista();
            jasenet.tarkista();
        } else {
            nimiInput.value = '';
            luontiaikaInput.value = '';
            luontiaikaInput.setAttribute('step', '60');

        }
    }    
    
    // Lomakkeen tarkistus ja tallennus
    form.addEventListener('submit', evt => {
        evt.preventDefault();

        // hae valitun sarjan ID
        const valittuSarja = sarjaInputs.find(valinta => valinta.checked).value;
        const valittuSarjaID = data.kisat[0].sarjat.find(sarja => (sarja.nimi == valittuSarja)).id;
        // määritä uusi joukkue ID (suurin olemassa oleva + 1)
        const kaikkiJoukkueIDt = data.joukkueet.map(joukkue => joukkue.id);
        const uusiJoukkueID = Math.max(...kaikkiJoukkueIDt) + 1;

        // hae valitut leimaustavat
        const valitutLeimaustaparuudut = leimaustapaInputs.filter(leimaustapa => leimaustapa.checked);
        const leimaustavat = valitutLeimaustaparuudut.map(valinta => valinta.value);
        
        // jäsenten nimet
        const jasenNimet = Array.from(jasenet).map(kentta => kentta.value).filter(nimi => nimi != '');
        
        // luontiaika
        const luontiaika = luontiaikaInput.value.replace('T', ' ');

        const uusiJoukkue = {
            nimi: nimiInput.value,
            jasenet: jasenNimet,
            luontiaika: luontiaika,
            leimaustapa: leimaustavat,
            sarja: valittuSarjaID,
            id: uusiJoukkueID
        }

        // jos muokataan vanhaa, korvaa vanhat tiedot uusilla
        if (form.joukkue) {
            ilmoitusTeksti.textContent = 'Joukkuetta ' + form.joukkue.nimi + ' on muokattu.';
            Object.assign(form.joukkue, uusiJoukkue);
        }
        // jos tehdään uutta, lisää joukkue listaan
        else {
            data.joukkueet.push(uusiJoukkue);
            ilmoitusTeksti.textContent = 'Joukkue ' + nimiInput.value + ' on lisätty.';
        }

        listaaJoukkueet();
        form.reset();
    }); 
}

// Luo kentät jäsenien nimille
function luoJasenKentat() {
    // Luo jasenien nimikentät
    const parent = document.getElementById('JasenetField');
    // poista vanhat
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
    const lukumaara = 5;
    const jasenet = new Set();

    jasenet.tarkista = function(evt) {
        // Tarkista onko tarvetta luoda uusi kenttä
        const kaikkiTaynna = Array.from(jasenet).every(kentta => kentta.value != '');
        if (kaikkiTaynna) jasenet.luoKentta();

        // Hae täytetyt kentät
        const taytetyt = Array.from(jasenet).filter(kentta => kentta.value != '');
        // Set sisältää vain uniikkeja nimiä
        const uniikitNimet = new Set(taytetyt.map(kentta => kentta.value));
        // hae ensimmäinen jäsen-kenttä validity checkin kohteeksi
        const ekaJasen = Array.from(jasenet)[0];

        // Tarkista että vähintään kaksi jäsentä
        if (taytetyt.length < 2) {
            ekaJasen.setCustomValidity('Anna vähintään kaksi jäsentä.');
        // Tarkista ettei saman nimisiä jäseniä
        } else if (uniikitNimet.size < taytetyt.length) {
            ekaJasen.setCustomValidity('Samannimisiä jäseniä ei sallita.');
        // Kaikki kunnossa
        } else {
            ekaJasen.setCustomValidity('');
        }
    }
    // luo yhden uuden nimi-kentän
    jasenet.luoKentta = function() {
        const div = document.createElement('div');
        div.setAttribute('class', 'container');
        // luo label
        const id = 'Jäsen ' + (this.size + 1);
        const label = document.createElement('label');
        label.textContent = id;
        label.setAttribute('for', id);
        // luo text input
        const textInput = document.createElement('input');
        textInput.setAttribute('type', 'text');
        textInput.setAttribute('id', id);
        textInput.addEventListener('blur', this.tarkista);
        // lisaa DOM:iin
        parent.appendChild(div);
        div.appendChild(label);
        div.appendChild(textInput);
        this.add(textInput);
    }
    // luo vakio määrä kenttiä valmiiksi
    for (let i=0; i < lukumaara; i++) {
        jasenet.luoKentta();
    }

    jasenet.tarkista();
    return jasenet;
}

// Luo html-lista annetun teksti-listan perusteella
function luoLista(parent, lista) {
    const ul = document.createElement('ul');
    parent.appendChild(ul);
    lista.forEach(item => {
        const li = document.createElement('li');
        li.textContent = item;
        ul.appendChild(li);
    });
    return ul;
}

// Luo html lista joukkueista ja sen jäsenistä
function listaaJoukkueet() {
    const div = document.getElementById('joukkueListaus');
    const form = document.getElementById('LisaaJoukkueForm');

    // Tyhjennetään mahdollinen aikaisempi listaus
    while (div.firstChild) {
        div.removeChild(div.firstChild);
    }
    // hae joukkueet ja jarjesta nimen mukaan (kopioi jasenet-arrayn ennen järjestämistä)
    const joukkueTiedot = data.joukkueet.map(joukkue => {
        return { nimi: joukkue.nimi, jasenet: joukkue.jasenet.slice(0).sort(), ref: joukkue};
    });
    joukkueTiedot.sort( (a, b) => a.nimi.localeCompare(b.nimi));

    // Luo html-lista joukkueen nimistä, joiden alle alilista jäsenistä
    const ul = document.createElement('ul');
    joukkueTiedot.forEach(joukkue => {
        const li = document.createElement('li');
        const a = document.createElement('a');
        // lisää viite joukkue objektiin
        a.joukkue = joukkue.ref;
        a.textContent = joukkue.nimi;
        a.setAttribute('href', '#LisaaJoukkueForm');
        // luo uusi joukkue kaavake klikatun joukkueen tiedoilla
        a.addEventListener('click', evt => form.reset(evt.target.joukkue));
        li.appendChild(a);
        luoLista(li, joukkue.jasenet);
        ul.appendChild(li);
    });
    div.appendChild(ul);
}

// Listaa datasta löytyvät leimaustavat valintaruutuina
function luoLeimaustapaValinnat() {
    const parent = document.getElementById('leimaustavatDiv');
    // poista mahdolliset edelliset valinnat
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
    // hae lista leimaustavoista
    const leimaustavat = data.kisat[0].leimaustapa;
    // luo leimaustavoille valintaruudut
    const valintaruudut = [];
    leimaustavat.forEach(leimaustapa => {
        const p = document.createElement('p');
        parent.appendChild(p);
        const checkbox = document.createElement('input');
        p.appendChild(checkbox);
        const label = document.createElement('label');
        p.appendChild(label);

        const id = 'Leimaustapa' + leimaustapa;
        // Aseta checkbox attribuutit
        checkbox.setAttribute('type',   'checkbox');
        checkbox.setAttribute('id',     id);
        checkbox.setAttribute('value',  leimaustapa);
        checkbox.setAttribute('name',   'Leimaustapa');

        // Aseta label attribuutit
        label.setAttribute('for', id);
        label.textContent = leimaustapa;

        valintaruudut.push(checkbox);
    });

    // Tarkistus leimaustavoille
    valintaruudut.tarkista = function(evt) {
        const leimaustapaValittu = valintaruudut.reduce((valittu, valinta) => (valittu || valinta.checked));
        const leimaustapaValidityText = (leimaustapaValittu) ? '' : 'Valitse vähintään yksi leimaustapa!';
        valintaruudut[0].setCustomValidity(leimaustapaValidityText);
    }
    valintaruudut.forEach(valinta => valinta.addEventListener('change', valintaruudut.tarkista));

    return valintaruudut;
}

// Listaa datasta löytyvät sarjat valintapainikkeina
function luoSarjaValinnat() {
    const parent = document.getElementById('sarjatDiv');
    // poista mahdolliset edelliset valinnat
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
    // hae lista sarjoista
    const sarjat = data.kisat[0].sarjat.map(sarja => sarja.nimi).sort();

    const valintanapit = [];
    // luo sarjoille valintanapit
    sarjat.forEach((sarja, index) => {
        const p = document.createElement('p');
        parent.appendChild(p);
        const radio = document.createElement('input');
        p.appendChild(radio);
        const label = document.createElement('label');
        p.appendChild(label);

        const id = 'Sarja' + sarja;
        // Aseta radio attribuutit
        radio.setAttribute('type',   'radio');
        radio.setAttribute('id',     id);
        radio.setAttribute('value',  sarja);
        radio.setAttribute('name',   'Sarja');
        if (index == 0) radio.setAttribute('checked', 'true');

        // Aseta label attribuutit
        label.setAttribute('for', id);
        label.textContent = sarja;

        valintanapit.push(radio);
    });
    return valintanapit;
}




