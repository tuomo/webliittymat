"use strict"

window.onload = function()
{
	for (let i = 0; i < 10; i++)
	{
		setTimeout(lahetaPalkki,i*250);
	}
	
	piirraPupunPuolikas("vasen");
	piirraPupunPuolikas("oikea");
}

/**
 * Kopioi mallipalkki ja laita kopion animaatio käyntiin
 */
function lahetaPalkki()
{
	let palkki = document.querySelector(".palkki").cloneNode(true);
	palkki.setAttribute("class","palkki_anim");
	document.body.appendChild(palkki);
}

/**
 * Piirrä canvasiin puolikas pupu
 * @param {String} puoli "vasen" tai "oikea", piirrettävä puoli
 */
function piirraPupunPuolikas(puoli)
{
	let canvas = document.querySelector(".pupu_" + puoli);
	let context = canvas.getContext("2d");
	
	let img = document.querySelector(".pupu");
	
	let xoff = 0;
	if (puoli == "oikea") xoff = 191.5;
	context.drawImage(img,0+xoff,0,191.5,600,0,0,191.5,600);
}
