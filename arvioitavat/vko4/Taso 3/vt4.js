"use strict"

//Rullaavan tekstin ominaisuudet
let teksti = {korkeus:120,nopeus:.1,teksti:"TIEA2120 Web-käyttöliittymien ohjelmointi -kurssin viikkotehtävä 4 taso 3 edellyttää skrollerin toteuttamista. Tämä skrolleri toimii tekstin määrästä riippumatta"};

window.onload = function()
{
	//Lisää dokumentiin porrastetusti animoituja palkkeja
	for (let i = 0; i < 10; i++)
	{
		setTimeout(lahetaPalkki,i*250);
	}
	
	//Lisää pöllöjä, kun pöllönlisäysnappia painetaan
	document.querySelector("button").addEventListener("click",lisaaPollo);
	
	//Alusta rullaava teksti
	teksti.canvas = document.querySelector(".teksti");
	teksti.context = teksti.canvas.getContext("2d");
	addEventListener("resize",alustaTeksti());
	alustaTeksti();
	
	//Aloita ruutupäivitykset
	requestAnimationFrame(ruutu);
}

/**
 * Lisää dokumenttiin animoitu palkki
 */
function lahetaPalkki()
{
	//Kloonaa uusi palkki vanhasta ja aseta sille animoitu css-luokka
	let palkki = document.querySelector(".palkki").cloneNode(true);
	palkki.setAttribute("class","palkki_anim");
	document.body.appendChild(palkki);
}

/**
 * Piirrä canvasiin puolikas pupu
 * @param {string} puoli "vasen" tai "oikea", piirrettävä puoli
 */
function piirraPupunPuolikas(puoli,korkeus)
{
	//Hae canvas ja sen context
	let canvas = document.querySelector(".pupu_" + puoli);
	let context = canvas.getContext("2d");
	
	//Tyhjennä canvas
	context.clearRect(0,0,canvas.width,canvas.height);
	
	//Hae kuva sekä sen korkeus ja leveys
	let img = document.querySelector(".pupu_img");
	let h = img.height;
	let w = img.width / 2;
	
	//Piirrä oikeasta pöllöstä oikea puoli
	let xoff = 0;
	if (puoli == "oikea") xoff = w;
	
	//Piirretään rivi kerrallaan
	for (let i = 0; i < h; i++)
	{
		//Aaltoefekti:
		//Rivistä ja ajanhetkestä riippuva sini, jonka
		//perusteella satetaan piirtää naapuririvejä
		let yoff = Math.sin(i/16 + Date.now()/200)*12;
		
		//Piirrä rivi
		context.drawImage(img,xoff,i+yoff,w,1,0,i,w,1);
	}
}

/**
 * Lisää dokumentiin uusi pöllö
 */
function lisaaPollo()
{
	//Kloonaa uusi pöllö vanhasta
	let pollo = document.querySelector(".pollo_anim").cloneNode();
	
	//Aseta joka toinen pöllö kulkemaan eri suuntaan
	if (!this.suunta) pollo.style.animationDirection = "reverse";
	else pollo.style.animationDirection = "normal";
	this.suunta = !this.suunta;
	
	//Aseta pöllölle satunnainen liikerata
	let timingFunctions = ["ease","ease-in","ease-out","ease-in-out","linear","step-start","step-end"];
	pollo.style.animationTimingFunction = timingFunctions[Math.floor(Math.random()*timingFunctions.length)];
	
	document.body.appendChild(pollo);
}

/**
 * Tee tekstiskrollerille tarpeelliset alustukset.
 * Osa on tarpeen tehdä joka kerta, kun canvasin kokoa muutetaan
 */
function alustaTeksti()
{	
	//Aseta canvas sivun levyiseksi ja tekstin korkuiseksi
	teksti.canvas.width = document.body.scrollWidth;
	teksti.canvas.height = teksti.korkeus*1.25;
	
	//Aseta tekstin koko ja hae sen mukainen pituus
	teksti.context.font = teksti.korkeus + "px sans-serif"
	teksti.pituus = teksti.context.measureText(teksti.teksti).width;
	
	//Aseta (tekstin) täytöksi liukuväri
	let gradient = teksti.context.createLinearGradient(0, 0, 0, teksti.korkeus*1.25);
	gradient.addColorStop(0, '#646400');
	gradient.addColorStop(.5, 'yellow');
	gradient.addColorStop(1, '#646400');
	teksti.context.fillStyle = gradient;
	
	teksti.alkuhetki = Date.now();
}

/**
 * Piirrä teksti alusta kuluneen ajan mukaiseen paikkaan
 */
function piirraTeksti()
{
	//Tyhjennä canvas
	teksti.context.clearRect(0,0,teksti.canvas.width,teksti.canvas.height);
	
	//Aseta tekstin paikka canvasilla kuluneen ajan mukaan
	let x = (teksti.alkuhetki-Date.now())*teksti.nopeus + teksti.canvas.width;
	//Kun teksti on rullannut loppuun, palauta se alkuun
	if (x < -teksti.pituus)
	{
		teksti.alkuhetki = Date.now();
		piirraTeksti();
		return;
	}
	//Piirrä teksti
	teksti.context.fillText(teksti.teksti,x,teksti.korkeus);
}

/**
 * Ruutupäivtys
 */
function ruutu()
{	
	piirraTeksti();
	
	piirraPupunPuolikas("vasen");
	piirraPupunPuolikas("oikea");
	
	//Pyydä uusi ruutupäivitys
	requestAnimationFrame(ruutu);
}
