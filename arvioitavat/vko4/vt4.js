"use strict";

//Rullaavan tekstin ominaisuudet
let teksti = {
	korkeus: 120,
	nopeus: 0.1,
	yoff: 100,
	teksti:
		"TIEA2120 Web-käyttöliittymien ohjelmointi -kurssin viikkotehtävä 4 taso 3 edellyttää skrollerin toteuttamista. Tämä skrolleri toimii tekstin määrästä riippumatta"
};
//Aaltoilevan tekstin ominaisuudet
let siniteksti = {
	font: "25px monospace",
	fill: "white",
	nopeus: 0.3, //Rullausnopeus (px/ms)
	//Efektit
	args: [
		{
			y: {
				A: 40, //Amplitudi (px)
				wl: 30, //Aallonpituus (merkkiä)
				f: 1 / 1000 //Taajuus (aaltoa/ms)
			},
			x: {
				A: 0,
				wl: 1,
				f: 0
			}
		},
		{
			y: {
				A: 40,
				wl: 30,
				f: 1 / 1000
			},
			x: {
				A: 160,
				wl: 60,
				f: 1 / 1000
			}
		},
		{
			y: {
				A: 40,
				wl: 30,
				f: 1 / 1000
			},
			x: {
				A: 120,
				wl: 15,
				f: 1 / 1000
			}
		}
	]
};

let kinos = [];
let pilvi = [];

window.onload = function() {
	//Lisää dokumentiin porrastetusti animoituja palkkeja
	for (let i = 0; i < 10; i++) {
		lahetaPalkki(i * 250);
	}

	//Lisää pöllöjä, kun pöllönlisäysnappia painetaan
	document.querySelector("button").addEventListener("click", lisaaPollo);

	//Alusta rullaava teksti
	teksti.canvas = document.querySelector(".teksti");
	teksti.context = teksti.canvas.getContext("2d");
	addEventListener("resize", alustaTeksti());
	alustaTeksti();

	//Aloita ruutupäivitykset
	requestAnimationFrame(ruutu);
};

/**
 * Lisää dokumenttiin animoitu palkki
 * @param {number} t Aika millisekunteina, jolla animaation aloitusta lykätään
 */
function lahetaPalkki(t) {
	//Kloonaa uusi palkki vanhasta ja aseta sille animoitu css-luokka
	//ja lykkää animaation aloitusta annetulla ajalla
	let palkki = document.querySelector(".palkki").cloneNode(true);
	palkki.setAttribute("class", "palkki_anim");
	palkki.style.animationDelay = t / 1000 + "s";
	document.body.appendChild(palkki);
}

/**
 * Piirrä canvasiin puolikas pupu
 * @param {string} puoli "vasen" tai "oikea", piirrettävä puoli
 */
function piirraPupunPuolikas(puoli, korkeus) {
	//Hae canvas ja sen context
	let canvas = document.querySelector(".pupu_" + puoli);
	let context = canvas.getContext("2d");

	//Tyhjennä canvas
	context.clearRect(0, 0, canvas.width, canvas.height);

	//Hae kuva sekä sen korkeus ja leveys
	let img = document.querySelector(".pupu_img");
	let h = img.height;
	let w = img.width / 2;

	//Piirrä oikeasta pöllöstä oikea puoli
	let xoff = 0;
	if (puoli == "oikea") xoff = w;

	//Piirretään rivi kerrallaan
	for (let i = 0; i < h; i++) {
		//Aaltoefekti:
		//Rivistä ja ajanhetkestä riippuva sini, jonka
		//perusteella satetaan piirtää naapuririvejä
		let yoff = Math.sin(i / 16 + Date.now() / 200) * 12;

		//Piirrä rivi
		context.drawImage(img, xoff, i + yoff, w, 1, 0, i, w, 1);
	}
}

/**
 * Lisää dokumentiin uusi pöllö
 */
function lisaaPollo() {
	//Kloonaa uusi pöllö vanhasta
	let pollo = document.querySelector(".pollo_anim").cloneNode();

	//Aseta joka toinen pöllö kulkemaan eri suuntaan
	if (!this.suunta) pollo.style.animationDirection = "reverse";
	else pollo.style.animationDirection = "normal";
	this.suunta = !this.suunta;

	//Aseta pöllölle satunnainen liikerata
	let timingFunctions = [
		"ease",
		"ease-in",
		"ease-out",
		"ease-in-out",
		"linear",
		"step-start",
		"step-end"
	];
	pollo.style.animationTimingFunction =
		timingFunctions[Math.floor(Math.random() * timingFunctions.length)];

	document.body.appendChild(pollo);
}

/**
 * Tee tekstiskrollerille tarpeelliset alustukset.
 * Osa on tarpeen tehdä joka kerta, kun canvasin kokoa muutetaan
 */
function alustaTeksti() {
	//Aseta canvas sivun levyiseksi
	teksti.canvas.width = document.body.scrollWidth;
	teksti.canvas.height = 300;

	//Luo alemman skrollerin fontti ja laske sen mukainen pituus
	teksti.font = teksti.korkeus + "px sans-serif";
	teksti.context.font = teksti.font;
	teksti.pituus = teksti.context.measureText(teksti.teksti).width;

	//Aseta alemman skrollerin liukuväri
	let gradient = teksti.context.createLinearGradient(
		0,
		teksti.yoff,
		0,
		teksti.yoff + teksti.korkeus * 1.25
	);
	gradient.addColorStop(0, "#646400");
	gradient.addColorStop(0.5, "yellow");
	gradient.addColorStop(1, "#646400");
	teksti.gradient = gradient;

	//Laske ylemmän skrollerin fontin mukainen kokonaispituus ja yhden merkin pituus
	teksti.context.font = siniteksti.font;
	siniteksti.pituus = teksti.context.measureText(teksti.teksti).width;
	siniteksti.merkkipituus = teksti.context.measureText("m").width;

	//Aseta molemmat skrollerit alkuun
	teksti.alkuhetki = Date.now();
	siniteksti.alkuhetki = Date.now();
}

/**
 * Piirrä rullaavat tekstit
 */
function piirraTeksti() {
	let t = Date.now();

	//Tyhjennä canvas
	teksti.context.clearRect(0, 0, teksti.canvas.width, teksti.canvas.height);

	//Vaihda alemman skrollerin tyyliin
	teksti.context.font = teksti.font;
	teksti.context.fillStyle = teksti.gradient;

	//Aseta tekstin paikka canvasilla kuluneen ajan mukaan
	let x = (teksti.alkuhetki - t) * teksti.nopeus + teksti.canvas.width;
	//Kun teksti on rullannut loppuun, palauta se alkuun
	if (x < -teksti.pituus) {
		teksti.alkuhetki = t;
		piirraTeksti();
		return;
	}
	//Piirrä teksti
	teksti.context.fillText(teksti.teksti, x, teksti.korkeus + teksti.yoff);

	//Vaihda ylemmän skrollerin tyyliin
	teksti.context.fillStyle = siniteksti.fill;
	teksti.context.font = siniteksti.font;

	//Siirrä tekstiä vasemmalle rullausnopeuden mukaisesti
	let xs =
		(siniteksti.alkuhetki - t) * siniteksti.nopeus + teksti.canvas.width;

	//Haei nykykyinen efekti (x- ja y-sinien argumentit)
	let args = siniteksti.args[0];

	//Piirrä jokainen merkki erikseen
	for (let i in teksti.teksti) {
		//Laske x- ja y-siirtymät sineistä nykyisen efektin argumenteilla
		//vrt. https://en.wikipedia.org/wiki/Wavelength#Mathematical_representation
		let y =
			args.y.A * Math.sin(2 * Math.PI * (i / args.y.wl - args.y.f * t));
		let x =
			args.x.A * Math.sin(2 * Math.PI * (i / args.x.wl - args.x.f * t));

		//Piirrä merkki
		teksti.context.fillText(
			teksti.teksti[i],
			xs + i * siniteksti.merkkipituus + x,
			y + 60
		);
	}

	//Kun teksti on rullannut loppuun palauta se alkuun ja vaihda efektiä
	if (xs < -siniteksti.pituus) {
		siniteksti.alkuhetki = t;
		siniteksti.args.push(siniteksti.args.shift());
	}
}

function luoHiutale() {
	let w = 28; //Hiutaleen kuvan leveys
	let h = 22; //Hiutaleen kinoskorkeus (< kuin kuvan korkeus)
	let xtol = 4; //Hiutaleen kuvaleveyden ja kinosleveyden erotuksen puolikas
	//Putoamisanimaation enimmäisaika (sivun pohjalle asti) sekunteina
	let anim_t = 10;
	//Aika millisekunteina, jonka sisällä samaan x-koordinaattiin lisätyt hiutaleet olisivat päällekkäin
	let tyhj_t = (h / anim_t) * 1000;

	//Arvo hiutalelle paikka näytöltä
	let x = Math.floor(Math.random() * (document.body.scrollWidth - w));
	let x1 = x + xtol;
	let x2 = x1 + w - xtol;

	//Etsi ylin hiutale alapuolisesta kinoksesta
	let ylin = 0;
	//Käy läpi kinoksen hiutaleen alla olevat x-koordinaatit
	for (let x = x1; x <= x2; x++) {
		//Jos hiutale syntyisi toisen hiutaleen päälle, älä luo hiutasta
		if (Date.now() - pilvi[x] < tyhj_t) return;
		//Tarkista y-taulukko, jos se on olemassa
		if (kinos[x]) {
			//Tarkista, onko y-taulukossa tunnettua ylempää hiutaletta
			for (let y = ylin; y < kinos[x].length; y++) {
				if (kinos[x][y]) {
					ylin = parseInt(y) + 1;
				}
			}
		}
		//Alusta x-koordinaatin y-taulukko (tarvitaan myöhemmin)
		else {
			kinos[x] = [];
		}
	}

	//Varaa hiutaleelle paikka kinoksesta ja pilvestä
	for (let x = x1; x <= x2; x++) {
		pilvi[x] = Date.now();
		kinos[x][ylin] = true;
	}

	//Laske kinoksen päällä olevan hiutaleeen absoluuttinen korkeus sivun pohjasta
	let y = h * ylin;
	//Laske animaation kesto niin, että kaikki hiutaleet putoavat samalla vauhdilla
	let aika =
		anim_t *
		((document.body.scrollHeight - y) / document.body.scrollHeight);

	//Luo uusi hiutale mallihiukkasen kloonina ja
	//aseta sille uudet ominaisuudet
	let lumi = document.querySelector(".lumi").cloneNode();
	lumi.setAttribute("class", "lumi_anim");
	lumi.style.left = x + "px";
	lumi.style.bottom = y + "px";
	lumi.style.animationDuration = aika + "s";
	document.body.appendChild(lumi);
}

/**
 * Ruutupäivtys
 */
function ruutu() {
	//Piirrä rullaavat tekstit
	piirraTeksti();

	//Piirrä aaltoilevat pupunpuolikaat
	piirraPupunPuolikas("vasen");
	piirraPupunPuolikas("oikea");

	//Luo silloin tällöin uusi hiutale
	if (Math.random() < 0.05) luoHiutale();

	//Pyydä uusi ruutupäivitys
	requestAnimationFrame(ruutu);
}
