// voit tutkia tarkemmin käsiteltäviä tietorakenteita konsolin kautta 
// tai json-editorin kautta osoitteessa http://jsoneditoronline.org/
// Jos käytät json-editoria niin avaa data osoitteesta:
// http://appro.mit.jyu.fi/tiea2120/vt/vt1/2018/data.json
// http://appro.mit.jyu.fi/tiea2120/vt/vt1/

// Seuraavilla voit tutkia käytössäsi olevaa tietorakennetta. Voit ohjelmallisesti
// vapaasti muuttaa selaimen muistissa olevan rakenteen sisältöä ja muotoa.

// console.log(data);

// console.dir(data);

// Kirjoita tästä eteenpäin oma ohjelmakoodis

"use strict";

// tiea2120 - Viikkotehtävä 1
// tasot 1 - 5
// author: Janne "Jaba[2.6]" Siltainsuu

// authors comments: i went a little bit overboard with the commenting, 
// but for the first time I actually knew every single line of the code
// this code was also alot more easier to change during the process because
// it was very well explained what the code actually does
// this code is highly inefficient, the loops go around way too many times
// i would actually like to build a database about tupa and sarja and use that
// in the backend to get all the data, mut then again it would be backend programming then... :D

/*###########################################
  Course functions start after this comment
############################################*/ 

//Returns all the teams provided in the data json file
function getTeamNames() {
	// the variable where team names will be stored
	var teams = []
	// Starts a loop that goes trough all the series in the data file
	for (let i in data.sarjat) {
		// starts another loop that goes trough the teams in the sarja
		for (let j in data.sarjat[i].joukkueet) {
			// assigns the team as a variable, so the code will be more readable
			var joukkue = data.sarjat[i].joukkueet[j];
			// prints out the team name
			teams.push(joukkue.nimi);
		}
	}
	// returns the team names
	return teams
}

//adds a team from json string provided in the parameters
function addTeam(team, series) {
	// Starts a loop that goes trough all the series in the data file
	for (let i in data.sarjat) {
		// checks if the correct sarja is found and adds the team to the sarja
		if(data.sarjat[i].nimi == series) {
			// when the condition is met the team gets pushed to the end of the array
			// and then the loop ends beause there is nothing to do anymore
			data.sarjat[i].joukkueet[data.sarjat[i].joukkueet.lenght+1] = team;
			// loop exits
			break;
		}
	}
}

function deleteTeam(name) {
	// Starts a loop that goes trough all the series in the data file
	for (let i in data.sarjat) {
		// starts another loop that goes trough the teams in the sarja
		for (let j in data.sarjat[i].joukkueet) {
			// assigns the team as a variable, so the code will be more readable
			var joukkue = data.sarjat[i].joukkueet[j];
			// finds out if this team is the one that we are looking for
			// transfers everything to lovercase so that the function is not case sensitive
			if(joukkue.nimi.toLowerCase() == name.toLowerCase()) {
				// removes the team 
				delete data.sarjat[i].joukkueet[j];
				// exits the function since team has been deleted and nothing to do anymore
				return
			}
		}
	}
}


// prints out all the rastit that start with a number
function rastitThatStartWithANumber() {
	// initialize a variable to be used to append all the rastit that start with a number
	var rastikoodit = ""
	// go trough all the rastit
	for (let i in data.rastit) {
		// get first a single rasti in a variable for simplisity
		var rasti = data.rastit[i]
		// get the koodi from this praticular rasti that we are examining
		var koodi = rasti.koodi
		// get the first character to be examined
		var firstDigit = koodi.substring(0,1);
		// lets call our custom function to determine is it a whole number
		if(isWholeNumber(firstDigit)) {
			// if the first code is appended then no ; is added
			if(rastikoodit.length == 0) {
				rastikoodit += koodi;
			// after that the ; is added in front of the so that the first and last do not look awkward
			} else {
				rastikoodit += ";" + koodi;
			}
		}
	}
	//output the rastikoodit to the console
	console.log(rastikoodit)
}

// this is a collection of few functtions that work in order to provide the teams in a certain order
function getTeamsAndScores() {
	// This is the variable where the teams and the scores will stored
	var teamsAndScores = []
	// Starts a loop that goes trough all the series in the data file
	for (let i in data.sarjat) {
		// starts another loop that goes trough the teams in the sarja
		for (let j in data.sarjat[i].joukkueet) {
			// assigns the team as a variable, so the code will be more readable
			var joukkue = data.sarjat[i].joukkueet[j];
			// prints out the team name
			var score = getTeamScoreById(joukkue.id)
			// pushes the score to the dictionary as a key value pair
			teamsAndScores.push({
				// stores the team name in the key name
			    name:   joukkue.nimi,
			    // stores the score of the team in the value score
			    score: 	score
			});
		}
	}
	// sorts the dictionary by using the function compare
	teamsAndScores.sort(compare)
	
	// returns an array that has the teams and scores in array
	return teamsAndScores
}

// is the spesific compare function
// took a while to figure this out, but this has been found and modified from the following url:
// https://www.sitepoint.com/sort-an-array-of-objects-in-javascript/
function compare(a, b) {
	// introducing the score variables 
	const scoreA = a.score;
	const scoreB = b.score;

	// introduces the comparison variable
	let comparison = 0;
	// if the scoreA is grether than score b -1 is set to comparison
	// this had to be reversed from the script that I found
	if (scoreA > scoreB) {
		comparison = -1;
	// if the scoreb is grether than scorea 1 is set to comparison
	} else if (scoreA < scoreB) {
		comparison = 1;
	}
	// returns either -1, 0 or 1
	return comparison;
}


// is the spesific compare function
// took a while to figure this out, but this has been found and modified from the following url:
// https://www.sitepoint.com/sort-an-array-of-objects-in-javascript/
function compareWithTime(a, b) {
	// introducing the score variables 
	const scoreA = a.score;
	const scoreB = b.score;
	const timeA = a.time;
	const timeB = b.time;

	// introduces the comparison variable
	let comparison = 0;
	// if the scoreA is grether than score b -1 is set to comparison
	// this had to be reversed from the script that I found
	if (scoreA > scoreB) {
		comparison = -1;
	// if the scoreb is grether than scorea 1 is set to comparison
	} else if (scoreA < scoreB) {
		comparison = 1;
	} else if(scoreA == scoreB) {
		if (timeA < timeB) {
			comparison = -1;
			// if the scoreb is grether than scorea 1 is set to comparison
		} else if (timeA > timeB) {
			comparison = 1;
		}
	}
	// returns either -1, 0 or 1
	return comparison;
}

// gets a teams score by using the teams id number
// this function was created inorder to make the problem 
// in pieces that are not very big, so that it is possible to
// understand the code better
function getTeamScoreById(id) {
	// introduces the variable where the teams score is saved
	// default value is 0 because there might be a situation where the team has no score
	var teamScore = 0;
	// introduces a list of rasti which have been put in to the record, to make sure not a single rasti is calculeted twice
	var rastitInRecord = []
	// starts a loop that goes trough all the tupa markings
	for (let i in data.tupa) {
		// gets the tupa that is being handeled at this time of the loop
		var tupa = data.tupa[i]
		// gets the id of this praticular tupa marking
		var tupaId = tupa.joukkue
		// if this tupaId matches the id of the team we are examining we start to calculate scores
		if(tupaId == id) {
			// introduces a variable rasti that is the praticular rasti we are looking for
			var rasti = tupa.rasti
			// lets look trough the custom function has this rasti been already inserted to the scores
			// NOTE that the isValueInArray needs to be false inorder for the score to be added
			if(!isValueInArray(rasti, rastitInRecord)) {
				// if we are here it means that it has not been inserted in to the scoreboard
				// score is added to the teamscore variable by using a custom function that returns 
				// integer if this rasti gives out scores.
				// if this rasti does not give out any score, then the function gives back a 0 that is a neutral value
				teamScore += getRastiScoreById(rasti);
				// we are going to push the rasti to the array that holds the rastit that have already been calculated
				rastitInRecord.push(rasti)
			} 
		}	
	}	
	// returns the integer that holds the teams score that is >= 0
	return teamScore
}

// this get a rasti-information by examining the rasti id "rid"
function getRastiScoreById(rid) {
	// starts a loop that goes trough all the rastit in the data structure
	for (let i in data.rastit) {
		// introduces a variable that holds the praticular rasti that is being handeled to make the code more readable
		var rasti = data.rastit[i]
		// tests if this praticular rasti id is the same that we are examining
		if(rasti.id == rid) {
			// uses the custom function to get the rasti score from this praticular rasti
			return getRastiScore(rasti.koodi)
		}	
	}
	// there was nothing to return so we are going to return 0 that is a neutral value in this context
	return 0
}

// gets rasti if it mathes rid provided in the parameters
function getRastiById(rid) {
	// starts a loop that goes trough all the rastit in the data structure
	for (let i in data.rastit) {
		// introduces a variable that holds the praticular rasti that is being handeled to make the code more readable
		var rasti = data.rastit[i]
		// tests if this praticular rasti id is the same that we are examining
		if(rasti.id == rid) {
			// return rasti
			return rasti
		}	
	}
	// if rasti was not found returns false
	return false
}

// gets the rasti score from the rastikoodi which is sent as a parameter
function getRastiScore(koodi) {
	// if code for some reason is blank then a zero will be returned
	// this is here to prevent errors
	if(koodi.length == 0) {
		return 0
	}
	// introduces a variable that will hold the first digit of the rasti koodi
	var firstDigit = koodi.substring(0,1);
	// if the number is actually a whole number then the integer is parsed and returned as an interer for point calculation
	if(isWholeNumber(firstDigit)) {
		// if the firstdigit of the rasti is a number it gets parsed to an integer and sent back
		return parseInt(firstDigit)
	} else {
		// if it was not an integer a 0 is returned since it is a neutral value
		return 0
	}
}

// returns all the rastit visited by the team)
function getRastitVisitedByTeam(teamId) {
	// this is where the rastit will be stored
	var rastit = [];
	// loops trough all the tupa markings
	for(let i in data.tupa) {
		// initialized a variable to keep the code more readable
		var tupa = data.tupa[i];
		// tests if this tupa marking belongs to the tam that we are examining
		if(tupa.joukkue == teamId) {
			// gets the rastiId of the tupa mrking
			var rid = tupa.rasti;
			// gets the rasti by using a custom function
			var rasti = getRastiById(rid);
			// time of rasti visit for later use
			var time = tupa.aika
			// if the rasti was not found the custom function returns false, so this will not be executed then
			if(rasti) {
				// time added to the data
				rasti["time"] = time;
				rastit.push(rasti)
			}
		}	
	}
	return rastit
}

// returns distance traveled by team by the id of the team
function getDistanceTraveledByTeam(teamId) {
	// initializes a array where rastit will be stored
	var rastit = getRastitVisitedByTeam(teamId);
	// initializes a variable for the distance to be stored in
	var distance = 0;
	// loops trough the rastit that we found earlier
	for (var i = 0; i < rastit.length-1; i++) {
		// rasti 1 that is fetched
		var rasti1 = rastit[i];
		// rasti 2 that is fetched
		var rasti2 = rastit[i + 1];
		// testing that all the variables are set, if they are not set then distance is not calculated
		if((rasti1.lat > 0) && (rasti1.lon > 0) && (rasti2.lat > 0) && (rasti2.lon > 0)) {
			distance += getDistanceFromLatLonInKm(rasti1.lat, rasti1.lon, rasti2.lat, rasti2.lon);
		}
	}
	// returns the distance traveled by a spesific team
	return distance
}

// gets the time that it took for the team to go trough the competition
function getTimeUsedByTeam(teamId) {
	// gets the rastit visited by the team from a custom funtion
	var rastit = getRastitVisitedByTeam(teamId);
	// if the team has not went to any rastit, then there is no time to record so there time will be marked as zero
	if(rastit.length == 0) {
		// returns zero
		return "00:00:00"
	}
	// starts to get the first rasti as a date object
	var firstRastiTime = new Date(rastit[0]["time"]);
	// gets the last rasti as a date object for starters
	var lastRastiTime = new Date(rastit[rastit.length-1]["time"]);
	// starts a loop that checks if the first and last rasti is really first and last
	for(let i in rastit) {
		// gets the current rasti that is being examined as a date object
		var rastiTime = new Date(rastit[i]["time"])
		// checks if it is visited before the actual last rasti
		// this actually happens at least 17 times during run time
		if(rastiTime.getTime() < lastRastiTime.getTime()) {
			// because that happens we change the last rasti time
			lastRastiTime = new Date(rastit[i]["time"]);
		}
		// checks if the first rasti is really the first visited
		if(rastiTime.getTime() > firstRastiTime.getTime()) {
			// because this also happens really often the time of first rasti is changed
			firstRastiTime = new Date(rastit[i]["time"]);
		}
	}
	// calculates the difference between start and finnish times
	var timeUsed = new Date(firstRastiTime - lastRastiTime)
	// formes a string out of the found results and also adds leading zero if the time looks funky, ex: 7:1:8 --> 07:01:08
	var timeUsedStr = (insertLeadingZero(timeUsed.getUTCHours()) + ":" + insertLeadingZero(timeUsed.getMinutes()) + ":" +  insertLeadingZero(timeUsed.getSeconds()))
	// returns the string that was formed
	return timeUsedStr
}

// prints out the information required in taso five 
// EX: Dynamic Duo, 206 p, 43 km, 07:49:45
function printAllInformationOfTeamCompetition() {
	var teaminformation = []
	for (let i in data.sarjat) {
		// starts another loop that goes trough the teams in the sarja
		for (let j in data.sarjat[i].joukkueet) {
			// assigns the team as a variable, so the code will be more readable
			var joukkue = data.sarjat[i].joukkueet[j];	
			// pushes the information that is gathered to the data structure
			teaminformation.push({
				// stores the team name in the key name
			    name:   joukkue.nimi,
			    // stores the score of the team in the value score
			    score: 	getTeamScoreById(joukkue.id),
			    // stores the distance traveled by the team
			    distance: Math.round(getDistanceTraveledByTeam(joukkue.id)),
			    //stores the time that the team used
			    time: getTimeUsedByTeam(joukkue.id)
			});
		}
	}

	// this loop here id for finding out id the comparison with the time really works 
	// by setting manually the points of team onnennokkijat to 295. After that the print looks like this:
	// Onnenonkijat, 295 p, 49 km, 07:52:32 vt1.js:403:3
	// Kahden joukkue, 295 p, 55 km, 08:05:38
	// So it works as it should
	/*for (let i in teaminformation) {
		if(teaminformation[i]["name"] == "Onnenonkijat") {
			teaminformation[i]["score"] = 295;
		}
	}*/
	// sorts out the team in the point order by using the custom compare function written earlier
	teaminformation.sort(compareWithTime)
	// prints out all the info in a nice fashion
	for (let i in teaminformation) {	
		// prints out the information. The .trim is added because there is some whitespace in the teamnames and this resolves the problem easily
		// trim ex --> "Kaakelin putsaajat ," --> "Kaakelin putsaajat,
		console.log(teaminformation[i]["name"].trim() + ", " + teaminformation[i]["score"] + " p, " + teaminformation[i]["distance"] + " km, "  + teaminformation[i]["time"])
	}
}


/*###########################################
 Pure utility functions to make coding easier
############################################*/ 

// Printing breaks to ease the checking of the tasks later on.
// Also makes it easier to locate stuff for the debugging in console
function printBreakWithMessage(message) {
	// appends a "tolppa" in front of the message
	message = "| " + message
	// makes appends box corner
	var line = "+"
	console.log();
	// appends a line to make the box the correct size
	for (var i = 0; i < message.length; i++) {
		// ase many times as the message requires a line is appended
		line += "-"
	}
	// ends the line corner
	line += "+"
	// prints a line
	console.log(line);
	// prints the message
	console.log(message += " |");
	// prints a line under the message
	console.log(line);
	console.log();
} 

// checks is the number a whole number or not
// in a different function, to keep code more readable.
function isWholeNumber(num) {
	// test if the not a number is false and is decimal number
	if (!isNaN(num) && num % 1 == 0) {
		// if the number indeed is a number and is not a decimal number true is returned
    	return true;
	} 
	// if the number is something else then false is returned
	return false;
}

// tests if a value that is passed in the parameters is in the array
function isValueInArray(value, array) {
	// starts the loop of the array passed
	for (let i in array) {
		// if the value is in the array then true is returned
		// values are casted in to string so that the conditions are equal
		if(array[i].toString() == value.toString()) {
			// value was found from the array
			return true
		}
	}
	// value was not found from the array
	return false
}

// is built so that not every function in the program has to print stuff
function printArray(array) {
	// prints all the items in the array
	for (let i in array) {
		console.log(array[i]);
	}
}

// is built so that not every function in the program has to print stuff
function printTeamsAndScores(array) {
	// prints out the team as requested in the task
	for (let i in array) {
		// example of print: Onnenonkijat (273)
		console.log(array[i]["name"] + " (" + array[i]["score"] + ")")
	}
}

// if the number is less than 0 it adds a leading zero. EX: 1 --> 01
function insertLeadingZero(number) {
	// if number is less than ten a leading zero is added
	// ex 8 --> 08
	if(number < 10) {
		// casted the integer to string
		var numberStr = number.toString()
		// 8 --> 08
		numberStr = "0" + numberStr
		return numberStr
	}
	//if there was nothing to do the actual number is returned as a string
	return number.toString()
}

/* ###########################################
Functions that make it possible to print dist
############################################*/ 

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
	var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2-lat1);  // deg2rad below
	var dLon = deg2rad(lon2-lon1); 
	var a = 
		Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
		Math.sin(dLon/2) * Math.sin(dLon/2)
		; 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c; // Distance in km
	return d;
}

function deg2rad(deg) {
	return deg * (Math.PI/180)
}

/* ###########################################
After this the programm gets actually executed 
############################################*/ 

/*
TASO 1 STARTS HERE
*/

// prints the breaking message
printBreakWithMessage("TASO 1 STARTS HERE BY PRINTING THE TEAM NAMES");

// gets all the team names from the function getTeamNames and sents it to a custom function printArray that prints the contents
printArray(getTeamNames());

// the sample team that needs to be added
var mallijoukkue = { 
      "nimi": "Mallijoukkue",
      "last": "2017-09-01 10:00:00",
      "jasenet": [
        "Tommi Lahtonen",
        "Matti Meikäläinen"
      ],
      "id": 99999
}

// prints the breaking message
printBreakWithMessage("PRINTING THE TEAM NAMES PLUS THE ADDED TEAM");
// Adds mallijoukkue to the correct series that is provided in the parameters
// adds the team that is declared few lines before
addTeam(mallijoukkue, "4h");
// gets all the team names from the function getTeamNames and sents it to a custom function printArray that prints the contents
// to prove that the team "mallijoukkue" was added
printArray(getTeamNames());

// prints the breaking message
printBreakWithMessage("Prints all the rastit which start with a whole number");
rastitThatStartWithANumber();

// prints the breaking message
printBreakWithMessage("TASO 1 ENDS HERE AND TASO 3 STARTS HERE, STARTS WITH DELETING THREE TEAMS: Vara 1, Vara 2 & Vapaat");

/*
TASO 1 ENDS HERE and TASO 3 starts here
*/

// Deletes teams from the data structure
deleteTeam("Vara 1")
deleteTeam("Vara 2")
deleteTeam("Vapaat")

printArray(getTeamNames());
// prints the breaking message
printBreakWithMessage("CALCULATING THE SCORE OF THE TEAMS AND PRINTING TEAMS IN FORMAT: *teamname* (*score*)");
// gets the teams and scores. Then sends it to function that prints it.
printTeamsAndScores(getTeamsAndScores());

// prints the breaking message
printBreakWithMessage("TASO 3 ENDS HERE AND TASO 5 STARTS HERE, PRINTING ALL THE INFO GATHERED");

/*
TASO 3 ENDS HERE and TASO 5 starts here
*/

// gathers all the info and prints it
printAllInformationOfTeamCompetition()





