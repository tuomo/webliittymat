Hyvin huolellisesti tehty, hyvin myös kommentoitu. Koodin laatu on about niin hyvä kuin tällaisessa yhden kooditiedoston DOM-manipulointitehtävässä voi olla. Sisennykset myös pääosin oikein (ehkä väärin sisennettyä riviä vaan löysin) eikä var:ia ei käytetty paikallisissa rakenteissa.

Jännästi Google Chromella ei näytä toimivan tuo joukkueen tietojen siirto tuohon editointikenttään. Eli kun klikkaat jotain joukkuetta tulee valitusta konsoliin ja mitään ei tapahdu. En nyt tästä kehtaa antaa pistepenaltyä yli tuhannen rivin koodipohjassa pelkästään yhteen viikkotehtävään, joka ainakin Firefoxilla toimii muuten aika moitteetta. Puuduttava homma oli ja olet selkeästi panostanut paljon tähän. 

Pikkuhuomioita:
- Joukkueiden jäseniä ei vissiin voi poistaa? En ainakaan keksinyt miten.
- Lisäksi nuo joukkueen rastileimaukset olisi vissiin pitänyt tulostaa tuohon loppuun tehtävänannon mukaan?

Lisäksi ei löydy kuin hyvin pientä asenteellista nipotettavaa arvioijalta koodin selkeyttämiseksi. Älä ota henkilökohtaisesti, tässä vaan liuta omia pikkuvinkkejä mitä harkita jatkossa:
- koodisi on aika "Javamaista". Eli hartaasti perusrakenteilla (if-else, for-loopit) kaikki asiat juurta jaksaen tehtynä. Lisäksi "Vesa Lappalaismaisesti" lopetat useiden funktioiden suorittamisen kesken funktion tyhjällä return:lla. Näinkin voit tehdä, mutta et löydä juuri moderneja JS-kirjastoja, joiden lähdekoodissa control flow toimii näin. Parempi että et lisäile näitä turhia lopetus -iffejä vaan et vaan palauta mitään jos ehdot ei täyty. Tai palautat null, false, undefined tms.
- ES2015 jne. Standardien myötä Javascript on kehittynyt varsinkin syntaksinsa osalta huimasti selkeämpään suuntaan, kannattaa tutkia dokumentaatiota ja katsoa löytyisikö sieltä uusia hyviä juttuja. Listaan tässä muutaman
- if -> if -> if -control flow:n voit muuntaa suoraan switch-case:ksi, koska ainoastaan yksi vaihtoehto voi olla valittuna
- kannattaa dokumentoida myös funktioiden paluuarvo @return tai @returns tagilla. Jos siis ylipäätään palauttaa mitään.
- Käytä template literalseja stringejä mudoostaessa. Sillon ei tarvitse tehdä: otsikot[i] + " "... eli -> `${otsikot[i]}} `
- if (kumpilomake === true) -> voit korvata if (kumpilomake). Javascriptissä on "truthy" arvot, jotka on käteviä if-lausekkeissa. Muualla ne oikeastaan vaan räjäyttää runtimen.
- pohdi jos taulukoiden osalta funktionaalisempi tyyli olisi helpompaa kuin monta kertaa sisennetyt for-loopit. Eli const suodatetut = data.sarjat.map(/* ... */).filter(/* ... */). Jännästi joissain kohdissa olet nohevasti näitä käyttänytkin, esim. noissa sort -funktioissa. Myös taulukko.foreach on kätevä(mpi kuin perinteinen for-loop) 
- Jotkut kirjoittamasi funktiot pystyisit korvaamaan JS:n omilla funktioilla. esim. tuo rivin 241 onkoSamannimistä. Ton vois korvata esim. suoraan: Array.prototype.includes()
- Käytät ylläolevaa muutenkin vaan yhdesti koko koodipohjassa, niin se kannattaa tehdä suoraan tuolla kutsuvan funktion sisällä tuolla JS:n valmiilla funktiolla
- Toinen esimerkki; rivin 88 listaaJasenet(). Korvaa array.join()
- lambdat pystyy nykyään kirjoittamaan muodossa arg => returnvalue. Selkeyttää vähän kun ei tarvitse kirjoittaa pitkän kaavan mukaan function(a) {return b}. Esim callbackeissa kätevä
- itse käytän const:ia sillon kun data ei muutu mutta eipä tästä nyt jaksa sen enempää nipottaa kun ei käytännössä tee mitään eroa, let käy myös hyvin
- joissain kohdissa saattaa olla helpompaa käyttää funktion default argumentteja. Esim. jossain järjestyksen valinnassa. Eli function jarjestaKaikki(jarjestys = "aakkoset") { /*...*/ }
- destructuring assignmentiä kannattaa käyttää. Silloin saat ulos samannimisen paikallisen muuttujan kuin objektin avainkentän arvo on. Eli esim. let rastiId = muokattavaJoukkue.rastit[indeksi].id ---> const { id } = muokattavaJoukkue.rastit[indeksi]. Opettajan mielestä tämä ei hyvä tapa, mutta esim. Eslint:n yleisin "suositeltu" -konfiguraatio, sekä AirBnB:n ja muiden tekemät konfiguraatiot nimenomaan painottavat tämän käyttöä ja varoittavat, jos et käytä sitä tarvittaessa. Myös Facebook käyttää sitä kaikissa JS-kirjastoissaan ja dokumentaatiot opastavat käyttämään sitä. Lisätietoja: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
- Ternary operator on näppärä, esim. rivillä 269-272 voisi kokonaan korvata onelinerillä; painike.disabled ? taytetty === vaadittujaKenttia ? true : false. Opettaja tästäkin eri mieltä, mutta myös tämä on yleisesti suositeltuja käytänteitä modernissa front-end kehityksessä. Harkitse ja tee oma päätös mitä käyttää
- Kannattaa laittaa Eslintti tai joku muu fiksu lintteri editoriin ja sitten autoformat/prettier -laajennus. Eli sisentää ja muutenkin formatoi koodin oikein tallentaessa. Niin ei tarvitse itse nyhvätä puolipisteiden, sisennysten ja whitespacejen kanssa. Lintteri äkäiselle huutamaan joka pikkuvirheestä niin oppii oikeat käytänteet hetkessä
- Tilan ylläpito muutamalla globaalilla koodipohjassa sivuvaikutuksellisesti mutatoitavalla muuttujalla on järkyttävää ja vastoin kaikkia hyviä käytänteitä. Mutta eihän tässä tehtävässä nyt jaksanut mitään fiksua tilakonettakaan rakentaa, joten niin itsekin tein. Tilan ylläpitoon suurissa koodipohjissa kannattaa kuitenkin tutustua esim. Redux:n.
- Fiksusti olet käyttänyt slice():ä splice():n sijaan. Splice mutatoi dataa ja on siksi kuolemanvaarallinen käyttää

Arvioija: tuhopia@student.jyu.fi