// data-muuttuja on sama kuin viikkotehtävässä 1.

"use strict";

window.onload = function() {
    //console.log(data);
    var rastilomakeenOtsikot = ["Rastin tiedot","Lat","Lon","Koodi"],
        rastilomakeenPainike = ["rasti", "rasti", "Lisää rasti"],
        joukkuelomakkenOtsikot = ["Uusi joukkue", "Nimi*"],
        joukkuelomakkenOtsikot2 = ["Jäsenet", "Jäsen 1*", "Jäsen 2*"],
        joukkuelomakkeenPainike = ["joukkue", "lisaaJoukkue", "Lisää joukkue"];
    
    luoOmaTietorakenne();
    laskeTulokset();
    oletusjarjestys();
    pyoristaTulokset();
    luoTaulukko();
    document.getElementsByTagName("form")[0].appendChild(luoLomake(rastilomakeenOtsikot,[],rastilomakeenPainike,false)); //lisää lomakkeen sivulle
    document.getElementsByTagName("form")[2].appendChild(luoLomake(joukkuelomakkenOtsikot,joukkuelomakkenOtsikot2,joukkuelomakkeenPainike,true)); //lisää lomakkeen sivulle
    luoleimalomake();
};
  
var tulokset = [];  //oma tietorakenne
var muokattavaJoukkue = [];
var seuraavaInputIdNro = 0;
var vaadittujaKenttia = 3;
var taytettyjaKenttia = [0,0,0];
var taulukonPaaotsikot = {"sarja":"Sarja", "nimi":"Joukkue", "pisteet":"Pisteet", "matka":"Matka", "aika":"Aika"};
var seuraavaJoukkueId = 1000000000000000; 
//Id-numeroa kasvatetaan aina yhdellä eli ei mahdollista, että olisi samalla id-numerolla toista 
//joukkuetta ellei lisätä yli 4000000000000000 joukuetta. Tämän takia ei id tarkistusta :)


/**
 * Luo taulukon tulokset-tietorakenteesta ja lisää taulukon tupa-lohkoon
 */
function luoTaulukko() {
    let taulukko = document.createElement("table"),
        otsikko = document.createElement('caption'),
        otsikkoRivi = document.createElement("tr");
    taulukko.id = "taulukko";
    otsikko.appendChild(document.createTextNode("Tulokset"));
    taulukko.appendChild(otsikko);

    for (let key in taulukonPaaotsikot) {
        let th = document.createElement("th"),
            sarakeLinkki = document.createElement("a");
        sarakeLinkki.setAttribute("href","");
        sarakeLinkki.id = key;
        sarakeLinkki.addEventListener("click", klikkijarjestelija);
        sarakeLinkki.appendChild(document.createTextNode(taulukonPaaotsikot[key])); 
        th.appendChild(sarakeLinkki);
        otsikkoRivi.appendChild(th);
    }
    taulukko.appendChild(otsikkoRivi);

    for (let i in tulokset) {
        let rivi = document.createElement("tr");

        for (let j in taulukonPaaotsikot) {
            if (j === "nimi") {
                let sarake =  document.createElement("td"),
                    linkki = document.createElement("a"),
                    rivinvaihto = document.createElement("br");
                linkki.setAttribute("href","#joukkue");
                linkki.addEventListener("click", siirraJoukkueLomakkeeseen);
                linkki.appendChild(document.createTextNode(tulokset[i][j])); 
                sarake.appendChild(linkki);
                sarake.appendChild(rivinvaihto);
                sarake.appendChild(document.createTextNode(listaaJasenet(tulokset[i])))
                rivi.appendChild(sarake);
            } else {
                let sarake =  document.createElement("td");
                sarake.appendChild(document.createTextNode(tulokset[i][j]));
                rivi.appendChild(sarake);
            } 
        }
        taulukko.appendChild(rivi);
    }
    document.getElementById("tupa").appendChild(taulukko);
}

/**
 * Ottaa joukkueen jäsenet ja tekee niistä yhden stringin.
 * palauttaa jäsenet pilkuilla erotettuina
 * @param {*} joukkueenTiedot käsiteltävä joukkue
 */
function listaaJasenet(joukkueenTiedot) {
    let jasenet = "",
        erotin = "";
    for (let i of joukkueenTiedot.jasenet) {
        jasenet = jasenet.concat(erotin + i)
        erotin = ", ";
    }
    return jasenet;
}

/**
 * Päivittää data-tietorakenteen tiedot omaan tietorakenteeseen, joka
 * järjestetään ja tulokset siistitään taulukkoa varten. Vanha taulukko
 * poistetaan ja päivitetty taulukko lisätään.
 */
function paivitaTaulukko(lajitteluperuste,tyhjataankoLeimat) {
    luoOmaTietorakenne();
    laskeTulokset();
    if (lajitteluperuste === "sarja" || lajitteluperuste ==="")
        oletusjarjestys()
    if (lajitteluperuste === "nimi") 
        aakkosjarjestys()
    if (lajitteluperuste === "pisteet")
        pistejarjestys()
    if (lajitteluperuste=== "matka")
        matkajarjestys()
    if (lajitteluperuste === "aika")
        aikajarjestys()
    pyoristaTulokset();
    var vanha = document.getElementById("taulukko");
    vanha.parentNode.removeChild(vanha);
    luoTaulukko();
    
    if (tyhjataankoLeimat) {
        alustaLomake();
        tyhjaaLeimat();
    } else {
        paivitaLeimat();
    }
}

/**
 * käsittelee taulukon otsikon painalluksen ja lähettee päivitys
 * aliohjelmalle tiedon, jonka mukaan taulukko järjestetään.
 * @param {*} event linkki, joka aiheutti tapahtuman
 */
function klikkijarjestelija(event) {
    event.preventDefault();
    paivitaTaulukko(event.target.id,false);
}

/**
 * Lisää klikatun joukkueen tiedot globaaliin muuttujaan ja täyttää joukkueen 
 * nimen ja jäsenet joukkuelomakkeeseen muokkausta varten. Joukkuelomakkeeseen 
 * lisätään syöttäkenttiä lisää, jos jäseniä on enemmän kuin yksi.
 * Vara 1-4 joukkueita ei voi muokata ellei lisätä toista jäsentä.
 * @param {*} event linkki, joka aiheutti tapahtuman
 */
function siirraJoukkueLomakkeeseen(event) {
    alustaLomake();
    for (let joukkue of tulokset)
        if (joukkue.nimi === event.originalTarget.text) 
            muokattavaJoukkue = joukkue;
    
    document.getElementById("kentta"+ 0).value = muokattavaJoukkue.nimi;
    taytettyjaKenttia[0] = 1;
    let jasenia = muokattavaJoukkue.jasenet.length;
    for (let i = 0; i < jasenia; i++) {
        if (i > 1)
            lisaaSyottokentta();
        document.getElementById("kentta"+(i+1)).value = muokattavaJoukkue.jasenet[i];
        taytettyjaKenttia[(i+1)] = 1;
    }
    if (jasenia > 1) {
        lisaaSyottokentta();
        document.getElementById("lisaaJoukkue").disabled = false;
    }
    tyhjaaLeimat();
    keraaleimat();
}

/**
 * Tarkastaa onko rastilomakkeen kaikissa tekstikentissä arvo.
 * Jos kaikissa kentissä on arvo, lisätään rasti tietorakenteeseen.
 * Ei tarkistusta, koska tehtävässä ei pyydetty.
 * @param {*} event painike, joka aiheutti tapahtuman
 */
function lisaaRastiDataan(event) {  
    event.preventDefault();
    let elementti = event.target.parentNode.parentNode,
        sisalto = elementti.getElementsByTagName("input"),
        taulukko = [];
    for (let i = 0; i < sisalto.length; i++) {
        if (sisalto[i].value === "") {
            return;
        }
        taulukko.push(sisalto[i].value);
    }
    lisaaRasti(taulukko);
    nollaaRastiLomake(event);
}

/**
 * Kerätään lomakkeen tekstikentistä tiedot ja lisätään joukkue data-tietorakenteen
 * 2h-sarjaan. Jos globaalissa muuttujassa on joukkueen tiedot, käsitellään tapahtuma 
 * joukkueen muokkauksena ja päivitetään tiedot data-tietorakenteeseen.
 * @param {*} event painike, joka aiheutti tapahtuman
 */
function lisaaTaiMuokkaaJoukkue(event) {
    event.preventDefault();
    let lisattavatTiedot = [];
    //kerätään tiedot lomakkeesta
    for (let i = 0; i < taytettyjaKenttia.length; i++) {
        let arvo = document.getElementById("kentta"+ i).value
        if (arvo !== "")
            lisattavatTiedot.push(arvo.trim());  
    }

    if (muokattavaJoukkue.length === 0) { // lisätään uusi joukkue
        if (onkoSamannimistä(lisattavatTiedot[0])) { // ei hyväksytä samannimistä toista joukkuetta
            return;
        }
        let lisattavaJoukkue = {
            "id": seuraavaJoukkueId,
            "jasenet": [],
            "last": null,
            "nimi":lisattavatTiedot[0],
        }
        seuraavaJoukkueId++;
        for (let i = 1; i < lisattavatTiedot.length; i++)
            lisattavaJoukkue.jasenet.push(lisattavatTiedot[i]);
        data.sarjat[1].joukkueet.push(lisattavaJoukkue);

    } else { // muokataan klikattua joukkuetta
        muokattavaJoukkue.nimi = lisattavatTiedot[0];
        //päivitetään tiedot globaaliin joukkuemuuttujaan
        for (let i = 1; i < lisattavatTiedot.length; i++) 
            muokattavaJoukkue.jasenet[i-1] = lisattavatTiedot[i]; 

        for (let i of data.sarjat)
            for (let joukkue of i.joukkueet) 
                if (joukkue.id === muokattavaJoukkue.id)
                    joukkue = muokattavaJoukkue; //lisätään dataan muutokset
        muokattavaJoukkue = [];
    }
    paivitaTaulukko("",true);
}

/**
 * tarkastaa onko omassa tietorakenteessa samannimistä joukkuetta,
 * jos on palautetaan true, muuten palautetaan false.
 * @param {*} joukkueenNimi 
 */
function onkoSamannimistä(joukkueenNimi) {
    for (let i = 0; i < tulokset.length; i++)
        if (tulokset[i].nimi.toUpperCase() === joukkueenNimi.toUpperCase())
            return true;
    return false;
}

/**
 * Tarkistaa onko joukkuelomakkeen pakollisissa tekstikentissä arvoja.
 * Jos kaikissa pakollisissa on arvot, painike vapautetaan käyttöön.
 * @param {*} event tekstikenttä, joka aiheutti tarkistuksen
 */
function tarkista(event) {  
    let sisalto = event.target.value,
        painike = document.getElementById("lisaaJoukkue"),
        id = parseInt(event.target.id.charAt(6)),
        taytetty = 0;

    //lisätään globaaliin muuttujaan 1, jos tekstikenttään on kirjoitettu
    if (sisalto.trim() === "")
        taytettyjaKenttia[id] = 0;
    else 
        taytettyjaKenttia[id] = 1;

    //käydään globaalin muuttujan alkiot läpi ja lisätään alkio muuttujaan taytetty
    for (let i = 0; i < vaadittujaKenttia; i++) 
        taytetty += taytettyjaKenttia[i];
    
    if (taytetty === vaadittujaKenttia)
        painike.disabled = false;
    else
        painike.disabled = true;
    
    //jos lomakkeen viimeisessä paikassa on arvo, lisätään uusi syöttökenttä
    if (taytettyjaKenttia[taytettyjaKenttia.length - 1] === 1)  
        lisaaSyottokentta();
}

/**
 * Lisää joukkueen leimaavat rastit pudotusvalikkoon, lisää kaikki koodit
 * koodi-pudotusvalikkoon, lisää puuttuvat rastit lisää uusi leima pudotuvalikkoon.
 */
function keraaleimat() {
    let valikko = alustaValikko("valikko","Valitse leima");
    for (let i = 0; i < muokattavaJoukkue.rastit.length; i++) {
        let vaihtoehto = document.createElement("option");
        vaihtoehto.appendChild(document.createTextNode(muokattavaJoukkue.rastit[i].aikaleima));
        vaihtoehto.value = i;
        valikko.appendChild(vaihtoehto);
    }

    let valikko2 = alustaValikko("koodiValikko","");
    for (let i = 0; i < data.rastit.length; i++) {
        let vaihtoehto = document.createElement("option");
        vaihtoehto.appendChild(document.createTextNode(data.rastit[i].koodi));
        vaihtoehto.value = data.rastit[i].id;
        valikko2.appendChild(vaihtoehto);
    }

    let puuttuvatrastit = data.rastit.slice();
    for (let joukkueRasti of muokattavaJoukkue.rastit) {
        for (let rasti of puuttuvatrastit) {
            if (parseInt(joukkueRasti.id) === parseInt(rasti.id)) {
                puuttuvatrastit.splice(puuttuvatrastit.indexOf(rasti),1);
            }
        }
    }
    let valikko3 = alustaValikko("uusiLeima","Valitse koodi");
    for (let i = 0; i < puuttuvatrastit.length; i++) {
        let vaihtoehto = document.createElement("option");
        vaihtoehto.appendChild(document.createTextNode(puuttuvatrastit[i].koodi));
        vaihtoehto.value = puuttuvatrastit[i].id;
        valikko3.appendChild(vaihtoehto);
    }

    let viimeinenLeima = muokattavaJoukkue.last
    if (viimeinenLeima === null) {
        document.getElementById("uusiAikaleima").value = "2017-03-18 hh:mm:ss"
    } else {
        document.getElementById("uusiAikaleima").value = muokattavaJoukkue.last;
    }
}

/**
 * täyttää oletusarvon alasvetotaulukkoon.
 * @param {*} valikonId alasvetovalikon id, jota käsitellään
 * @param {*} ekaArvo oletusarvo, joka annetaan ensimmäiselle vaihtoehdolle
 */
function alustaValikko(valikonId,ekaArvo) {
    let valikko = document.getElementById(valikonId),
        teksti = document.createElement("option");
    teksti.appendChild(document.createTextNode(ekaArvo));
    teksti.value = "";
    valikko.appendChild(teksti);
    return valikko;
}

/**
 * Asettaa valitun leiman koodin valituksi toiseen alasvetovalikkoon.
 * @param {*} event alasvetovalikon likkaus aiheuttaa tapahtuman
 */
function keraaTiedot(event) {
    let pudotusvalikko = event.target;
    if (pudotusvalikko.id === "koodiValikko" || pudotusvalikko.id === "uusiLeima")
        return;
    if (pudotusvalikko.value === "") return;
    let indeksi = pudotusvalikko.options[pudotusvalikko.selectedIndex].value,
        koodivalikko = document.getElementById("koodiValikko"),
        rastiId = muokattavaJoukkue.rastit[indeksi].id,
        aika = document.getElementById("aikaleima");
    for (let i = 0; i < data.rastit.length; i++) {
        if (rastiId === data.rastit[i].id) {
            koodivalikko.value = rastiId;
            break;
        }
    }
    aika.value = muokattavaJoukkue.rastit[indeksi].aikaleima;
}

/**
 * Käsittelee joko leiman poiston tai muokkauksen riippuen tapahtuman aiheuttaneesta
 * painikkeesta. Poistaa data-tietorakenteesta valitun leiman ja päivittää tiedot 
 * kaikkialle. Jos joukkueella on ollut leima moneen kertaan leimattuna, niin 
 * ensimmäinen leima häviää datasta, mutta pisteet eivät muutu. 
 * Muokkaa leiman tietoja data-tietorakenteeseen ja päivittää tiedot kaikkialle.
 * @param {*} event painike, joka aiheutti tapahtuman
 */
function kasitteleLeima(event) {
    event.preventDefault();
    if (muokattavaJoukkue.length === 0 ) return;

    if (event.target.id === "poistaLeima") {
        let lista = document.getElementById("valikko");
        if (lista.options[lista.selectedIndex].value === "") return; //mitään ei valittu
        
        let indeksi = lista.options[lista.selectedIndex].value,
            poistettava = muokattavaJoukkue.rastit[indeksi].tupaId;
        data.tupa.splice(poistettava,1);
    } else {
        let leimavalikko = document.getElementById("valikko");
        if (leimavalikko.options[leimavalikko.selectedIndex].value === "") return; //mitään ei valittu
        
        let indeksi = leimavalikko.options[leimavalikko.selectedIndex].value,
            muokattavaId = muokattavaJoukkue.rastit[indeksi].tupaId,
            koodivalikko = document.getElementById("koodiValikko"),
            uusiRastiId = koodivalikko.options[koodivalikko.selectedIndex].value,
            aika = document.getElementById("aikaleima").value;

        data.tupa[muokattavaId].aika = aika;
        data.tupa[muokattavaId].rasti = uusiRastiId;
    }
    paivitaTaulukko("",false);
}

/**
 * Lisää uuden leiman muokattavalle joukkueelle ja viedään leimaus
 * tupa-tietorakenteeseen.
 * @param {*} event lisää leima -painike aiheitti tapahtuman
 */
function lisaaLeima(event) {
    event.preventDefault();
    if (muokattavaJoukkue.length === 0 ) return;

    let joukkueID = muokattavaJoukkue.id,
        valikko = document.getElementById("uusiLeima"),
        rastiID = valikko.options[valikko.selectedIndex].value,
        lisattavaAika = document.getElementById("uusiAikaleima").value,
        objekti = data.tupa[(data.tupa.length-1)],
        lisattava = Object.assign({}, objekti); //kopioidaan tuvan viimeinen leima malliksi
    if (rastiID === "") return;
    lisattava.aika = lisattavaAika;
    lisattava.rasti = rastiID;
    lisattava.joukkue = joukkueID;

    data.tupa.push(lisattava);
    paivitaTaulukko("",false);
}

/**
 * Luodaan lomake tuotujen tietojen mukaan. Palauttaa lomakkeen.
 * @param {*} otsikot1 otsikot ulompaan lomakkeeseen
 * @param {*} otsikot2 otsikot sisempään lomakkeeseen
 * @param {*} painikkeenTiedot painikkeeseen lisättävät tiedot
 * @param {*} kumpiLomake boolean, jonka mukaan valitaan luotava lomakemalli
 */
function luoLomake(otsikot1,otsikot2,painikkeenTiedot,kumpiLomake) {
    let lomake = document.createElement("form");   
    lomake.setAttribute('method',"post");
    lomake.setAttribute('action',"foobar.ei.toimi.example");
    
    let osio,
        osio2;
    if (kumpiLomake) {
        osio = luoOsio(otsikot1,kumpiLomake);
        osio2 = luoOsio(otsikot2,kumpiLomake); 
        osio.appendChild(osio2);
    } else {
        osio = luoOsio(otsikot1,kumpiLomake);
    }
    osio.appendChild(luoPainike(painikkeenTiedot,kumpiLomake));
    lomake.appendChild(osio);
    return lomake;
}

/**
 * Luodaan lomakkeen osio, jossa on otsikko ja tekstikenttä.
 * Palauttaa valmiin osion.
 * @param {*} otsikot otsikot tekstikentille
 * @param {*} kumpiLomake boolean, jonka mukaan lisätään tapahtumakuuntelija
 */
function luoOsio(otsikot,kumpiLomake) {
    let alue = document.createElement("fieldset"),
        otsikko = document.createElement("legend");
    otsikko.appendChild(document.createTextNode(otsikot[0]));
    alue.appendChild(otsikko);
    for (let i = 1; i < otsikot.length; i++) {
        let p = document.createElement("p"),
            nimike = document.createElement("label"),
            syotto = document.createElement("input");
        syotto.type = "text";
        syotto.value = "";
        if (kumpiLomake === true) {
            syotto.id = "kentta" + seuraavaInputIdNro;
            seuraavaInputIdNro++;
            syotto.addEventListener("input", tarkista);
        }
        nimike.appendChild(document.createTextNode(otsikot[i] + " "));
        nimike.appendChild(syotto);
        p.appendChild(nimike);
        alue.appendChild(p);
    }
    return alue;
}

/**
 * Luo painikkeen ja lisää tapahtuman käsittelijän
 * @param {*} painikkeenTiedot painikkeeseen lisättävät tiedot
 * @param {*} kumpiLomake boolean, jonka mukaan lisätään klikkikäsittelijä
 */
function luoPainike(painikkeenTiedot,kumpiLomake) {
    let p = document.createElement("p"),
        painike = document.createElement("button");
    painike.name = painikkeenTiedot[0];
    painike.id = painikkeenTiedot[1];
    if (kumpiLomake) {
        painike.addEventListener("click", lisaaTaiMuokkaaJoukkue);
        painike.disabled = true;
    } else {
        painike.addEventListener("click", lisaaRastiDataan);
    }
    painike.appendChild(document.createTextNode(painikkeenTiedot[2]));
    p.appendChild(painike);
    return p;
}

/**
 * Lisätään joukkuelomakkeeseen syöttökenttä
 */
function lisaaSyottokentta() {
    let alue = document.getElementsByTagName("fieldset"),
        p = document.createElement("p"),
        nimike = document.createElement("label"),
        syotto = document.createElement("input");
    
    syotto.type = "text";
    syotto.value = "";
    syotto.id = "kentta" + seuraavaInputIdNro;
    syotto.addEventListener("input", tarkista);
    
    nimike.appendChild(document.createTextNode("Jäsen " + seuraavaInputIdNro + " "));
    nimike.appendChild(syotto);
    p.appendChild(nimike);
    alue[2].appendChild(p);

    taytettyjaKenttia.push(0);
    seuraavaInputIdNro++;
}

/**
 * Luo pohjan fieldset rakenteelle
 * @param {*} fieldsetOtsikko legendin otsikko
 */
function luoFieldset(fieldsetOtsikko) {
    let alue = document.createElement("fieldset"),
        otsikko = document.createElement("legend");
    otsikko.appendChild(document.createTextNode(fieldsetOtsikko));
    alue.appendChild(otsikko);
    return alue;
}

/**
 * Luo pohjan pudotusvalikolle
 * @param {*} id pudotusvalikon id
 * @param {*} tiedot pudotusvalikon edessa oleva teksti
 */
function luoSelect(id,tiedot) {
    let p = document.createElement("p"),
        nimike = document.createElement("label"),
        valikko = document.createElement("select");
    
    nimike.appendChild(document.createTextNode(tiedot)); 
    valikko.id = id;
    valikko.addEventListener("change", keraaTiedot);
    
    p.appendChild(nimike);
    p.appendChild(valikko);
    return p;
}

/**
 * Luo tekstikentän, jossa label ja input.
 * @param {*} label kirjoitettava teksti
 * @param {*} id syöttökentän id
 */
function luoTekstikentta(label, id) {
    let p = document.createElement("p"),
        nimike = document.createElement("label"),
        syotto = document.createElement("input");
    syotto.type = "text";
    syotto.value = "";
    syotto.id = id;

    nimike.appendChild(document.createTextNode(label));
    nimike.appendChild(syotto);
    p.appendChild(nimike);
    return p;
}

/**
 * luo joukkueen leimat muokkaukseen painikkeet valitulla
 * eventlistenerillä.
 * @param {*} id painikkeen id
 * @param {*} tiedot painikkeen teksti
 * @param {*} kumpiEvent boolean, siitä kumpi eventlistener
 */
function luoPainike2(id,tiedot,kumpiEvent) {
    let painike = document.createElement("button");

    painike.id = id;
    if (kumpiEvent) {
        painike.addEventListener("click", kasitteleLeima);
    } else {
        painike.addEventListener("click", lisaaLeima);
    }
    painike.appendChild(document.createTextNode(tiedot));
    return painike;
}

/**
 * Luo leimojen muokkaamiseen lomakkeen
 */
function luoleimalomake() {
    let pudotusvalikot = {"valikko":"Leimattu rasti ", "koodiValikko":" Koodi "},
        painikkeet = {"muutaLeima":"Muuta leiman tiedot","poistaLeima":"Poista leimattu rasti"},
        alue = document.getElementsByTagName("fieldset"),
        osaalue = luoFieldset("Joukkueen leimat"),
        p = document.createElement("p"),
        p2 = document.createElement("p");
    
    p.appendChild(document.createTextNode("Leimojen muokkaus ja lisäys on käytössä vain taulukkoon kirjatuilla joukkueilla"));
    osaalue.appendChild(p);

    for (let key in pudotusvalikot)
        osaalue.appendChild(luoSelect(key, pudotusvalikot[key])); 
    osaalue.appendChild(luoTekstikentta("Aika ","aikaleima"));

    for (let key in painikkeet)
        p2.appendChild(luoPainike2(key, painikkeet[key],true));
    osaalue.appendChild(p2);

    let osaalue2 = luoFieldset("Lisaa uusi leima");
    osaalue2.appendChild(luoSelect("uusiLeima","Koodi "));
    osaalue2.appendChild(luoTekstikentta("Aika ","uusiAikaleima"));
    osaalue2.appendChild(luoPainike2("lisaaLeima", "Lisää leima",false));
    
    alue[1].appendChild(osaalue);
    alue[1].appendChild(osaalue2);
}

/**
 * Tyhjentää joukkuelomakkeen pakolliset tekstikentät ja poistaa ylimääräiset 
 * tekstikentät. Samalla alustaa globaalit lomakkeen muuttujat.
 */
function alustaLomake() {
    for (let i = 0; i < taytettyjaKenttia.length; i++) {
        if (i < vaadittujaKenttia) {
            document.getElementById("kentta"+ i).value = "";
        } else {
            let poistettava = document.getElementById("kentta"+ i);
            poistettava.parentNode.remove(poistettava);
        }  
    }
    taytettyjaKenttia = [0,0,0];
    seuraavaInputIdNro = 3;
    document.getElementById("lisaaJoukkue").disabled = true;
}

/**
 * Tyhjää leimalomakkeesta kaikki tiedot joukkueen lisäyksen ja 
 * muokkauksen yhteydessä.
 */
function tyhjaaLeimat() {
    let pudotusvalikko = document.getElementById("valikko");
    for(let i = pudotusvalikko.options.length - 1 ; i >= 0 ; i--) {
        pudotusvalikko.remove(i);
    }
    let koodivalikko2 = document.getElementById("koodiValikko");
    for(let i = koodivalikko2.options.length - 1 ; i >= 0 ; i--) {
        koodivalikko2.remove(i);
    }
    let koodivalikko3 = document.getElementById("uusiLeima");
    for(let i = koodivalikko3.options.length - 1 ; i >= 0 ; i--) {
        koodivalikko3.remove(i);
    }
    document.getElementById("aikaleima").value = "";
    document.getElementById("uusiAikaleima").value = "2017-03-18 hh:mm:ss";
}

/**
 * päivittää leimat pudotusvalikoihin, kun on muokattu tai lisätty leimoja.
 */
function paivitaLeimat() {
    let pudotusvalikko = document.getElementById("valikko"),
        joukkueId = muokattavaJoukkue.id;

    for (let joukkue of tulokset)
        if (joukkue.id === joukkueId) 
            muokattavaJoukkue = joukkue;

    for(let i = pudotusvalikko.options.length - 1 ; i >= 0 ; i--)
        pudotusvalikko.remove(i);

    if (muokattavaJoukkue.length === 0) //pudotusvalikkoon ei voida laittaa tietoja
        return;
      
    let teksti = document.createElement("option");
    teksti.appendChild(document.createTextNode("Valitse leima"));
    teksti.value = "";
    pudotusvalikko.appendChild(teksti);
    for (let i = 0; i < muokattavaJoukkue.rastit.length; i++) {
        let vaihtoehto = document.createElement("option");
        vaihtoehto.appendChild(document.createTextNode(muokattavaJoukkue.rastit[i].aikaleima));
        vaihtoehto.value = i;
        pudotusvalikko.appendChild(vaihtoehto);
    }
    paivitaPuuttuvatLeimat();
    document.getElementById("koodiValikko").value = "";
    document.getElementById("aikaleima").value = "";
    
    let viimeinenLeima = muokattavaJoukkue.last
    if (viimeinenLeima === null) {
        document.getElementById("uusiAikaleima").value = "2017-03-18 hh:mm:ss"
    } else {
        document.getElementById("uusiAikaleima").value = muokattavaJoukkue.last;
    }
}


/**
 * Tyhjentää ja lisää pudotusvalikkoon joukkueen rastit,
 * joita se ei ole leimannut
 */
function paivitaPuuttuvatLeimat() {
    let pudotusvalikko = document.getElementById("uusiLeima"),
        puuttuvatrastit = data.rastit.slice();
    
    for (let i = pudotusvalikko.options.length - 1 ; i >= 0 ; i--)
        pudotusvalikko.remove(i); 

    for (let joukkueRasti of muokattavaJoukkue.rastit) {
        for (let rasti of puuttuvatrastit) {
            if (parseInt(joukkueRasti.id) === parseInt(rasti.id)) {
                puuttuvatrastit.splice(puuttuvatrastit.indexOf(rasti),1);
            }
        }
    }

    let teksti2 = document.createElement("option");
    teksti2.appendChild(document.createTextNode("Valitse koodi"));
    teksti2.value = "";
    pudotusvalikko.appendChild(teksti2);
    for (let i = 0; i < puuttuvatrastit.length; i++) {
        let vaihtoehto = document.createElement("option");
        vaihtoehto.appendChild(document.createTextNode(puuttuvatrastit[i].koodi));
        vaihtoehto.value = puuttuvatrastit[i].id;
        pudotusvalikko.appendChild(vaihtoehto);
    }
    document.getElementById("uusiLeima").value = "";
    document.getElementById("uusiAikaleima").value = "";
    document.getElementById("uusiAikaleima").value = muokattavaJoukkue.last;
}

/**
 * Tyhjää kaikki rastilomakkeen kentät
 * @param {*} event 
 */
function nollaaRastiLomake(event) {
    let elementti = event.target.parentNode.parentNode,
        sisalto = elementti.getElementsByTagName("input");
    for (let i = 0; i < sisalto.length; i++) {
        sisalto[i].value = "";
    }
}

/**
 * Lisää uuden rastin data-tietorakenteeseen ja tulostaa kaikkien
 * rastien tiedot konsoliin
 * @param {*} rastinTiedot lisättävä rasti
 */
function lisaaRasti(rastinTiedot) {
    let viimeinenId = data.rastit[data.rastit.length-1];  //Onko oikea kohta id-numeron perusteella?
    let rasti = {
        "id": viimeinenId.id + 100000000333,
        "kilpailu": 5372934059196416,
        "koodi": "" + rastinTiedot[2],
        "lat": "" + rastinTiedot[0],
        "lon": "" + rastinTiedot[1]
    }
    data.rastit.push(rasti);
    for (let i of data.rastit) {
        console.log("koodi: " + i.koodi + ", lat: " + i.lat + ", lon: " + i.lon);
    }
}

/**
 * Ottaa data-tietorakenteesta joukkueiden tiedot ja lisää 
 * ne omaan tietorakenteeseen (globaati muuttuja).
 */
function luoOmaTietorakenne() {
    tulokset = []; //tyhjätään oma tietorakenne
    for (let i of data.sarjat) {
        for (let joukkue of i.joukkueet) {
            let lisattavaJoukkue = {
                "sarja": i.nimi,
                ...joukkue,
                "aloitus": null,
                "aika": 0,
                "matka": 0,
                "pisteet": 0,
                "viimeisinRasti": null,
                "rastit":[]
            }
            tulokset.push(lisattavaJoukkue);
        }
    }
}

/**
 * järjestää oman tietorakenteen sarjan mukaan. Sarjan ollessa
 * sama järjestetään pisteiden mukaan ja viimeisenä joukkueen
 * nimen mukaan.
 */
function oletusjarjestys() {
    tulokset.sort(function(a, b) {
        if (parseInt(a.sarja) === parseInt(b.sarja)) {
            if (a.pisteet !== b.pisteet) { 
                return b.pisteet - a.pisteet;
            }
            let nimiA = a.nimi.toUpperCase(),           
                nimiB = b.nimi.toUpperCase();
            if (nimiA < nimiB) {
                return -1;
            }
            if (nimiA > nimiB) {
                return 1;
            }
            return 0;
        } else {
            return parseInt(a.sarja) - parseInt(b.sarja);
        }
      });
}

/**
 * Järjestää oman tietorakenteen aakkosjärjestykseen joukkueiden nimen mukaan.
 */
function aakkosjarjestys() {
    tulokset.sort(function(a, b) {
        let nimiA = a.nimi.toUpperCase(),           
            nimiB = b.nimi.toUpperCase();
        if (nimiA < nimiB) 
            return -1;
        if (nimiA > nimiB)
            return 1;
        return 0;
    });
}

/**
 * jäsjestää oman tietorakenteen joukkueiden pisteiden mukaan.
 * Suurimman pisteet ensin ja pienimmät pisteet viimeisenä.
 */
function pistejarjestys() {
    tulokset.sort(function(a, b) {
        return parseInt(b.pisteet) - parseInt(a.pisteet) ;
    });
}

/**
 * jäsjestää oman tietorakenteen joukkueiden kuljetun matkan mukaan.
 * Pisin matka ensin ja lyhin matka viimeisenä.
 */
function matkajarjestys() {
    tulokset.sort(function(a, b) {
        return parseInt(b.matka) - parseInt(a.matka) ;
    });
}

/**
 * jäsjestää oman tietorakenteen joukkueiden käytetyn ajan mukaan.
 * Pisin aika ensimmäisenä ja lyhin aika viimeisenä.
 */
function aikajarjestys() {
    tulokset.sort(function(a, b) {
        return parseInt(b.aika) - parseInt(a.aika) ;
    });
}

/**
 * muuttaa omasta tietorakenteesta joukkueen ajan muotoon
 * hh:mm:ss ja matka pyöristetään kokonaisluvuksi.
 */
function pyoristaTulokset() {
    for (let i = 0; i < tulokset.length; i++) {
        let kokonaisaika = msToTime(tulokset[i].aika);
        tulokset[i].aika = kokonaisaika;
        tulokset[i].matka = tulokset[i].matka.toFixed(0);
    }
}

/**
 * etsii rastit-tietorakenteesta rastin, jolla on etsittävä id-numero.
 * palauttaa löydetyn rastin tiedot tai null.
 * @param {*} rastiId tupa-tietorakenteeseen kirjattu rastin id-numero
 */
function etsiRasti(rastiId) {
    for (let rasti of data.rastit) {
        if (rasti.id === parseInt(rastiId)) {
            return rasti;
        }
    }
    return null;
}

/**
 * etsii sarjat-tietorakenteesta joukkueen, jolla on etsittävä id-numero.
 * palauttaa löydetyn joukkueen tiedot tai null.
 * @param {*} joukkueId tupa-tietorakenteeseen kirjattu joukkueen id-numero
 */
function etsiJoukkue(joukkueId) {
    for (let i of data.sarjat) {
        for (let joukkue of i.joukkueet) {
            if (joukkue.id === joukkueId) {
                return joukkue;
            }
        }
    }
    return null;
}

/**
 * Käy tupa-tietorakenteen läpi ja etsii onko leimatut rastit 
 * ja joukkueet olemassa, jotta kilpailun tiedot voidaan kirjata.
 */
function laskeTulokset() {
    for (let i = 0; i < data.tupa.length; i++) {
        let rasti = etsiRasti(data.tupa[i].rasti),
            joukkue = etsiJoukkue(data.tupa[i].joukkue),
            aikaleima = data.tupa[i].aika;
        
        if ( rasti !== null && joukkue !== null) {
            rasti = {
                ...rasti, //kopioidaan rastin tiedot
                "tupaId": i, 
                "aikaleima": aikaleima
            }
            lisaaPisteet(rasti,joukkue,aikaleima); //rasti ja joukkue on olemassa
        }
    }
    jarjestaRastiajat();
    kirjaaAika();
}

/**
 * Järjestää kunkin joukkueen rastit aikajärjestykseen.
 */
function jarjestaRastiajat() {
    for (let i = 0; i < tulokset.length; i++) {
        tulokset[0].rastit.sort(function(a, b) {
            if (a !== null && b !== null) {
            let aa = new Date(a.aikaleima),
                bb = new Date(b.aikaleima);
            
                return parseInt(bb.getTime()) - parseInt(aa.getTime()) ;
            }
        }); 
    }
}

/**
 * etsii joukkueen omasta tulokset-tietorakenteesta ja varmistaa, että
 * joukkeella ei ole ennestään lisättävää rastia. Jos rastia ei
 * ole, kirjataan matka ja pisteet, sekä lisätään rastin id joukkueelle.
 * @param {*} rasti leimatun rastin tiedot
 * @param {*} joukkue leimanneen joukkueen tiedot
 * @param {*} aikaleima leimausaika
 */
function lisaaPisteet(rasti,joukkue,aikaleima) {
    for (let joukkuetiedot of tulokset) {
        if (joukkuetiedot.id === joukkue.id) {
            for (let joukkuerasti of joukkuetiedot.rastit) {
                if (parseInt(joukkuerasti.id) === parseInt(rasti.id)) {
                    return;
                }   
            }
            kirjaaMatka(rasti,joukkuetiedot,aikaleima);
            joukkuetiedot.pisteet += otaPisteet(rasti);
            joukkuetiedot.rastit.push(rasti);
            return;
        }    
    }
}

/**
 * kirjaa joukkueen kulkeman matkan omaan tietorakenteeseen. Tarkastaa onko 
 * joukkueella yhtään rastia ennestään, jos ei ole, niin matkaa ei 
 * leimata. Jos rastin lon ja lat tietoja ei ole, rastia ei lasketa mukaan.
 * @param {*} rasti leimatun rastin tiedot
 * @param {*} joukkue leimanneen joukkueen tiedot
 * @param {*} aikaleima leimausaika
 */
function kirjaaMatka(rasti,joukkue,aikaleima) {
    if (rasti.lon === null || rasti.lat === null) {
        return;
    }
    if (joukkue.viimeisinRasti === null) {
        joukkue.viimeisinRasti = rasti;
        joukkue.aloitus = aikaleima;
        return;
    }
    let lon1 = joukkue.viimeisinRasti.lon,
        lat1 = joukkue.viimeisinRasti.lat,
        lon2 = rasti.lon,
        lat2 = rasti.lat,
        matka = getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2);
    joukkue.matka += matka;
    joukkue.viimeisinRasti = rasti;
}

function kumpiPienempi(aikaleima,joukkue) {
    let viimeisinAika = new Date(aikaleima),
        aloitusAika = new Date(joukkue.aloitus),
        total = viimeisinAika.getTime() - aloitusAika.getTime();
    if (total < 0) {
        joukkue.aloitusAika=aikaleima;
    }
}

/**
 * kirjaa joukkueen käyttämän ajan millisekunteina.
 * viimeisimmästä ajasta vähennetään aloitusaika.
 */
function kirjaaAika() {
    for (let i = 0; i < tulokset.length; i++) {
        if (tulokset[i].rastit.length !== 0) {
            let lopetus = new Date(tulokset[i].rastit[(tulokset[i].rastit.length-1)].aikaleima),
                aloitus = new Date(tulokset[i].rastit[0].aikaleima),
                total = lopetus.getTime() - aloitus.getTime();
            if (total < 0) {
            tulokset[i].aika = aloitus.getTime() - lopetus.getTime();
            } else {
            tulokset[i].aika = total;
            }
        }  
    }
}

/**
 * tarkastaa onko koodin ensimmäinen merkki numero. Jos merkki on
 * numero, palauttaa kyseisen merkin, muuten palauttaa nollan.
 * @param {*} rasti rastin tiedot
 */
function otaPisteet(rasti) {
    let isNum = /[0-9]+$/.test(rasti.koodi.charAt(0));
    if (isNum) {
        return parseInt(rasti.koodi.charAt(0));
    }
    return 0;
}

/**
 * laskee pisteiden välisen etäisyyden kilometreinä
 * @param {*} lat1 vanhemman rastin lat
 * @param {*} lon1 vanhemman rastin lon
 * @param {*} lat2 uusimman rastin lat
 * @param {*} lon2 uusimman rastin lon
 */
function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
}

// ???
function deg2rad(deg) {
    return deg * (Math.PI/180)
}

/**
 * muuntaa millisekunnit muotoon hh:mm:ss.
 * Palauttaa stringin ajasta.
 * @param {*} aika 
 */
function msToTime(aika) {
    if (isNaN(aika)) return "00:00:00";

    var sekunnit = parseInt((aika/1000)%60), 
        minuutit = parseInt((aika/(1000*60))%60), 
        tunnit = parseInt((aika/(1000*60*60))%24);

    tunnit = (tunnit < 10) ? "0" + tunnit : tunnit;
    minuutit = (minuutit < 10) ? "0" + minuutit : minuutit;
    sekunnit = (sekunnit < 10) ? "0" + sekunnit : sekunnit;

    return tunnit + ":" + minuutit + ":" + sekunnit;
}