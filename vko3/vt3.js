// data-muuttuja sisältää kaiken tarvittavan ja on rakenteeltaan lähes samankaltainen kuin viikkotehtävässä 2
// Rastileimaukset on siirretty tupa-rakenteesta suoraan jokaisen joukkueen yhteyteen
//
// voit tutkia tarkemmin käsiteltävää tietorakennetta konsolin kautta
// tai json-editorin kautta osoitteessa http://jsoneditoronline.org/
// Jos käytät json-editoria niin avaa data osoitteesta:
// http://appro.mit.jyu.fi/tiea2120/vt/vt3/data.json

("use strict");

console.log(data);

var joukkueet, sarjat;
var editoitavaJoukkue = null;

/**
 * Alustus
 */
window.onload = function() {
	joukkueet = haeJoukkueetDatasta();
	sarjat = haeSarjat(data);
	renderSarjat(sarjat);
	renderJasenet(5);
	renderJoukkueet(joukkueet);

	renderLeimausTavat();

	// Kuuntelijat
	asetaNimiJasenTarkistukset();
	asetaTallennaKuuntelija();
	asetaLoputKuuntelijat();
};

/**
 * Renderöi joukkueet uudelleen ja tyhjentää kentät
 *
 */
function renderoiJaTyhjenna() {
	document.getElementsByTagName("form")[0].reset();
	renderJoukkueet(joukkueet);
	const sarja = document.getElementsByClassName("sarjat")[0];
	sarja.checked = true;
}

/**
 * Asettaa tarkistuksen formiin, että kahta samannimistä joukkuetta ei voi olla.
 * @param {*} nykyJoukkueet kaikki nykyiset joukkueet
 */
function asetaNimiJasenTarkistukset(nykyJoukkueet = joukkueet) {
	let elem = document.getElementById("joukkueenNimiInput");
	let jasenet = document.getElementsByClassName("jasenKentta")[0];
	// Eka poistetaan mahdolliset kuuntelijat
	elem.removeEventListener("input", kuuntelija);
	jasenet.removeEventListener("input", jasenKuuntelija);

	elem.addEventListener("input", kuuntelija);
	jasenet.addEventListener("input", jasenKuuntelija);
}

/**
 * Asettaa kuuntelijat mitä ei vielä asetettu
 *
 */
function asetaLoputKuuntelijat() {
	let leimaustavat = document.getElementsByClassName("leimaustavat")[0];
	let sarjanNimi = document.getElementById("sarjanNimiInput");
	let alkuAika = document.getElementById("alkuAika");
	let loppuaika = document.getElementById("loppuAika");

	leimaustavat.addEventListener("input", leimaustapaKuuntelija);
	sarjanNimi.addEventListener("input", sarjanNimiKuuntelija);
	alkuAika.addEventListener("input", sarjanAikaKuuntelija);
	loppuaika.addEventListener("input", sarjanAikaKuuntelija);
}

/**
 * Sarjan nimi kuuntelija
 *
 */
function sarjanNimiKuuntelija() {
	let sarjanNimi = document.getElementById("sarjanNimiInput");
	const nimet = sarjat.map(sarja => sarja.nimi);
	if (nimet.includes(sarjanNimi.value))
		sarjanNimi.setCustomValidity("Samanniminen sarja on jo olemassa");
	else if (sarjanNimi.value.length < 2)
		sarjanNimi.setCustomValidity("Liian lyhyt sarjan nimi");
	else sarjanNimi.setCustomValidity("");
}

/**
 * Leimauskuuntelija
 *
 */
function leimaustapaKuuntelija() {
	const leimaustavat = document.getElementsByClassName("leimaustavat");
	let yksitapa = false;
	for (let i = 0; i < leimaustavat.length; i++) {
		if (leimaustavat[i].checked) yksitapa = true;
	}
	if (!yksitapa)
		leimaustavat[0].setCustomValidity("Valitse vähintään yksi leimaustapa");
	else leimaustavat[0].setCustomValidity("");
}

/**
 * Nimikuuntelija
 *
 * @param {*} event
 */
function kuuntelija(event) {
	const nimet = joukkueet.map(joukkue => joukkue.nimi.trim());
	// Haetaan nimi input
	let elem = document.getElementById("joukkueenNimiInput");
	const { value } = elem;
	const nimi = value.trim();
	if (nimet.includes(nimi)) {
		elem.setCustomValidity("Joukkue annetulla nimellä on jo olemassa");
		elem.reportValidity();
	} else if (value.length < 2) {
		elem.setCustomValidity("Liian lyhyt");
		elem.reportValidity();
	} else elem.setCustomValidity("");
}

/**
 * Callbacki jäsenille input eventtiä varten
 *
 * @param {*} event
 */
function jasenKuuntelija(event) {
	let elem = document.getElementsByClassName("jasenKentta");
	let eka = elem[0];
	const jasenet = haeJasenet();
	if (jasenet.length <= 2) {
		eka.setCustomValidity(
			"Liian vähän lisättäviä jäseniä tai jäsenet ei uniikkeja"
		);
	} else {
		eka.setCustomValidity("");
	}
}

/**
 * Callbacki kuuntelija uusille jäsenille
 *
 */
function uusiJasenKuuntelija() {
	let elem = document.getElementsByClassName("jasenKentta");
	// Lisää uusia kenttiä tarvittaessa
	let taydet = 0;
	for (let i = 0; i < elem.length; i++) {
		if (elem[i].value.trim().length > 0) taydet++;
	}
	if (taydet >= elem.length) lisaaJasenInput();
}

/**
 * Lisää jäsen inputin
 *
 */
function lisaaJasenInput() {
	let jasenElementti = document.getElementsByClassName("jasenKentta");
	let jasenContainer = document.getElementById("jasenet");
	// poista kuuntelija eka viimeiseltä
	jasenElementti[jasenElementti.length - 1].removeEventListener(
		"input",
		uusiJasenKuuntelija
	);

	console.log(`taydet kuulutettu`);
	const label = document.createElement("LABEL");
	const jasenNumero = jasenElementti.length + 1;
	label.textContent = "Jäsen " + jasenNumero.toString();
	const input = document.createElement("INPUT");
	input.type = "text";
	input.size = "40";
	input.className = "jasenKentta";
	label.appendChild(input);
	// Lisätään uusi kuuntelija
	input.addEventListener("input", uusiJasenKuuntelija);
	jasenContainer.appendChild(label);
}

function asetaTallennaKuuntelija() {
	let tallenna = document.getElementById("tallenna");
	let tallennaSarjaElem = document.getElementById("tallennaSarja");

	tallenna.addEventListener("click", tallennaJoukkue);
	tallennaSarjaElem.addEventListener("click", tallennaSarja);
}

/**
 * Hakee lisätyt jäsenet.
 * Ei voi olla kahta samannimistä jäsentä
 * @return jasenet -taulukon
 */
function haeJasenet() {
	let jasenet = [];
	let jasenetKentat = document.getElementsByClassName("jasenKentta");
	for (let i = 0; i < jasenetKentat.length; i++) {
		const jasen = jasenetKentat[i].value.trim();
		if (!jasenet.includes(jasen) && jasen.length > 0) jasenet.push(jasen);
	}
	return jasenet;
}

/**
 * Sarjan aika kuuntelija
 *
 */
function sarjanAikaKuuntelija() {
	let alkuaika = document.getElementById("alkuAika");
	let loppuaika = document.getElementById("loppuAika");
	if (alkuaika.value.length > 0 && loppuaika.value.length > 0) {
		const kisanAlku = Date.parse(data.kisat[0].alkuaika);
		const kisanLoppu = Date.parse(data.kisat[0].loppuaika);
		const alkuAika = Date.parse(alkuaika.value);
		const loppuAika = Date.parse(loppuaika.value);
		const kesto = document.getElementById("sarjanKestoInput").value;
		const kestoMS = 3600000 * parseInt(kesto);
		if (alkuAika < kisanAlku)
			alkuaika.setCustomValidity(
				"Alkuaika ei saa olla pienempi kuin kilpailun alkuaika"
			);
		else if (loppuAika > kisanLoppu)
			alkuaika.setCustomValidity(
				"Loppuaika ei saa olla suurempi kuin kilpailun loppuaika"
			);
		else if (loppuAika < alkuAika + kestoMS)
			alkuaika.setCustomValidity(
				"Loppuajan on oltava suurempi kuin sarjan alkuaika ja kesto yhteensä"
			);
	} else alkuaika.setCustomValidity("");
}

/**
 * Tallennusfunktio uudelle sarjalle
 *
 * @param {*} event
 */
function tallennaSarja(event) {
	event.preventDefault();

	let nimi = document.getElementById("sarjanNimiInput");
	let kesto = document.getElementById("sarjanKestoInput");
	let alkuaika = document.getElementById("alkuAika");
	let loppuaika = document.getElementById("loppuAika");

	if (
		alkuaika.checkValidity() &&
		nimi.checkValidity() &&
		kesto.checkValidity()
	) {
		let sarja = {
			nimi: nimi.value,
			kesto: parseInt(kesto.value),
			id: generoiRandom(),
			alkuaika: null,
			loppuaika: null
		};
		const alkuaikaArvo = alkuaika.value;
		const loppuaikaArvo = loppuaika.value;

		if (alkuaikaArvo.length > 2 && loppuaikaArvo.length > 2) {
			sarja.alkuaika = parseInt(alkuaikaArvo);
			sarja.loppuaika = parseInt(loppuaikaArvo);
		}

		sarjat.push(sarja);
		sarjat = sarjat.sort((a, b) => {
			if (a.nimi < b.nimi) return -1;
			else if (a.nimi > b.nimi) return 1;
			return 0;
		});
		renderSarjat(sarjat);

		// Näytetään tallennus onnistui viesti
		let tallennusDiv = document.getElementById("tallennaSarjaDiv");
		let ok = document.createElement("span");
		ok.className = "onnistumisViesti";
		ok.textContent = "Tallennus onnistui";
		tallennusDiv.appendChild(ok);
		setTimeout(() => {
			tallennusDiv.removeChild(ok);
		}, 4000);
		// lopulta resetoidaan sarjan form
		document.getElementById("sarjanLisaysForm").reset();
	} else {
		alkuaika.reportValidity();
	}
}

/**
 * Tallentaa uuden tai olemassaolevan joukkueen
 *
 * @param {*} event
 */
function tallennaJoukkue(event) {
	event.preventDefault();

	// Validoidaan syötetty data
	let nimi = document.getElementById("joukkueenNimiInput");
	const nimiOk = nimi.checkValidity();

	let aika = document.getElementById("luontiaika");
	const aikaOk = aika.checkValidity();

	// tarkasta, että väh. 1 checkbox valittuna
	const leimaustavat = document.getElementsByClassName("leimaustavat");
	let tavat = [];
	for (let i = 0; i < leimaustavat.length; i++) {
		if (leimaustavat[i].checked) tavat.push(leimaustavat[i].value);
	}

	const leimausValittu = tavat.length > 0;

	const jasenet = haeJasenet();
	const jasenetOk = jasenet.length >= 2;

	const sarjatElem = document.getElementsByClassName("sarjat");
	let valittuSarja = null;
	for (let i = 0; i < sarjatElem.length; i++) {
		if (sarjatElem[i].checked) valittuSarja = sarjatElem[i].id;
	}

	if (
		nimiOk &&
		aikaOk &&
		leimausValittu &&
		jasenetOk &&
		valittuSarja !== null
	) {
		if (editoitavaJoukkue !== null) {
			const indeksi = joukkueet.indexOf(editoitavaJoukkue);
			joukkueet[indeksi] = {
				...editoitavaJoukkue,
				nimi: nimi.value,
				leimaustavat: tavat,
				luontiaika: parseDateTime(aika.value),
				jasenet: jasenet.sort()
			};
			editoitavaJoukkue = null;
		} else {
			let id = generoiRandom();
			const idt = joukkueet.map(jk => jk.id);
			while (idt.includes(id)) {
				id = generoiRandom();
			}
			const joukkue = {
				jasenet: jasenet.sort(),
				id,
				nimi: nimi.value,
				leimaustavat: tavat,
				luontiaika: parseDateTime(aika.value)
			};
			joukkueet.push(joukkue);
			// Järkätään uudelleen aakkosjärjestykseen
			joukkueet = joukkueet.sort(joukkueJarjestys);
		}

		// Näytetään tallennus onnistui viesti
		let tallennusDiv = document.getElementById("tallennusDiv");
		let ok = document.createElement("span");
		ok.className = "onnistumisViesti";
		ok.textContent = "Tallennus onnistui";
		tallennusDiv.appendChild(ok);
		setTimeout(() => {
			tallennusDiv.removeChild(ok);
		}, 4000);

		renderoiJaTyhjenna();
	} else {
		if (!jasenetOk)
			document.getElementsByClassName("jasenKentta")[0].reportValidity();
		if (!nimiOk) nimi.reportValidity();
		if (!leimausValittu) leimaustavat[0].reportValidity();
		if (!aikaOk) aika.reportValidity();
	}
}

// Vertailufunktio joukkueille
function joukkueJarjestys(a, b) {
	const aNimi = a.nimi.toLowerCase();
	const bNimi = b.nimi.toLowerCase();
	if (aNimi < bNimi) return -1;
	else if (aNimi > bNimi) return 1;
	return 0;
}

/**
 * Lisää leimaustavat lomakkeelle data-rakenteesta
 */
function renderLeimausTavat() {
	// Aluksi poistetaan kaikki edelliset mahdolliset leimaustavat
	let elem = document.getElementById("leimaustavat");
	for (let i = 0; i < elem.childNodes.length; i++) {
		elem.removeChild(elem.lastChild);
	}

	const leimaustavat = haeLeimausTavat();
	let j = 0;
	leimaustavat.forEach(tapa => {
		let leimaustapaLabel = document.createElement("label");
		leimaustapaLabel.textContent = `${tapa} `;

		let leimaustapacheckbox = document.createElement("input");
		leimaustapacheckbox.type = "checkbox";
		leimaustapacheckbox.name = "leimaustapa";
		leimaustapacheckbox.id = `${j}-leimaustapa`;
		leimaustapacheckbox.value = tapa;
		leimaustapacheckbox.className = "leimaustavat";

		leimaustapaLabel.appendChild(leimaustapacheckbox);
		elem.appendChild(leimaustapaLabel);

		let br = document.createElement("br");
		elem.appendChild(br);
	});

	// laitetaan aluksi leimaamattomana falseksi
	document
		.getElementsByClassName("leimaustavat")[0]
		.setCustomValidity("Valitse vähintään yksi leimaustapa");
}

/**
 * Piirtää sarjat DOM:n
 * @param {Array<String>} sarjat annetut sarjat
 */
function renderSarjat(sarjatParam) {
	// Aluksi poistetaan kaikki edelliset mahdolliset leimaustavat
	let sarjaElementti = document.getElementById("sarja");
	while (sarjaElementti.hasChildNodes()) {
		sarjaElementti.removeChild(sarjaElementti.lastChild);
	}

	let i = 0;
	// Luodaan lisättävät vaihtoehdot
	sarjatParam.forEach(sarja => {
		const label = document.createElement("LABEL");
		label.textContent = sarja.nimi;
		label.class = "form_label";
		label.className = "sarjatLabel";
		const input = document.createElement("INPUT");
		input.type = "radio";
		input.name = "sarja";
		input.id = sarja.nimi;
		input.className = "sarjat";
		label.appendChild(input);

		if (i === 0) input.checked = true; // asetetaan ensimmäinen valituksi

		sarjaElementti.appendChild(label);
		let br = document.createElement("br");
		sarjaElementti.appendChild(br);
		i++;
	});
}

/**
 * Renderöi halutun määrän jäsenlisäyskenttiä
 * @param {number} numero haluttu määrä
 */
function renderJasenet(numero) {
	let jasenElementti = document.getElementsByClassName("jasenKentta");
	let jasenContainer = document.getElementById("jasenet");
	for (let i = 0; i < numero; i++) {
		const label = document.createElement("LABEL");
		label.textContent = "Jäsen " + (i + 1).toString();
		const input = document.createElement("INPUT");
		input.type = "text";
		input.size = "40";
		input.className = "jasenKentta";
		label.appendChild(input);
		jasenContainer.appendChild(label);
	}
	document
		.getElementsByClassName("jasenKentta")[0]
		.setCustomValidity("Lisää vähintään kaksi uniikkia jäsentä");

	jasenElementti[jasenElementti.length - 1].addEventListener(
		"input",
		uusiJasenKuuntelija
	);
}

/**
 * Renderöi joukkuet aakkosjärjestyksessä dokumentin loppuun
 * @param {Array<Object>} joukkueTaulukko joukkeiden nimet aakkosjärjestyksessä
 */
function renderJoukkueet(joukkueTaulukko) {
	let joukkueElementti = document.getElementById("loppulistaus");

	// Aluksi tyhjennetään mahdollinen edellinen listaus
	// for (let i = 0; i < joukkueElementti.childNodes.length; i++) {
	// 	joukkueElementti.removeChild(joukkueElementti.lastChild);
	// }
	while (joukkueElementti.hasChildNodes()) {
		joukkueElementti.removeChild(joukkueElementti.lastChild);
	}

	let ul = document.createElement("UL");
	// Lisätään joukkueet yksitellen DOM elementteihin
	joukkueTaulukko.forEach(joukkue => {
		let li = document.createElement("LI");
		li.textContent = joukkue.nimi;
		li.href = "#tiedot";
		li.addEventListener("click", e => {
			e.preventDefault();
			esitaytaJoukkueLomakkeelle(joukkue);
		});
		li.className = "joukkueenNimiLi";

		// Lisätään joukkueen jäsenet
		let uusiUl = document.createElement("UL");
		joukkue.jasenet.forEach(jasen => {
			let jli = document.createElement("LI");
			jli.textContent = jasen;
			uusiUl.appendChild(jli);
		});
		li.appendChild(uusiUl);
		ul.appendChild(li);
	});
	// Lopuksi laitetaan muodostettu elementti DOM-puuhun
	joukkueElementti.appendChild(ul);
}

/* DATA FUNKTIOT */

function haeLeimausTavat() {
	let tavat = [];
	joukkueet.forEach(valittujoukke => {
		const leimaustapa = valittujoukke.leimaustapa;
		leimaustapa.forEach(tapa => {
			if (!tavat.includes(tapa)) tavat.push(tapa);
		});
	});
	return tavat;
}

/**
 * Esitäyttää joukkueen lomakkeelle
 *
 * @param {*} joukkue täytettävä joukkue
 */
function esitaytaJoukkueLomakkeelle(joukkue) {
	editoitavaJoukkue = joukkue;

	let nimiInput = document.getElementById("joukkueenNimiInput");
	let luontiaikaInput = document.getElementById("luontiaika");
	let leimaustavat = document.getElementsByClassName("leimaustavat");
	let sarjaInput = document.getElementsByClassName("sarjat");
	let jasenetInput = document.getElementsByClassName("jasenKentta");

	const aika = new Date(joukkue.luontiaika);

	nimiInput.value = joukkue.nimi;
	const aikaISO = aika.toISOString();
	luontiaikaInput.value = aikaISO.substring(0, aikaISO.length - 8);

	// Täytetään leimaustavat
	const { leimaustapa } = joukkue;
	for (let i = 0; i < leimaustavat.length; i++) {
		if (leimaustapa.includes(leimaustavat[i].value))
			leimaustavat[i].checked = true;
		else leimaustavat[i].checked = false;
	}

	// Sarja
	const { sarja } = joukkue; // id
	const sarjaNimi = sarjat.find(s => s.id == sarja).nimi;
	for (let j = 0; j < sarjaInput.length; j++) {
		if (sarjaInput[j].id == sarjaNimi) {
			sarjaInput[j].checked = true;
			break;
		}
	}

	// Jäsenet
	const { jasenet } = joukkue;
	// tyhjennä
	for (let i = 0; i < jasenetInput.length; i++) jasenetInput[i].value = "";
	while (jasenet.length > jasenetInput.length) {
		lisaaJasenInput();
	}
	for (let j = 0; j < jasenet.length; j++) {
		jasenetInput[j].value = jasenet[j];
	}
}

/**
 * Hakee ja palauttaa sarjalistauksen taulukkona lajiteltuna aakkosjärjestyksessä
 */
function haeSarjat(data) {
	let tempSarjat = [];
	data.kisat.forEach(kisa => {
		kisa.sarjat.forEach(sarja => {
			tempSarjat.push(sarja);
			// Mieti jos ID myös pitää ottaa tässä talteen
		});
	});
	const lajiteltu = tempSarjat.sort((a, b) => {
		if (a.nimi < b.nimi) return -1;
		else if (a.nimi > b.nimi) return 1;
		return 0;
	});

	return lajiteltu;
}

/**
 * Hakee ja palauttaa joukkueiden nimet aakkosjärjestyksessä
 */
function haeJoukkueetDatasta() {
	let kaikkiJoukkueet = [];
	data.joukkueet.forEach(joukkue => {
		kaikkiJoukkueet.push(joukkue);
	});
	// Nimet aakkosjärjestykseen
	kaikkiJoukkueet.forEach(joukkue => {
		joukkue.jasenet = joukkue.jasenet.sort();
	});
	kaikkiJoukkueet.sort((a, b) => {
		if (a.nimi < b.nimi) return -1;
		else if (a.nimi > b.nimi) return 1;
		return 0;
	}); // Järjestää aakkosjärjestykseen automaattisesti
	return kaikkiJoukkueet;
}

// Generoi random ID:n joukkueelle
const generoiRandom = () =>
	Math.floor(Math.random() * 8999999999999999 + 1000000000000000);

/**
 * Lisää nollan numeron eteen, jos yksi numero
 *
 * @returns 1 -> 01
 */
function pad(numero) {
	let s = `${numero}`;
	if (s.length == 1) return `0${numero}`;
	else return s;
}

/**
 * Parsaa ISO-formaatista oikeaan muotoon
 * @param {*} datetime
 * @returns parsatun ajankohdan
 */
function parseDateTime(datetime) {
	let parsattu = datetime.replace("T", " ");
	const vika = parsattu.charAt(parsattu.length - 1);
	if (isNaN(parseInt(vika))) {
		parsattu = parsattu.substring(0, parsattu.length - 1);
	}
	return parsattu;
}
