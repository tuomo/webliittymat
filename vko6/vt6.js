"use strict";

class App extends React.Component {
	constructor(props) {
		super(props);

		// tehdään kopio tietorakenteen joukkueista
		// Tämä on tehtävä näin, että saadaan oikeasti aikaan kopio eikä vain viittausta samaan tietorakenteeseen. Objekteja ja taulukoita ei voida kopioida vain sijoitusoperaattorilla
		// päivitettäessä React-komponentin tilaa on aina vanha tila kopioitava uudeksi tällä tavalla
		// kts. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from
		let joukkueet = Array.from(data.joukkueet, function(j) {
			// luodaan uusijoukkue
			let uusij = {};
			// kopioidaan tavalliset kentät
			let kentat = ["nimi", "sarja", "seura", "id"];
			for (let i of kentat) uusij[i] = j[i];
			// taulukot on kopioitava erikseen. Nyt riittää pelkkä array.from, koska tauluiden
			// sisällä ei ole muita taulukoita tai objekteja
			let uusijasenet = Array.from(j["jasenet"]);
			let uusirastit = Array.from(j["rastit"]);
			let uusileimaustapa = Array.from(j["leimaustapa"]);
			uusij["jasenet"] = uusijasenet;
			uusij["rastit"] = uusirastit;
			uusij["leimaustapa"] = uusileimaustapa;
			return uusij;
		});
		console.log(joukkueet);

		// Kopio sarjoista
		const sarjat = [...data.kisat[0].sarjat].map(sarja => ({
			nimi: sarja.nimi,
			id: sarja.id
		}));
		// ja rasteista
		const rastit = [...data.rastit];
		this.state = {
			joukkueet,
			editoitava: null,
			nimi: "",
			luontiaika: null,
			sarjat,
			rastit,
			jasenet: [],
			leimaustavat: []
		};

		this.haeLeimaustavat = this.haeLeimaustavat.bind(this);
		this.editoiJoukkuetta = this.editoiJoukkuetta.bind(this);
		this.handleTallennaRasti = this.handleTallennaRasti.bind(this);
		this.handleTallenna = this.handleTallenna.bind(this);
	}

	// luo random ID:n joukkueelle
	luoEditoitavaId = joukkueet => {
		let id = Math.floor(
			Math.random() * 8999999999999999 + 1000000000000000
		);
		while (joukkueet.map(j => j.id).includes(id)) {
			id = Math.floor(
				Math.random() * 8999999999999999 + 1000000000000000
			);
		}
		return id;
	};

	editoiJoukkuetta(id) {
		const joukkue = this.state.joukkueet.find(j => j.id == id);
		if (joukkue) {
			this.setState({ editoitava: joukkue });
		} else {
			this.setState({
				editoitava: joukkue,
				id: id,
				nimi: "",
				luontiaika: null,
				jasenet: [],
				leimaustavat: []
			});
		}
	}

	haeLeimaustavat() {
		let tavat = [];
		this.state.joukkueet.forEach(valittujoukke => {
			const leimaustapa = valittujoukke.leimaustapa;
			if (leimaustapa !== null)
				leimaustapa.forEach(tapa => {
					if (tapa !== null && !tavat.includes(tapa))
						tavat.push(tapa);
				});
		});
		return tavat;
	}

	handleTallenna(joukkue) {
		let id = joukkue.id;
		if (joukkue.id == null) {
			id = this.luoEditoitavaId(this.state.joukkueet);
			this.setState({
				joukkueet: [...this.state.joukkueet, { ...joukkue, id }],
				editoitava: null
			});
		} else {
			const i = this.state.joukkueet.findIndex(j => j.id == id);
			let uusi = [...this.state.joukkueet];
			const { nimi, jasenet, luontiaika, leimaustapa, sarja } = joukkue;
			uusi[i] = {
				...uusi[i],
				nimi,
				jasenet,
				luontiaika,
				leimaustapa,
				sarja
			};
			const editoitava = uusi[i];
			this.setState({ joukkueet: uusi, editoitava });
		}
	}

	handleTallennaRasti(rasti) {
		let rastit = [...this.state.rastit];
		const i = rastit.findIndex(r => r.id == rasti.id);
		rastit[i] = rasti;
		this.setState({ rastit });
	}

	render() {
		const { joukkueet, sarjat, rastit, editoitava } = this.state;
		const leimaustavat = this.haeLeimaustavat();
		return (
			<div>
				<h1>Tulospalvelu</h1>
				<div className="flexContainer">
					<ListaaRastit
						rastit={rastit}
						tallennaRasti={this.handleTallennaRasti}
					/>
					<ListaaJoukkueet
						joukkueet={joukkueet}
						sarjat={sarjat}
						editoiJoukkuetta={this.editoiJoukkuetta}
					/>
					<LisaaJoukkue
						sarjat={sarjat}
						leimaustavat={leimaustavat}
						joukkue={editoitava}
						tallenna={this.handleTallenna}
					/>
				</div>
			</div>
		);
	}
}

class ListaaRastit extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			muokattava: null, // muokattava id
			id: -1, // id:n arvo
			koodi: "",
			lat: "",
			lon: ""
		};

		this.editRasti = this.editRasti.bind(this);
		this.startEditRasti = this.startEditRasti.bind(this);
		this.handleKeyPress = this.handleKeyPress.bind(this);
	}

	startEditRasti(rasti) {
		console.log(`rasti`, { ...rasti });
		this.setState({ ...rasti });
	}

	editRasti(event) {
		const { value } = event.target;
		this.setState({ koodi: value });
		// Validiointi
		const eka = parseInt(value.charAt(0));
		if (!eka) {
			event.target.setCustomValidity("Ei validi rasti");
		} else {
			event.target.setCustomValidity("");
		}
	}

	// Otetaan enter talteen
	handleKeyPress(event) {
		console.log(`event key enter`, event.key == "Enter");
		if (event.key == "Enter") {
			if (event.target.checkValidity()) {
				const rasti = {
					id: this.state.id,
					koodi: this.state.koodi,
					lat: this.state.lat,
					lon: this.state.lon
				};
				this.props.tallennaRasti(rasti);
				event.target.blur();
			} else {
				event.target.reportValidity();
			}
		}
	}

	editLat(id) {}

	editLon(id) {}

	render() {
		const kaikkiRastit = this.props.rastit.map(rasti => {
			const editable = rasti.id == this.state.id;
			const koodiElem = editable ? (
				<input
					onChange={this.editRasti}
					value={this.state.koodi}
					onKeyPress={this.handleKeyPress}
					onBlur={() => {
						console.log(`onBlur`);
						this.setState({ muokattava: null });
					}}
				/>
			) : (
				<span
					onClick={() => {
						this.startEditRasti(rasti);
					}}
				>
					{rasti.koodi}
				</span>
			);

			return (
				<li key={rasti.id}>
					{koodiElem}
					<div>
						{rasti.lat}, {rasti.lon}
					</div>
				</li>
			);
		});
		return (
			<div id="rastiListaus">
				<h2>Rastit</h2>
				<ul>{kaikkiRastit}</ul>
			</div>
		);
	}
}

class LisaaJoukkue extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			id: null,
			nimi: "",
			luontiaika: "",
			leimaustapa: [],
			sarjaId: null,
			jasenet: ["", ""]
		};

		this.handleTallenna = this.handleTallenna.bind(this);
		this.handleChangeJasenet = this.handleChangeJasenet.bind(this);
		this.renderJasenKentat = this.renderJasenKentat.bind(this);
		this.handleChangeNimi = this.handleChangeNimi.bind(this);
		this.handleChangeLuontiaika = this.handleChangeLuontiaika.bind(this);
	}

	UNSAFE_componentWillReceiveProps(nextProps, nextState) {
		const { joukkue } = nextProps;
		if (joukkue) {
			const {
				nimi,
				luontiAika,
				leimaustapa,
				sarja,
				jasenet,
				id
			} = joukkue;
			this.setState({
				id,
				nimi,
				luontiAika: luontiAika ? luontiAika : "",
				leimaustapa: leimaustapa ? leimaustapa : [],
				sarjaId: sarja,
				jasenet
			});
		} else {
			this.setState({
				id: null,
				nimi: "",
				luontiaika: "",
				leimaustapa: [],
				sarjaId: this.props.sarjat[0].id,
				jasenet: ["", ""]
			});
		}
	}

	handleChangeNimi(event) {
		if (event.target.value.length > 1) event.target.setCustomValidity("");
		else event.target.setCustomValidity("Tarpeeksi pitkä nimi pakollinen");
		this.setState({ nimi: event.target.value });
	}

	handleChangeLuontiaika(event) {
		this.setState({ luontiaika: event.target.value });
	}

	// joukkueen tallennus
	handleTallenna(event) {
		event.preventDefault();
		const nimiKentta = document.getElementById("joukkueenNimiInput");
		const jasen1 = document.getElementById("0");
		const {
			nimi,
			luontiaika,
			leimaustapa,
			sarjaId,
			jasenet,
			id
		} = this.state;
		const joukkue = {
			id,
			nimi: nimi,
			jasenet: jasenet.filter(j => j.length > 0),
			sarja: sarjaId,
			leimaustapa: leimaustapa.length > 0 ? leimaustapa : null,
			luontiaika: isNaN(Date.parse(luontiaika)) ? null : luontiaika // jos luontiaika validi
		};
		if (nimiKentta.checkValidity() && jasen1.checkValidity())
			this.props.tallenna(joukkue);
		else {
			nimiKentta.reportValidity();
			jasen1.reportValidity();
		}
	}

	handleChangeJasenet(event) {
		const { id, value } = event.target;
		const jasen1 = document.getElementById("0");
		if (this.state.jasenet.filter(j => j.length > 1).length < 2) {
			jasen1.setCustomValidity(
				"Tarvitset vähintään kaksi tarpeeksi pitkännimistä jäsentä"
			);
		} else {
			jasen1.setCustomValidity("");
		}
		const i = parseInt(id);
		const { jasenet } = this.state;
		let kopio = [...jasenet];
		kopio[i] = value;
		// Lisätään uusi kenttä
		const taydet = jasenet.map(j => j.length).filter(i => i > 0).length;
		if (taydet >= jasenet.length && jasenet.length < 5) kopio.push("");
		// Päivitetään tila
		this.setState({ jasenet: kopio });
	}

	renderJasenKentat() {
		const { jasenet } = this.state;
		const kentat = jasenet.map((jasen, i) => (
			<label key={`j${i}`}>
				{`Jäsen ${i + 1}`}
				<input
					key={`jasen-${i}`}
					id={`${i}`}
					type="text"
					size="40"
					className="jasenKentta"
					value={jasen}
					onChange={this.handleChangeJasenet}
					required={i <= 1 ? true : false}
				/>
			</label>
		));
		return kentat;
	}

	// Asetetaan default sarja valituksi
	componentDidMount() {
		this.setState({ sarjaId: this.props.sarjat[0].id });
	}

	render() {
		const {
			sarjat,
			leimaustavat,
			// muutaLeimaustapaa,
			editoitava
		} = this.props;
		const { jasenet, nimi, luontiaika, leimaustapa } = this.state;
		const sarjaListaus = sarjat.map(sarja => (
			<label className="sarjatLabel" key={sarja.nimi}>
				{sarja.nimi}
				<input
					type="radio"
					name="sarja"
					id={sarja.id}
					className="sarjat"
					key={`${sarja.nimi}i`}
					onChange={() => {
						this.setState({ sarjaId: sarja.id });
					}}
					checked={sarja.id == this.state.sarjaId ? true : false}
				/>
			</label>
		));
		const leimaustapaListaus = leimaustavat.map(tapa => (
			<label key={tapa}>
				{tapa}
				<input
					type="checkbox"
					name="leimaustapa"
					id={tapa}
					className="leimaustavat"
					value={tapa}
					key={`${tapa}i`}
					onChange={event => {
						const { value } = event.target;
						let uusi = [...this.state.leimaustapa];
						// Poistetaan arvo
						if (uusi.includes(value))
							uusi = uusi.filter(v => v !== value);
						else uusi.push(value);
						this.setState({ leimaustapa: uusi });
					}}
					checked={this.state.leimaustapa.includes(tapa)}
				/>
			</label>
		));
		const jasenKentat = this.renderJasenKentat();
		return (
			<div className="container">
				<h2>Lisää joukkue</h2>
				<form>
					<fieldset id="tiedot" name="joukkueen_tiedot">
						<legend>joukkueen tiedot</legend>
						<label className="form_label">
							Nimi
							<input
								id="joukkueenNimiInput"
								type="text"
								name="nimi"
								className="form_input"
								minLength="2"
								required={true}
								size="40"
								onChange={this.handleChangeNimi}
								value={nimi}
							/>
						</label>
						<br />
						<br />
						<label className="form_label">
							Luontiaika
							<input
								id="luontiaika"
								type="datetime-local"
								name="luontiaika"
								className="form_input"
								max="2018-01-01T01:00:00"
								onChange={event => {
									this.setState({
										luontiaika: event.target.value
									});
								}}
								value={luontiaika}
								required
							/>
						</label>
						<br />
						<br />
						<p className="leimaustavatParagraph">
							<label className="form_label">Leimaustapa</label>
							<span id="leimaustavat">{leimaustapaListaus}</span>
						</p>
						<p className="sarjatParagraph">
							Sarja
							<span id="sarja">{sarjaListaus}</span>
						</p>
					</fieldset>
					<fieldset id="jasenet" name="jasenet">
						<legend>Jäsenet</legend>
						{jasenKentat}
					</fieldset>
					<div id="tallennusDiv">
						<button id="tallenna" onClick={this.handleTallenna}>
							Tallenna Joukkue
						</button>
					</div>
				</form>
			</div>
		);
	}
}

const ListaaJoukkueet = props => {
	const { joukkueet, sarjat, editoiJoukkuetta } = props;
	const kaikkiJoukkueet = joukkueet.map(joukkue => (
		<JoukkueenTiedot
			joukkue={joukkue}
			sarjat={sarjat}
			key={joukkue.nimi}
			editoiJoukkuetta={editoiJoukkuetta}
		/>
	));
	return (
		<div id="loppulistaus">
			<h2>Joukkueet</h2>
			<ul>{kaikkiJoukkueet}</ul>
		</div>
	);
};

const JoukkueenTiedot = props => (
	<li className="joukkueenNimiLi">
		<a
			href="#container"
			onClick={() => {
				props.editoiJoukkuetta(props.joukkue.id);
			}}
		>
			{props.joukkue.nimi}
		</a>
		<div>
			{props.sarjat &&
				props.sarjat.find(s => s.id == props.joukkue.sarja).nimi}{" "}
			(
			{props.joukkue.leimaustapa !== null &&
				props.joukkue.leimaustapa.join(", ")}
			)
		</div>
		<JoukkueenJasenet
			jasenet={props.joukkue.jasenet}
			joukkueenNimi={props.joukkue.nimi}
		/>
	</li>
);

const JoukkueenJasenet = props => (
	<ul>
		{props.jasenet.map(jasen => (
			<li key={`${props.joukkueenNimi}-${jasen}`}>{jasen}</li>
		))}
	</ul>
);

ReactDOM.render(<App />, document.getElementById("root"));
